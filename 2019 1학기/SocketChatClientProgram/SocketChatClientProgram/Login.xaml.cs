﻿using SocketChatClientProgram.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SocketChatClientProgram
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void LgnBtn_Click(object sender, RoutedEventArgs e)
        {
            if (IDInputTB.Text == "" || PWDInputPB.Password == "")
            {
                if (IDInputTB.Text == "")
                {
                    MessageBox.Show("아이디를 입력해 주세요");
                    IDInputTB.Focus();
                }
                else
                {
                    MessageBox.Show("비밀번호를 입력해 주세요");
                    PWDInputPB.Focus();
                }
            }
            else
            {
                LoginDAO loginDAO = new LoginDAO();
                if (loginDAO.loginSql(IDInputTB.Text, PWDInputPB.Password))
                {
                    //ChatMain window = new ChatMain();
                    ChatMain_MVVM window = new ChatMain_MVVM();
                    window.Show();
                    Window.GetWindow(this).Close();
                }
                else
                {
                    MessageBox.Show("비밀번호가 맞지 않거나, 존재하지 않는 아이디 입니다.");
                }
            }
        }
    }
}
