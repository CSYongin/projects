﻿using Newtonsoft.Json.Linq;
using SocketChatClientProgram.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SocketChatClientProgram.Model
{
    class ChatRoomListModel
    {
        TcpClient clientSocket = ViewModel.ChatMain.clientSocket;
        NetworkStream stream = ViewModel.ChatMain.stream;
        string CurRoomName = ViewModel.ChatMain.CurRoomName;

        public string roomName { get; set; }

        private ICommand changeRoom;
        public ICommand ChangeRoom
        {
            get { return (this.changeRoom) ?? (this.changeRoom = new DelegateCommand(ChangeRoomProcess)); }
        }
        private void ChangeRoomProcess()
        {
            string CurRoomName = ViewModel.ChatMain.CurRoomName;
            //MessageBox.Show("버튼 클릭, 현재방 이름 : " + CurRoomName + ", 가고자 하는 방 이름 : " + this.roomName);
            if (!this.roomName.Equals(CurRoomName))
            {
                //MessageBox.Show("방 이름 다름");
                try
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        var jsonMsg = JObject.FromObject(new { protocol = 125, enterRoomName = this.roomName, leaveRoomName = CurRoomName });
                        byte[] buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString() + "$");
                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                        Console.WriteLine(jsonMsg);
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
