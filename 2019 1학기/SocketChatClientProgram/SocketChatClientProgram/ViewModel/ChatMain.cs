﻿using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json.Linq;
using SocketChatClientProgram.Model;
using System.Threading;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;

namespace SocketChatClientProgram.ViewModel
{
    class ChatMain : INotifyPropertyChanged
    {
        public static TcpClient clientSocket = new TcpClient();
        public static NetworkStream stream = default(NetworkStream);
        public static string CurRoomName = null;

        ObservableCollection<ChatModel> chats = new ObservableCollection<ChatModel>();
        ObservableCollection<ChatRoomMemberList> members = new ObservableCollection<ChatRoomMemberList>();
        ObservableCollection<ChatRoomListModel> rooms = new ObservableCollection<ChatRoomListModel>();

        public ChatMain()
        {
            chats.Clear();
            members.Clear();
            rooms.Clear();
            ChatRoomName = null;

            clientSocket.Connect("127.0.0.1", 9999);
            stream = clientSocket.GetStream();

            string nickName = LoginSession.UserNickName;

            byte[] buffer = Encoding.UTF8.GetBytes(nickName + "$");
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush();

            Thread t_handler = new Thread(GetMessage);
            t_handler.IsBackground = true;
            t_handler.Start();

            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    ChatModel chat = new ChatModel();
                    chat.chatString = "채팅방 로비에 접속했습니다.";
                    chats.Add(chat);
                    ChatCollection = chats;
                })
            );
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region property
        private string chatRoomName;

        public string ChatRoomName
        {
            get { return chatRoomName; }
            set {
                chatRoomName = value;
                OnPropertyChanged("ChatRoomName");
            }
        }

        private string sendMessageTextBox;

        public string SendMessageTextBox
        {
            get { return sendMessageTextBox; }
            set {
                sendMessageTextBox = value;
                OnPropertyChanged("SendMessageTextBox");
            }
        }

        private ChatRoomListModel selectedRoomName;

        public ChatRoomListModel SelectedRoomName
        {
            get { return selectedRoomName; }
            set {
                selectedRoomName = value;
                OnPropertyChanged("SelectedRoomName");
            }
        }


        private ObservableCollection<ChatModel> chatCollection;

        public ObservableCollection<ChatModel> ChatCollection
        {
            get { return chatCollection; }
            set {
                chatCollection = value;
                OnPropertyChanged("ChatCollection");
            }
        }

        private ObservableCollection<ChatRoomListModel> chatRoomListCollection;

        public ObservableCollection<ChatRoomListModel> ChatRoomListCollection
        {
            get { return chatRoomListCollection; }
            set {
                chatRoomListCollection = value;
                OnPropertyChanged("ChatRoomListCollection");
            }
        }

        private ObservableCollection<ChatRoomMemberList> chatRoomMemberListCollection;

        public ObservableCollection<ChatRoomMemberList> ChatRoomMemberListCollection
        {
            get { return chatRoomMemberListCollection; }
            set {
                chatRoomMemberListCollection = value;
                OnPropertyChanged("ChatRoomMemberListCollection");
            }
        }

        #endregion property

        #region Command
        private ICommand sendMessage;
        public ICommand SendMessage
        {
            get { return (this.sendMessage) ?? (this.sendMessage = new DelegateCommand(SendMessageProcess)); }
        }
        private void SendMessageProcess()
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() => 
                {
                    if (SendMessageTextBox != null && SendMessageTextBox != "")
                    {
                        var jsonMsg = JObject.FromObject(new { protocol = 900, msg = SendMessageTextBox, curroom = ChatRoomName });
                        byte[] buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString() + "$");
                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                        Console.WriteLine(jsonMsg);
                    }
                });
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                SendMessageTextBox = null;
            }
        }

        private ICommand createNewChatRoom;
        public ICommand CreateNewChatRoom
        {
            get { return (this.createNewChatRoom) ?? (this.createNewChatRoom = new DelegateCommand(CreateNewChatRoomProcess)); }
        }
        private void CreateNewChatRoomProcess()
        {
            SocketChatClientProgram.CreateRoom CR = new SocketChatClientProgram.CreateRoom();
            CR.Show();
        }
        #endregion Command

        private void GetMessage()
        {
            while (true)
            {
                stream = clientSocket.GetStream();
                int BUFFERSIZE = clientSocket.ReceiveBufferSize;
                byte[] buffer = new byte[BUFFERSIZE];
                int bytes = stream.Read(buffer, 0, buffer.Length);

                string message = Encoding.UTF8.GetString(buffer, 0, bytes);
                JObject jsonMsg = JObject.Parse(message);
                //MessageBox.Show(jsonMsg.ToString());
                int protocolKey = Int32.Parse(jsonMsg["protocol"].ToString());

                switch (protocolKey)
                {
                    case 100:
                        //채팅프로그램에 처음 들어올 시 대기방 인원의 파악을 위해 처리하는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            //각각의 어플리케이션에서 각각의 UI 스레드가 발생되도록 처리
                            {
                                try
                                {
                                    CurRoomName = "대기방";
                                    ChatRoomName = CurRoomName;
                                    int usercount = 1;
                                    foreach (var user in jsonMsg["대기방"])
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = user.ToString();
                                        members.Add(member);
                                        usercount++;
                                    }
                                    foreach (var roomname in jsonMsg["RoomList"])
                                    {
                                        ChatRoomListModel room = new ChatRoomListModel();
                                        room.roomName = roomname.ToString();
                                        rooms.Add(room);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                    ChatRoomListCollection = rooms;
                                }
                            }));
                            break;
                        }
                    case 110:
                        //채팅 프로그램의 대기방에 들어오는 인원을 처리 하는 명령을 처리하는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    if (CurRoomName == "대기방")
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = jsonMsg["addmember"].ToString();
                                        members.Add(member);
                                        ChatModel chat = new ChatModel();
                                        chat.chatString = jsonMsg["msg"].ToString();
                                        chats.Add(chat);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                    ChatCollection = chats;
                                }
                            }));
                            break;
                        }
                    case 115:
                        //채팅 프로그램의 대기방에 나가는 인원을 처리 하는 명령을 처리하는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    if (CurRoomName == "대기방")
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = jsonMsg["removemember"].ToString();
                                        try
                                        {
                                            foreach (ChatRoomMemberList item in members)
                                            {
                                                if (item.memberName == member.memberName)
                                                {
                                                    members.Remove(item);
                                                    break;
                                                }
                                            }
                                            ChatModel chat = new ChatModel();
                                            chat.chatString = jsonMsg["msg"].ToString();
                                            chats.Add(chat);
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                    ChatCollection = chats;
                                }
                            }));
                            break;
                        }
                    case 120:
                        //채팅 프로그램의 채팅방 생성시 사용되는 프로토콜
                        {
                            break;
                        }
                    case 121:
                        //채팅방 이동시 해당 사용자를 제외한 다른 모든 사람에게 변경 내용을 알려주는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    rooms.Clear();
                                    members.Clear();
                                    if(CurRoomName == jsonMsg["lastChatRoom"].ToString())
                                    {
                                        foreach (string member_name in jsonMsg["members"])
                                        {
                                            ChatRoomMemberList member = new ChatRoomMemberList();
                                            member.memberName = member_name;
                                            members.Add(member);
                                        }
                                    }
                                    foreach (string room_name in jsonMsg["rooms"])
                                    {
                                        ChatRoomListModel room = new ChatRoomListModel();
                                        room.roomName = room_name;
                                        rooms.Add(room);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                    ChatRoomListCollection = rooms;
                                }
                            }));
                            break;
                        }
                    case 122:
                        //채팅방 이동시 해당 사용자에게 변경 내용을 알려주는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    CurRoomName = jsonMsg["newChatRoom"].ToString();
                                    ChatRoomName = CurRoomName;
                                    members.Clear();
                                    rooms.Clear();
                                    foreach (string member_name in jsonMsg["newmembers"])
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = member_name;
                                        members.Add(member);
                                    }
                                    foreach (string room_name in jsonMsg["rooms"])
                                    {
                                        ChatRoomListModel room = new ChatRoomListModel();
                                        room.roomName = room_name;
                                        rooms.Add(room);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                    ChatRoomListCollection = rooms;
                                    ChatCollection = chats;
                                }
                            }));
                            break;
                        }
                    case 126:
                        //채팅방을 이동시 기존 채팅방 인원들에게 바뀐 정보를 변경하기 위한 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    members.Clear();
                                    foreach (string member_name in jsonMsg["members"])
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = member_name;
                                        members.Add(member);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                }
                            }));
                            break;
                        }
                    case 127:
                        //이동한 채팅방에 있는 인원들에게 바뀐 정보를 알려주는 프로토콜
                        {
                            Console.WriteLine("fasfsadf : " + jsonMsg);
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    CurRoomName = jsonMsg["enterChatRoom"].ToString();
                                    chats.Clear();
                                    members.Clear();
                                    foreach (string member_name in jsonMsg["members"])
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = member_name;
                                        members.Add(member);
                                    }
                                }
                                catch(Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomName = CurRoomName;
                                    ChatRoomMemberListCollection = members;
                                    ChatRoomListCollection = rooms;
                                    ChatCollection = chats;
                                }
                            }));
                            break;
                        }
                    case 128:
                        //채팅방으로 이동을 완료한 자기자신에게 프로그램 정보를 바꾸기 위한 정보를 가져오는 프로토콜
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                try
                                {
                                    members.Clear();
                                    foreach (string member_name in jsonMsg["members"])
                                    {
                                        ChatRoomMemberList member = new ChatRoomMemberList();
                                        member.memberName = member_name;
                                        members.Add(member);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                                finally
                                {
                                    ChatRoomMemberListCollection = members;
                                }
                            }));
                            break;
                        }
                    case 900:
                        //채팅프로그램의 채팅 내용 및 기타 메세지를 지속적으로 처리하는 프로토콜
                        {
                            try
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                    {
                                        ChatModel chat = new ChatModel();
                                        chat.chatString = jsonMsg["msg"].ToString();
                                        chats.Add(chat);
                                        ChatCollection = chats;
                                    })
                                );
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                            break;
                        }
                }
            }
        }
    }
}
