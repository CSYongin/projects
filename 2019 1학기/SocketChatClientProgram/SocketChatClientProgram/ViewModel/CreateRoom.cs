﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SocketChatClientProgram.ViewModel
{
    class CreateRoom : INotifyPropertyChanged
    {
        TcpClient clientSocket = ChatMain.clientSocket;
        NetworkStream stream = ChatMain.stream;
        string CurRoomName = ChatMain.CurRoomName;
        public CreateRoom()
        {
            TcpClient clientSocket = ChatMain.clientSocket;
            NetworkStream stream = ChatMain.stream;
            string CurRoomName = ChatMain.CurRoomName;
        }
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Properties
        private string sendTextBox;

        public string SendTextBox
        {
            get { return sendTextBox; }
            set {
                sendTextBox = value;
                OnPropertyChanged(SendTextBox);
            }
        }
        #endregion Properties

        #region Command
        private ICommand sendMessage;
        public ICommand SendMessage
        {
            get { return (this.sendMessage) ?? (this.sendMessage = new DelegateCommand(SendMessageProcess)); }
        }
        private void SendMessageProcess()
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (SendTextBox != null || SendTextBox != "")
                    {
                        var jsonMsg = JObject.FromObject(new { protocol = 120, newChatRoomName = SendTextBox , curChatRoom = CurRoomName });
                        byte[] buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString() + "$");
                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                        Console.WriteLine(jsonMsg);
                    }
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                ChatMain.CurRoomName = SendTextBox;
                var windows = Application.Current.Windows;
                for (var i = 0; i < windows.Count; i++)
                    if (windows[i].DataContext == this)
                        windows[i].Close();
            }
        }
        #endregion Command
    }
}
