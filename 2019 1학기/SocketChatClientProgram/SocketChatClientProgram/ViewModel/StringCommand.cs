﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SocketChatClientProgram.ViewModel
{
    class StringCommand : ICommand
    {
        private Action<string> _action;
        private Func<bool> _canExecute;
        public StringCommand(Action<string> _action) : this(_action, null)
        {
        }

        public StringCommand(Action<string> action, Func<bool> canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (this._canExecute == null)
            {
                return true;
            }
            return this._canExecute();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _action.Invoke(parameter as string);
        }
    }
}
