﻿using Newtonsoft.Json.Linq;
using SocketChatClientProgram.Model;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace SocketChatClientProgram
{
    /// <summary>
    /// ChatMain.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ChatMain : Window
    {
        TcpClient clientSocket = new TcpClient();
        NetworkStream stream = default(NetworkStream);
        Dictionary<string, List<string>> chatRoomList = new Dictionary<string, List<string>>();
        string message = string.Empty;

        #region Properties
        private string roomName;

        public string RoomName
        {
            get { return roomName; }
            set {
                roomName = value;
            }
        }

        private int roomUsersCount;

        public int RoomUsersCount
        {
            get { return roomUsersCount; }
            set {
                roomUsersCount = value;
            }
        }

        #endregion Properties

        public ChatMain()
        {
            clientSocket.Connect("127.0.0.1", 9999);
            stream = clientSocket.GetStream();

            string nickName = LoginSession.UserNickName;

            byte[] buffer = Encoding.UTF8.GetBytes(nickName + "$");
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush();

            InitializeComponent();

            Thread t_handler = new Thread(GetMessage);
            t_handler.IsBackground = true;
            t_handler.Start();

            message = "채팅방 로비에 접속하였습니다.";
            DisplayText(message);
        }

        private void GetMessage()
        {
            while (true)
            {
                stream = clientSocket.GetStream();
                int BUFFERSIZE = clientSocket.ReceiveBufferSize;
                byte[] buffer = new byte[BUFFERSIZE];
                int bytes = stream.Read(buffer, 0, buffer.Length);

                string message = Encoding.UTF8.GetString(buffer, 0, bytes);
                JObject jsonMsg = JObject.Parse(message);
                int protocolKey = Int32.Parse(jsonMsg["protocol"].ToString());

                switch (protocolKey)
                {
                    //채팅프로그램에 처음 들어올 시 대기방 인원의 파악을 위해 처리하는 프로토콜
                    case 100:
                        {
                            RoomName = "대기방";
                            int usercount = 0;
                            chatRoomList.Add("대기방", new List<string>());
                            foreach(var user in jsonMsg["대기방"])
                            {
                                chatRoomList["대기방"].Add(user.ToString());
                                usercount++;
                            }
                            RoomUsersCount = usercount;
                            break;
                        }
                    //채팅 프로그램의 대기방에 들어오는 인원을 처리 하는 명령을 처리하는 프로토콜
                    case 110:
                        {

                            break;
                        }
                    //채팅 프로그램의 대기방에 나가는 인원을 처리 하는 명령을 처리하는 프로토콜
                    case 115:
                        {

                            break;
                        }
                    //채팅프로그램의 채팅 내용 및 기타 메세지를 지속적으로 처리하는 프로토콜
                    case 900:
                        {
                            DisplayText(jsonMsg["msg"].ToString());
                            break;
                        }
                }
            }
        }

        private void DisplayText(string text)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                richTextBox.AppendText(text + Environment.NewLine);
            });
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string sendmsg = this.ChatSendTB.Text;
                var jsonMsg = JObject.FromObject(new { protocol = 900, msg = sendmsg });
                byte[] buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString() + "$");
                stream.Write(buffer, 0, buffer.Length);
                stream.Flush();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                this.ChatSendTB.Clear();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }
    }
}
