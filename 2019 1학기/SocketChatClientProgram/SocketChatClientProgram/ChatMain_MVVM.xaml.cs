﻿using System.Windows;

namespace SocketChatClientProgram
{
    /// <summary>
    /// Window1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ChatMain_MVVM : Window
    {
        public ChatMain_MVVM()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.ChatMain();
        }
    }
}
