﻿using System.Windows;

namespace SocketChatClientProgram
{
    /// <summary>
    /// CreateRoom.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CreateRoom : Window
    {
        public CreateRoom()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.CreateRoom();
        }
    }
}
