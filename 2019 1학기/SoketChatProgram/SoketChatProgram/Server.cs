﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SoketChatProgram
{
    public partial class Server : Form
    {
        TcpListener server = null;
        TcpClient clientSocket = null;
        static int counter = 0;
        public Dictionary<TcpClient, string> clientList = new Dictionary<TcpClient, string>();
        public Dictionary<string, List<string>> chatRoomList = new Dictionary<string, List<string>>();

        public Server()
        {
            InitializeComponent();

            chatRoomList.Add("대기방", new List<string>());

            Thread t = new Thread(InitSocket);
            t.IsBackground = true;
            t.Start();
        }
        private void InitSocket()
        {
            server = new TcpListener(IPAddress.Any, 9999);
            clientSocket = default(TcpClient);
            server.Start();
            DisplayText(">>채팅 서버 시작");

            while (true)
            {
                try
                {
                    counter++;
                    clientSocket = server.AcceptTcpClient();
                    Console.WriteLine(">> Accept connection from client");

                    NetworkStream stream = clientSocket.GetStream();
                    byte[] buffer = new byte[1024];
                    int bytes = stream.Read(buffer, 0, buffer.Length);
                    string user_name = Encoding.UTF8.GetString(buffer, 0, bytes);
                    user_name = user_name.Substring(0, user_name.IndexOf("$"));

                    chatRoomList["대기방"].Add(user_name);

                    var jsonMsg = JObject.FromObject(new { protocol = 100 });
                    JArray jsonWaitRoomMember = new JArray();
                    foreach (var user_nickname in chatRoomList["대기방"])
                    {
                        jsonWaitRoomMember.Add(user_nickname);
                    }
                    jsonMsg.Add("대기방", jsonWaitRoomMember);
                    JArray jsonRoomList = new JArray();
                    foreach (var roomName in chatRoomList)
                    {
                        jsonRoomList.Add(roomName.Key);
                    }
                    jsonMsg.Add("RoomList", jsonRoomList);
                    string jsonMsgString = jsonMsg.ToString();
                    //Console.WriteLine(jsonMsgString);

                    buffer = Encoding.UTF8.GetBytes(jsonMsgString);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Flush();

                    DisplayText(user_name + "님이 입장하셨습니다. ");
                    SendLogInLogOutMessageAll(user_name, true);
                    clientList.Add(clientSocket, user_name);
                    //SendMessageAll(user_name + " 님이 입장하셨습니다. ", "", false);

                    handlerClient h_client = new handlerClient();
                    h_client.OnReceived += new handlerClient.MessageDisplayHandler(OnReceived);
                    h_client.OnDisconnected += new handlerClient.DisconnectedHandler(h_client_OnDisconnected);
                    h_client.startClient(clientSocket, clientList);
                }
                catch (SocketException se)
                {
                    Trace.WriteLine(string.Format("InitSocket - SocketException : {0}", se.Message));
                    break;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(string.Format("InitSocket - Exception : {0}", ex.Message));
                    break;
                }
            }

            clientSocket.Close();
            server.Stop();
        }

        void h_client_OnDisconnected(TcpClient clientSocket)
        {
            if (clientList.ContainsKey(clientSocket))
            {
                string logout_user_name = clientList[clientSocket];
                string displayMessage = logout_user_name + "님이 퇴장 하셨습니다.";
                foreach(var pair in chatRoomList)
                {
                    string roomName = pair.Key.ToString();
                    List<string> roomMember = pair.Value;
                    for(int i = 0; i< roomMember.Count; i++)
                    {
                        if (logout_user_name == roomMember[i].ToString())
                        {
                            roomMember.Remove(logout_user_name);
                        }
                    }
                }
                clientList.Remove(clientSocket);
                DisplayText(displayMessage);
                SendLogInLogOutMessageAll(logout_user_name, false);
            }
        }

        private void OnReceived(string message, string user_name)
        {
            try
            {
                JObject jsonMsg = JObject.Parse(message);
                int protocolKey = Int32.Parse(jsonMsg["protocol"].ToString());
                switch (protocolKey)
                {
                    case 100:
                        //채팅프로그램의 채팅방 및 인원 리스트를 지속적으로 처리하는 프로토콜
                        {
                            break;
                        }
                    case 110:
                        //채팅 프로그램의 대기방에 들어오는 인원을 처리 하는 명령을 처리하는 프로토콜
                        {

                            break;
                        }
                    case 115:
                        //채팅 프로그램의 대기방에 나가는 인원을 처리 하는 명령을 처리하는 프로토콜
                        {

                            break;
                        }
                    case 120:
                        //채팅 프로그램의 채팅방 생성시 사용되는 프로토콜
                        {
                            chatRoomList.Add(jsonMsg["newChatRoomName"].ToString(), new List<string>());
                            chatRoomList[jsonMsg["newChatRoomName"].ToString()].Add(user_name);
                            chatRoomList[jsonMsg["curChatRoom"].ToString()].Remove(user_name);
                            foreach (var item in chatRoomList[jsonMsg["curChatRoom"].ToString()])
                            {
                                Console.WriteLine(item);
                                if (item == user_name)
                                {
                                    chatRoomList[jsonMsg["curChatRoom"].ToString()].Remove(item);
                                }
                            }
                            //Console.WriteLine("새로운 채팅방 : " + jsonMsg["newChatRoomName"].ToString() + ", 사용자 : " + user_name);
                            SendMessageCreateRoomMessage(user_name, jsonMsg["newChatRoomName"].ToString(), jsonMsg["curChatRoom"].ToString());
                            break;
                        }
                    case 125:
                        {
                            chatRoomList[jsonMsg["leaveRoomName"].ToString()].Remove(user_name);
                            foreach (var item in chatRoomList[jsonMsg["leaveRoomName"].ToString()])
                            {
                                Console.WriteLine(item);
                                if (item == user_name)
                                {
                                    chatRoomList[jsonMsg["leaveRoomName"].ToString()].Remove(item);
                                }
                            }
                            chatRoomList[jsonMsg["enterRoomName"].ToString()].Add(user_name);
                            SendMessageChangeRoomMessage(user_name, jsonMsg["enterRoomName"].ToString(), jsonMsg["leaveRoomName"].ToString());
                            break;
                        }
                    case 900:
                        //채팅프로그램의 채팅 내용 및 기타 메세지를 지속적으로 처리하는 프로토콜
                        {
                            string displayMessage = "From client : " + user_name + " : " + jsonMsg["msg"].ToString();
                            DisplayText(displayMessage);
                            SendMessageAll(jsonMsg["msg"].ToString(), user_name, jsonMsg["curroom"].ToString());
                            break;
                        }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                //string displayMessage = "From client : " + user_name + " : " + message;
                //DisplayText(displayMessage);
                //SendMessageAll(message, user_name, true);
            }
        }

        public void SendMessageAll(string message, string user_name, string cur_room_name)
        {
            foreach (string member_name in chatRoomList[cur_room_name])
            {
                try
                {
                    TcpClient client = clientList.FirstOrDefault(x => x.Value == member_name).Key;
                    NetworkStream stream = client.GetStream();
                    byte[] buffer = null;
                    string sendmsg = user_name + " : " + message;
                    var jsonMsg = JObject.FromObject(new { protocol = 900, msg = sendmsg });
                    buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                    stream.Write(buffer, 0, buffer.Length);
                    stream.Flush();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        public void SendMessageCreateRoomMessage(string user_name, string new_chatroom_name, string last_chatroom_name)
        {
            foreach (var pair in clientList)
            {
                if(pair.Value != user_name)
                {
                    Trace.WriteLine(string.Format("tcpclient : {0} user_name : {1}", pair.Key, pair.Value));
                    try
                    {
                        TcpClient client = pair.Key as TcpClient;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;
                        var jsonMsg = JObject.FromObject(new { protocol = 121, newChatRoom = new_chatroom_name, lastChatRoom = last_chatroom_name, username = user_name });
                        JArray jsonCurRoomMember = new JArray();
                        JArray jsonRoomList = new JArray();
                        foreach (var member in chatRoomList[last_chatroom_name])
                        {
                            jsonCurRoomMember.Add(member);
                        }
                        foreach (var room in chatRoomList.Keys)
                        {
                            jsonRoomList.Add(room);
                        }
                        jsonMsg.Add("members", jsonCurRoomMember);
                        jsonMsg.Add("rooms", jsonRoomList);
                        buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    Trace.WriteLine(string.Format("tcpclient : {0} user_name : {1}", pair.Key, pair.Value));
                    try
                    {
                        TcpClient client = pair.Key as TcpClient;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;
                        var jsonMsg = JObject.FromObject(new { protocol = 122, newChatRoom = new_chatroom_name, lastChatRoom = last_chatroom_name, username = user_name });
                        JArray jsonRoomList = new JArray();
                        JArray jsonNewRoomMember = new JArray();
                        foreach (var room in chatRoomList.Keys)
                        {
                            jsonRoomList.Add(room);
                        }
                        foreach (var user_nickname in chatRoomList[new_chatroom_name])
                        {
                            jsonNewRoomMember.Add(user_nickname);
                        }
                        jsonMsg.Add("rooms", jsonRoomList);
                        jsonMsg.Add("newmembers", jsonNewRoomMember);
                        buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }

        public void SendMessageChangeRoomMessage(string user_name, string enter_room_name, string leave_room_name)
        {
            foreach (string member_name in chatRoomList[leave_room_name])
            {
                if (member_name != user_name)
                {
                    try
                    {
                        TcpClient client = clientList.FirstOrDefault(x => x.Value == member_name).Key;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;
                        var jsonMsg = JObject.FromObject(new { protocol = 126, enterChatRoom = enter_room_name, leaveChatRoom = leave_room_name, username = user_name });
                        JArray jsonMembers = new JArray();
                        foreach (var user_nickname in chatRoomList[leave_room_name])
                        {
                            jsonMembers.Add(user_nickname);
                        }
                        jsonMsg.Add("members", jsonMembers);
                        buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    
                }
            }
            foreach (string member_name in chatRoomList[enter_room_name])
            {
                if (member_name != user_name)
                {
                    try
                    {
                        TcpClient client = clientList.FirstOrDefault(x => x.Value == member_name).Key;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;
                        var jsonMsg = JObject.FromObject(new { protocol = 128, enterChatRoom = enter_room_name, leaveChatRoom = leave_room_name, username = user_name });
                        JArray jsonEnterRoomMember = new JArray();
                        foreach (var user_nickname in chatRoomList[enter_room_name])
                        {
                            jsonEnterRoomMember.Add(user_nickname);
                        }
                        jsonMsg.Add("members", jsonEnterRoomMember);
                        buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    try
                    {
                        TcpClient client = clientList.FirstOrDefault(x => x.Value == user_name).Key;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;
                        var jsonMsg = JObject.FromObject(new { protocol = 127, enterChatRoom = enter_room_name, leaveChatRoom = leave_room_name, username = user_name });
                        JArray jsonEnterRoomMember = new JArray();
                        foreach (var user_nickname in chatRoomList[enter_room_name])
                        {
                            jsonEnterRoomMember.Add(user_nickname);
                        }
                        jsonMsg.Add("members", jsonEnterRoomMember);
                        buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                } 
            }
        }

        public void SendLogInLogOutMessageAll(string user_name, bool flag)
        {
            try
            {
                foreach (var pair in clientList)
                {
                    Trace.WriteLine(string.Format("tcpclient : {0} user_name : {1}", pair.Key, pair.Value));
                    try
                    {
                        TcpClient client = pair.Key as TcpClient;
                        NetworkStream stream = client.GetStream();
                        byte[] buffer = null;

                        if (flag)
                        {
                            string sendmsg = user_name + " 님이 입장하셨습니다.";
                            var jsonMsg = JObject.FromObject(new { protocol = 110, msg = sendmsg , addmember = user_name });
                            buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());
                        }
                        else
                        {
                            string sendmsg = user_name + " 님이 퇴장하셨습니다.";
                            var jsonMsg = JObject.FromObject(new { protocol = 115, msg = sendmsg , removemember = user_name });
                            buffer = Encoding.UTF8.GetBytes(jsonMsg.ToString());
                        }

                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void DisplayText(string text)
        {
            if (richTextBox1.InvokeRequired)
            {
                richTextBox1.BeginInvoke(new MethodInvoker(delegate
                {
                    richTextBox1.AppendText(text + Environment.NewLine);
                }));
            }
            else
                richTextBox1.AppendText(text + Environment.NewLine);
        }
    }
}
