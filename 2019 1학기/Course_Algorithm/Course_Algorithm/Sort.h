#pragma once
#include "stdafx.h"
#include <stdio.h>
#include <time.h>
#include <iostream>
#include "Search.h"

using namespace std;

class Sort {
	int* originArr;
	int arrLength;
	clock_t begin, end;
public:
	Sort(int* arr, int length);
	~Sort();

	void SelectedSort();
	void BubbleSort();
	void InsertSort();
	void ShellSort();
	void QuickSort_book();
	int partition(int* arr, int left, int right);
	void quickSort_book(int* arr, int left, int right);
	void QuickSort_ppt();
	void quickSort_ppt(int* arr, int left, int right);
	void MergeSort();
	void mergeSort(int* arr, int left, int right);
	void Merge(int* arr, int left, int mp, int right);
	void MaxHeapSort();
	void maxHeapSort(int* arr, int length);
	void CreateMaxHeap(int* arr, int length);
	void MaxHeapify(int* arr, int k, int length);
	void MinHeapSort();
	void minHeapSort(int* arr, int length);
	void CreateMinHeap(int* arr, int length);
	void MinHeapify(int* arr, int k, int length);
	void CoefficientSort(int* arr, int min, int max, int length);
	int* copyArr(int *arr, int length);
	int* ArrToMakeHeap(int *arr, int length);
	int* MadeHeapToArr(int *arr, int length);
	void printArr();
	void printArr(int* arr, int length);
	void printHeap(int* arr, int length);
	void swap(int* arr, int minloc, int curloc);
	int* swapArr(int* arr, int minloc, int curloc);
};
//void bubbleSort_error(int* arr, int length) {
//	/// X^2승 혹은 일반 함수형 시간그래프를 나타내는 알고리즘에서 사용을 하면 안좋은 예시
//	/// 처리할 데이터의 양이 많아질 수록 메모리 누수로 인해 프로그램 실행에 오류가 발생
//	bool sorted = true;
//	for (int i = 0; i < length - 1; i++) {
//		if (arr[i] > arr[i + 1]) {
//			sorted = false;
//			swapArr(arr, i + 1, i);
//			//print(swaparr(arr, i + 1, i), length);
//		}
//	}
//	if (sorted == false) {
//		bubbleSort_error(arr, length);
//	}
//	else {
//		//print(arr, length);
//	}
//}
Sort::Sort(int* arr, int length) {
	originArr = arr;
	arrLength = length;
	//print(originArr, arrLength);
}
Sort::~Sort() {
	//delete originArr;
}
int* Sort::copyArr(int *arr, int length) {
	int *newarr;
	newarr = new int[length];
	for (int i = 0; i < length; i++) {
		newarr[i] = arr[i];
	}
	return newarr;
}
int* Sort::ArrToMakeHeap(int *arr, int length) {
	int *newarr;
	newarr = new int[length];
	for (int i = 0; i < length+1; i++) {
		newarr[i] = arr[i-1];
	}
	return newarr;
}
int* Sort::MadeHeapToArr(int *arr, int length) {
	int *newarr;
	newarr = new int[length];
	for (int i = 0; i < length; i++) {
		newarr[i] = arr[i + 1];
	}
	return newarr;
}
void Sort::printArr() {
	for (int i = 0; i < arrLength; i++) {
		if (i < arrLength - 1) {
			printf("%d , ", originArr[i]);
		}
		else {
			printf("%d\n", originArr[i]);
		}
	}
}
void Sort::printArr(int* arr, int length) {
	for (int i = 0; i < length; i++) {
		if (i < length - 1) {
			printf("%d , ", arr[i]);
		}
		else {
			printf("%d\n", arr[i]);
		}
	}
}
void Sort::printHeap(int* arr, int length) {
	for (int i = 1; i < length; i++) {
		if (i < length - 1) {
			printf("%d , ", arr[i]);
		}
		else {
			printf("%d\n", arr[i]);
		}
	}
}
void Sort::swap(int* arr, int minloc, int curloc) {
	int temp = arr[curloc];
	arr[curloc] = arr[minloc];
	arr[minloc] = temp;
}
int* Sort::swapArr(int* arr, int minloc, int curloc) {
	int temp = arr[curloc];
	arr[curloc] = arr[minloc];
	arr[minloc] = temp;
	return arr;
}
void Sort::SelectedSort() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "선택정렬" << endl;
	printArr(tempArr, arrLength);
	cout << "선택정렬 시작" << endl;
	begin = clock();
	for (int i = 0; i < arrLength; i++) {
		int min = tempArr[i];
		int minLoc = i;
		int temp = 0;
		bool existmin = false;
		//최소값, 최소값의 현재 위치번지, 그리고 임시 저장공간을 선언 및 초기화;
		for (int j = i + 1; j < arrLength; j++) {
			if (min >= tempArr[j]) {
				min = tempArr[j];
				minLoc = j;
				existmin = true;
				//최소값으로 설정된 값보다 더 작은 값이 발견되면 해당 값을 최소값으로 설정하고, 
				//해당 번지값을 최소값의 위치번지로 설정한다.
			}
		}
		if (existmin == true) {
			swap(tempArr, minLoc, i);
			printArr(tempArr, arrLength);
		}
	}
	end = clock();
	cout << "선택정렬 결과" << endl;
	printArr(tempArr, arrLength);
	printf("선택정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::BubbleSort() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "버블정렬" << endl;
	printArr(tempArr, arrLength);
	cout << "버블정렬 시작" << endl;
	begin = clock();
	for (int i = 1; i < arrLength; i++) {
		for (int j = 0; j < arrLength - i; j++) {
			if (tempArr[j] > tempArr[j + 1]) {
				swap(tempArr, j + 1, j);
				printArr(tempArr, arrLength);
			}
		}
	}
	end = clock();
	cout << "버블정렬 결과" << endl;
	printArr(tempArr, arrLength);
	printf("버블정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::InsertSort() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "삽입정렬" << endl;
	printArr(tempArr, arrLength);
	cout << "삽입정렬 시작" << endl;
	begin = clock();
	for (int i = 0; i < arrLength - 1; i++) {
		if (tempArr[i] > tempArr[i + 1]) {
			int findSort = i + 1;
			for (int j = 0; j < findSort; j++) {
				if (tempArr[j] > tempArr[findSort]) {
					int toInsertArea = j;
					int temp = tempArr[findSort];
					for (findSort; findSort > toInsertArea; findSort--) {
						tempArr[findSort] = tempArr[findSort - 1];
					}
					tempArr[toInsertArea] = temp;
					printArr(tempArr, arrLength);
				}
			}
		}
	}
	end = clock();
	cout << "삽입정렬 결과" << endl;
	printArr(tempArr, arrLength);
	printf("삽입정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::ShellSort() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "쉘정렬" << endl;
	printArr(tempArr, arrLength);
	cout << "쉘정렬 시작" << endl;
	begin = clock();
	int i, j, h, v;
	for (h = 0; h < arrLength - 1; h = 3 * h + 1);
	//최대 간격을 구함
	for (; h > 0; h /= 3) {
		//최대 간격부터 3으로 나눈 몫으로 간격을 줄여 나감
		//printf("\n%d간격의 쉘정렬을 모두 실행한 결과\n", h);
		for (i = h; i < arrLength; i++) {
			v = tempArr[i];
			j = i;
			while (j >= h&&tempArr[j - h] > v) {
				//간격마다 계속 정렬을 시도함
				tempArr[j] = tempArr[j - h];
				j -= h;
			}
			tempArr[j] = v;
			//printf("\n");
			printArr(tempArr, arrLength);
		}
	}
	/*list<int> collection;
	for (int i = 1; i < arrLength; i = ((i * 3) + 1)) {
	collection.push_back(i);
	}
	while (!collection.empty())
	{
	for (int i = 0; i + collection.back() < arrLength; i++) {
	if (arr[i] > arr[i + collection.back()]) {
	swaparr(arr, i + collection.back(), i);
	}
	printf("\n");
	print(arr, length);
	}
	printf("%d간격만큼 쉘 정렬을 끝낸 결과\n", collection.back());
	print(arr, length);
	collection.pop_back();
	}*/
	end = clock();
	cout << "쉘정렬 결과" << endl;
	printArr(tempArr, arrLength);
	printf("쉘정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::QuickSort_book() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "퀵정렬_책기준" << endl;
	printArr(tempArr, arrLength);
	cout << "퀵정렬_책기준 시작" << endl;
	begin = clock();
	quickSort_book(tempArr, 0, arrLength-1);
	end = clock();
	cout << "퀵정렬_책기준 결과" << endl;
	printArr(tempArr, arrLength);
	printf("퀵정렬_책기준 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::quickSort_book(int* arr, int left, int right) {
	if (right > left) {
		int p;
		p = partition(arr, left, right);
		quickSort_book(arr, left, p - 1);
		quickSort_book(arr, p + 1, right);
	}
}
int Sort::partition(int* arr, int left, int right) {
	int pivot = arr[right];
	int i = left - 1;
	for (int j = left; j < right; j++) {
		if (arr[j] <= pivot) {
			swap(arr, ++i, j);
			printArr(arr, arrLength);
		}
	}
	swap(arr, i + 1, right);
	printArr(arr, arrLength);
	return i + 1;
}
void Sort::QuickSort_ppt() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "퀵정렬_PPT" << endl;
	printArr(tempArr, arrLength);
	cout << "퀵정렬_PPT 시작" << endl;
	begin = clock();
	quickSort_ppt(tempArr, 0, arrLength - 1);
	end = clock();
	cout << "퀵정렬_PPT 결과" << endl;
	printArr(tempArr, arrLength);
	printf("퀵정렬_PPT 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::quickSort_ppt(int* arr, int left, int right) {
	int i, j, v;
	if (right > left) {
		v = arr[right];
		i = left - 1;
		j = right;
		for (;;) {
			do { i++; } while (arr[i] < v);
			do { j--; } while (arr[j] > v);
			if (i >= j) { break; }
			swap(arr, i, j);
			printArr(arr, arrLength);
		}
		swap(arr, i, right);
		printArr(arr, arrLength);
		quickSort_ppt(arr, left, i - 1);
		quickSort_ppt(arr, i + 1, right);
	}
}
void Sort::MergeSort() {
	int* tempArr = nullptr;
	tempArr = copyArr(originArr, arrLength);
	cout << "합병정렬" << endl;
	printArr(tempArr, arrLength);
	cout << "합병정렬 시작" << endl;
	begin = clock();
	mergeSort(tempArr, 0, arrLength - 1);
	end = clock();
	cout << "합병정렬 결과" << endl;
	printArr(tempArr, arrLength);
	printf("합병정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
}
void Sort::mergeSort(int* arr, int left, int right) {
	int* temp = new int[arrLength];
	if (left < right) {
		int mid = (left + right) / 2;
		mergeSort(arr, left, mid);
		mergeSort(arr, mid + 1, right);
		Merge(arr, left, mid, right);
	}
}
void Sort::Merge(int* arr, int left, int mid, int right) {
	int i, j, k, l;
	i = left;
	j = mid + 1;
	k = left;
	int* temp = new int[arrLength];
	/* 분할 정렬된 arr의 합병 */
	while (i <= mid && j <= right) {
		if (arr[i] <= arr[j])
			temp[k++] = arr[i++];
		else
			temp[k++] = arr[j++];
	}
	// 남아 있는 값들을 일괄 복사
	if (i>mid) {
		for (l = j; l <= right; l++)
			temp[k++] = arr[l];
	}
	// 남아 있는 값들을 일괄 복사
	else {
		for (l = i; l <= mid; l++)
			temp[k++] = arr[l];
	}
	// 배열 temp[](임시 배열)의 리스트를 배열 arr[]로 재복사
	for (l = left; l <= right; l++) {
		arr[l] = temp[l];
	}
	printArr(arr, arrLength);
}
void Sort::MaxHeapSort() {
	int* tempArr = nullptr;
	tempArr = ArrToMakeHeap(originArr, arrLength + 1);
	cout << "최대힙정렬" << endl;
	printHeap(tempArr, arrLength + 1);
	cout << "최대힙구조생성" << endl;
	CreateMaxHeap(tempArr, arrLength + 1);
	cout << "최대힙구조생성결과" << endl;
	printHeap(tempArr, arrLength + 1);
	cout << "최대힙정렬 시작" << endl;
	begin = clock();
	maxHeapSort(tempArr, arrLength + 1);
	end = clock();
	cout << "최대힙정렬 결과" << endl;
	printHeap(tempArr, arrLength + 1);
	printf("최대힙정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
	cout << endl;
	cout << endl;
	int searchkey;
	cout << "찾으려는 값을 입력하세요" << endl;
	cin >> searchkey;
	Search se = Search(tempArr, arrLength + 1);
	se.MaxBinarySearch(searchkey);
}
void Sort::maxHeapSort(int* arr, int length) {
	for (int i = length - 1; i > 1; i--) {
		swap(arr, 1, i);
		printHeap(arr, length);
		MaxHeapify(arr, 1, i);
	}
}
void Sort::CreateMaxHeap(int* arr, int length) {
	for (int i = length / 2; i > 0; i--) {
		MaxHeapify(arr, i, length);
	}
}
void Sort::MaxHeapify(int* arr, int k, int length) {
	int childLeft = 2 * k, childRight = 2 * k + 1;
	int smallerChild;
	if (childRight <= length - 1) {
		if (arr[childLeft] < arr[childRight]) {
			smallerChild = childLeft;
		}
		else {
			smallerChild = childRight;
		}
	}
	else if (childLeft <= length - 1) {
		smallerChild = childLeft;
	}
	else {
		return;
	}
	if (arr[smallerChild] < arr[k]) {
		swap(arr, smallerChild, k);
		printHeap(arr, arrLength+1);
		MaxHeapify(arr, smallerChild, length);
	}
}
void Sort::MinHeapSort() {
	int* tempArr = nullptr;
	tempArr = ArrToMakeHeap(originArr, arrLength + 1);
	cout << "최소힙정렬" << endl;
	printHeap(tempArr, arrLength + 1);
	cout << "최소힙구조생성" << endl;
	CreateMinHeap(tempArr, arrLength + 1);
	cout << "최소힙구조생성결과" << endl;
	printHeap(tempArr, arrLength + 1);
	cout << "최소힙정렬 시작" << endl;
	begin = clock();
	minHeapSort(tempArr, arrLength + 1);
	end = clock();
	cout << "최소힙정렬 결과" << endl;
	printHeap(tempArr, arrLength + 1);
	printf("최소힙정렬 경과 시간 : %f\n\n", (double)((end - begin) / CLOCKS_PER_SEC));
	cout << endl;
	int searchkey;
	cin >> searchkey;
	cout << "입력한 값 : " << searchkey << endl;
	Search se = Search(tempArr, arrLength);
	se.MinBinarySearch(searchkey);
}
void Sort::minHeapSort(int* arr, int length) {
	for (int i = length - 1; i > 1; i--) {
		swap(arr, 1, i);
		printHeap(arr, length);
		MinHeapify(arr, 1, i);
	}
}
void Sort::CreateMinHeap(int* arr, int length) {
	for (int i = (length - 1) / 2; i > 0; i--) {
		MinHeapify(arr, i, length);
	}
}
void Sort::MinHeapify(int* arr, int k, int length) {
	int childLeft = 2 * k, childRight = 2 * k + 1;
	int biggerChild;
	if (childRight <= length - 1) {
		if (arr[childLeft] > arr[childRight]) {
			biggerChild = childLeft;
		}
		else {
			biggerChild = childRight;
		}
	}
	else if (childLeft <= length - 1) {
		biggerChild = childLeft;
	}
	else {
		return;
	}
	if (arr[biggerChild] > arr[k]) {
		swap(arr, biggerChild, k);
		printHeap(arr, arrLength + 1);
		MinHeapify(arr, biggerChild, length);
	}
}
void Sort::CoefficientSort(int* arr, int min, int max, int length) {
	int* temp = new int[length];
	int* count = new int[max - min + 1];
	int mintemp = min;
	for (int countcounter = 0; countcounter < max - min + 1; countcounter++) {
		count[countcounter] = 0;
		for (int i = 0; i < length; i++) {
			if (arr[i] == mintemp) {
				count[countcounter]++;
			}
		}
		mintemp++;
	}
	int tempCounter = 0;
	mintemp = min;
	for (int countcounter = 0; countcounter < max - min + 1; countcounter++) {
		for (int i = 0; i < count[countcounter]; i++) {
			temp[tempCounter++] = mintemp;
		}
		mintemp++;
	}
	printArr(temp, length);
}