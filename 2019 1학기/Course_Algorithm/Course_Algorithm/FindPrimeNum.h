#pragma once

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

class FindPrimeNum
{
	bool* arr;
	int* primeNumArr;
	int primeNum;
	int bound;
public:
	FindPrimeNum();
	FindPrimeNum(int num);
	~FindPrimeNum();
	int ReturnPrimeNum(int num);
	int ReturnPrimeNumUpFiveTimes(int num);
private:

};

FindPrimeNum::FindPrimeNum()
{
}

FindPrimeNum::FindPrimeNum(int num)
{
	bound = num+1000;
	arr = new bool[bound + 1];
	for (int i = 0; i < bound + 1; i++) {
		arr[i] = true;
	}
}

FindPrimeNum::~FindPrimeNum()
{
}

int FindPrimeNum::ReturnPrimeNum(int num) {
	int j=0;
	for (int i = 2; i < bound + 1; i++) {
		if (arr[i]) {
			if (i*i >bound*bound + 1) {
				break;
			}
			else {
				for (j = i*i; j < bound + 1;) {
					arr[j] = false;
					j = j + i;
				}
			}
		}
	}
	for (int i = 2; i < bound + 1; i++) {
		if (arr[i] && i >= 2) {
			primeNum = i;
			if (primeNum > num) {
				break;
			}
		}
	}
	return primeNum;
}

int FindPrimeNum::ReturnPrimeNumUpFiveTimes(int num) {
	int count = 0;
	int j = 0;
	for (int i = 2; i < bound + 1; i++) {
		if (arr[i]) {
			if (i*i >bound*bound + 1) {
				break;
			}
			else {
				for (j = i*i; j < bound + 1;) {
					arr[j] = false;
					j = j + i;
				}
			}
		}
	}
	for (int i = 2; i < bound + 1; i++) {
		if (arr[i] && i >= 2) {
			primeNum = i;
			if (primeNum > num) {
				if (count != 5) {
					count++;
				}
				else {
					break;
				}
			}
		}
	}
	return primeNum;
}