// Course_Algorithm.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "Search.h"
#include "Hash.h"

using namespace std;

int fib_bottom_Up(int n);
int fib_top_down(int n);
int f[100] = { 0 };

int main()
{
	int A[10] = { 6,2,8,1,3,9,4,5,10,7 };
	int C[8] = { 1,2,2,1,3,4,4,1 };
	int D[15] = { 20,5,11,5,3,15,6,8,13,13,15,5,8,15,13 };
	int E[10] = { 1,7,11,12,14,23,67,139,672,871 };
	int Randarr[100000];
	srand((int)time(NULL));
	for (int i = 0; i < 100000; i++) {
		Randarr[i] = rand() % 10000;
	}
	int length = sizeof(Randarr) / sizeof(int);

	Sort S = Sort(A, 10);
	//S.SelectedSort();
	//S.BubbleSort();
	//S.InsertSort();
	//S.ShellSort();
	//S.QuickSort_book();
	//S.QuickSort_ppt();
	//S.MergeSort();
	//S.MaxHeapSort();
	//S.MinHeapSort();

	//Hash H1 = Hash(1000);
	//H1.DoHashtoData();

	//Hash H2 = Hash();
	//H2.DoHash();

	printf("%d\n", fib_bottom_Up(40));
	printf("%d\n", fib_top_down(40));

	string text;
	cin >> text;
}

int fib_bottom_Up(int n) {
	f[100] = { 0 };
	f[1] = 1;
	f[2] = 1;
	for (int i = 3; i <= 40; i++) {
		f[i] = f[i - 1] + f[i - 2];
	}
	return f[n];
}

int fib_top_down(int n) {
	f[100] = { 0 };
	if (f[n] != 0) {
		return f[n];
	}
	else {
		if (n == 1 || n == 2) {
			f[n] = 1;
		}
		else {
			f[n] = fib_top_down(n - 1) + fib_top_down(n - 2);
		}
		return f[n];
	}
}

