#pragma once
#include "stdafx.h"
#include <stdio.h>
#include <time.h>
#include <iostream>

using namespace std;

class Search {
	int* originArr;
	int arrLength;

public:
	Search(int* arr, int length);
	~Search();
	void MaxBinarySearch(int searchKey);
	void MinBinarySearch(int searchKey);
	void printArr();
};

Search::Search(int* arr, int length) {
	originArr = arr;
	arrLength = length;
	//printArr();
}
Search::~Search() {
	//delete originArr;
}
void Search::printArr() {
	for (int i = 0; i < arrLength; i++) {
		if (i < arrLength - 1) {
			printf("%d , ", originArr[i]);
		}
		else {
			printf("%d\n", originArr[i]);
		}
	}
}
void Search::MaxBinarySearch(int searchKey) {
	int left = 1;
	int right = arrLength;
	int mid;
	bool isFind = false;
	while (right>=left)
	{
		mid = (left + right) / 2;
		if (originArr[mid] == searchKey) {
			isFind = true;
			break;
		}
		else if (originArr[mid] > searchKey) {
			left = mid + 1;
		}
		else {
			right = mid - 1;
		}
	}
	if (isFind == true) {
		printf("찾으려는 값 %d은/는 %d 위치에 존재합니다.", searchKey, mid);
	}
	else
	{
		printf("찾으려는 값 %d은/는 존재하지 않습니다.", searchKey);
	}
}
void Search::MinBinarySearch(int searchKey) {
	int left = 1;
	int right = arrLength;
	int mid;
	bool isFind = false;
	while (right >= left)
	{
		mid = (left + right) / 2;
		if (originArr[mid] == searchKey) {
			isFind = true;
			break;
		}
		else if (originArr[mid] > searchKey) {
			right = mid - 1;
		}
		else {
			left = mid + 1;
		}
	}
	if (isFind == true) {
		printf("찾으려는 값 %d은/는 %d 위치에 존재합니다.", searchKey, mid);
	}
	else
	{
		printf("찾으려는 값 %d은/는 존재하지 않습니다.", searchKey);
	}
}

