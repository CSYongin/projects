#pragma once
#include "stdafx.h"
#include "FindPrimeNum.h"
#include <stdio.h>
#include <time.h>
#include <iostream>

using namespace std;

class Hash
{
	int* hash;
	int hashSize;
	int key;
	int* hashTable;
public:
	Hash();
	Hash(int num);
	~Hash();
	void DoHash();
	void PrintHash();
	void DoHashtoData();
	void PrintHashtoData();
private:
	void CreateHashTable();
};

Hash::Hash()
{
	hash = new int[17];
	hashSize = 17;
	key = 19;
	hashTable = new int[19];
	int inputHash;
	printf("해쉬를 할 값을 입력하세요\n");
	for (int i = 0; i < 17; i++) {
		printf("hash[%d] = ", i);
		cin >> inputHash;
		hash[i] = inputHash;
	}
	CreateHashTable();
}

Hash::Hash(int num) {
	hash = new int[num];
	hashSize = num;
	//소수 찾는 알고리즘
	FindPrimeNum fpn = FindPrimeNum(num);
	//입력한 값보다 큰 소수중 5번째로 큰 소수를 반환
	key = fpn.ReturnPrimeNum(num);
	hashTable = new int[key];
	srand((int)time(NULL));
	for (int i = 0; i < num; i++) {
		hash[i] = (rand() % num);
	}
	CreateHashTable();
}

Hash::~Hash()
{
}

void Hash::DoHash() {
	int hashed;
	for (int i = 0; i < hashSize; i++) {
		hashed = hash[i]%key;
		if (hashTable[hashed] == -1) {
			hashTable[hashed] = hash[i];
		}
		else {
			while (hashTable[hashed] != -1)
			{
				hashed++;
				if (hashed > key) {
					hashed = 0;
				}
			}
			hashTable[hashed] = hash[i];
		}
		PrintHash();
	}
}

void Hash::PrintHash() {
	for (int i = 0; i < key; i++) {
		if (i < key - 1) {
			printf("%d , ", hashTable[i]);
		}
		else {
			printf("%d\n", hashTable[i]);
		}
	}
}

void Hash::DoHashtoData() {
	int hashed;
	for (int i = 0; i < hashSize; i++) {
		hashed = hash[i] % key;
		if (hashTable[hashed] == -1) {
			hashTable[hashed] = hash[i];
		}
		else {
			while (hashTable[hashed] != -1)
			{
				hashed++;
				if (hashed >= key) {
					hashed = 0;
				}
			}
			hashTable[hashed] = hash[i];
		}
	}
	PrintHashtoData();
}

void Hash::PrintHashtoData() {
	for (int i = 0; i < key; i++) {
		if (i < key - 1) {
			if (hashTable[i] == -1) {
				printf(" ");
			}
			else
			{
				printf("#");
			}
		}
		else {
			if (hashTable[i] == -1) {
				printf(" \n");
			}
			else 
			{
				printf("#\n");
			}
		}
	}
}

void Hash::CreateHashTable() {
	for (int i = 0; i < key; i++) {
		hashTable[i] = -1;
	}
}

