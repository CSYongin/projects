const express = require('express');
const app = express();
var path = require('path')
const pageRoutes = require('./routes/index.js');
const catalogRoutes = require('./routes/catalog.js');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));

app.set('view engine','pug')
app.get(express.static(path.join(__dirname, '/static')));

app.get('/',pageRoutes)
app.get('/manage',pageRoutes)
app.get('/start',pageRoutes)
app.get('/result',pageRoutes)
app.get('/create',pageRoutes)
app.get('/question',pageRoutes)
app.get('/startSurvey/',pageRoutes)
app.get('/detail/',pageRoutes)

app.post('/createSurvey',catalogRoutes)
app.post('/createQuestion',catalogRoutes)
app.post('/submitSurvey',catalogRoutes)
app.post('/updateSurvey',catalogRoutes)
app.post('/deleteSurvey',catalogRoutes)

app.listen(3000,()=>{
    console.log("connect");
})

module.exports = app;
