/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - sulmun
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sulmun` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sulmun`;

/*Table structure for table `q_type` */

DROP TABLE IF EXISTS `q_type`;

CREATE TABLE `q_type` (
  `QTID` int(1) unsigned NOT NULL,
  `descript` varchar(255) NOT NULL,
  PRIMARY KEY (`QTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `q_type` */

insert  into `q_type`(`QTID`,`descript`) values 
(1,'객관식(5문)'),
(2,'객관식(주관5문)'),
(3,'주관식'),
(4,'또래지향');

/*Table structure for table `qt_type` */

DROP TABLE IF EXISTS `qt_type`;

CREATE TABLE `qt_type` (
  `QTID` int(1) NOT NULL,
  `descript` varchar(20) NOT NULL,
  PRIMARY KEY (`QTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qt_type` */

insert  into `qt_type`(`QTID`,`descript`) values 
(1,'폭력성'),
(2,'사교성'),
(3,'친밀성'),
(4,'사회성'),
(5,'도덕성');

/*Table structure for table `questions` */

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `QID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descript` varchar(50) NOT NULL,
  `QType` int(1) unsigned NOT NULL,
  `Ttype` int(1) NOT NULL,
  `mcq1` varchar(100) DEFAULT NULL,
  `mcq2` varchar(100) DEFAULT NULL,
  `mcq3` varchar(100) DEFAULT NULL,
  `mcq4` varchar(100) DEFAULT NULL,
  `mcq5` varchar(100) DEFAULT NULL,
  `eq` varchar(100) DEFAULT NULL,
  `smq` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`QID`),
  KEY `type` (`Ttype`),
  KEY `QType` (`QType`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`Ttype`) REFERENCES `qt_type` (`QTID`),
  CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`QType`) REFERENCES `q_type` (`QTID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `questions` */

insert  into `questions`(`QID`,`descript`,`QType`,`Ttype`,`mcq1`,`mcq2`,`mcq3`,`mcq4`,`mcq5`,`eq`,`smq`) values 
(1,'A',1,1,'매우 그렇다','조금 그렇다','보통이다','조금 그렇지 않다','매우 그렇지 않다',NULL,NULL),
(3,'당신의 기분은 어떻습니까?',1,1,'매우 좋다','조금 좋다','보통이다','조금 나쁘다','매우 나쁘다',NULL,NULL),
(4,'B',1,1,'매우 그렇다','조금 그렇다','보통이다','조금 그렇지 않다','매우 그렇지 않다',NULL,NULL),
(5,'당신의 오늘 기분을 선택하세요',1,1,'매우 좋다','조금 좋다','보통이다','조금 나쁘다','매우 나쁘다',NULL,NULL),
(6,'지금 배가 고픈가요?',1,1,'매우 그렇다','조금 그렇다','보통이다','조금 그렇지 않다','매우 그렇지 않다',NULL,NULL);

/*Table structure for table `school_info` */

DROP TABLE IF EXISTS `school_info`;

CREATE TABLE `school_info` (
  `SCID` varchar(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(13) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  PRIMARY KEY (`SCID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `school_info` */

insert  into `school_info`(`SCID`,`name`,`phone`,`address1`,`address2`) values 
('T00001','수지고등학교','031-000-0000','경기도 용인시 수지구','수지고등학교');

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `SID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `SCID` varchar(20) NOT NULL,
  `grade` int(2) NOT NULL,
  `class` int(2) NOT NULL,
  `num` int(2) NOT NULL,
  `TID` varchar(50) NOT NULL,
  `gender` int(1) NOT NULL,
  PRIMARY KEY (`SID`),
  KEY `SCID` (`SCID`),
  KEY `TID` (`TID`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`SCID`) REFERENCES `school_info` (`SCID`),
  CONSTRAINT `students_ibfk_2` FOREIGN KEY (`TID`) REFERENCES `teachers` (`TID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `students` */

insert  into `students`(`SID`,`name`,`SCID`,`grade`,`class`,`num`,`TID`,`gender`) values 
(1,'홍길동','T00001',1,10,15,'test@gmail.com',1);

/*Table structure for table `survey_manager` */

DROP TABLE IF EXISTS `survey_manager`;

CREATE TABLE `survey_manager` (
  `seq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `surveyNo` int(11) unsigned NOT NULL,
  `queSeq` int(11) unsigned NOT NULL,
  `queNo` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `surveyNo` (`surveyNo`),
  KEY `queNo` (`queNo`),
  CONSTRAINT `survey_manager_ibfk_1` FOREIGN KEY (`surveyNo`) REFERENCES `surveys` (`SurveyNo`),
  CONSTRAINT `survey_manager_ibfk_2` FOREIGN KEY (`queNo`) REFERENCES `questions` (`QID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `survey_manager` */

insert  into `survey_manager`(`seq`,`surveyNo`,`queSeq`,`queNo`) values 
(1,1,1,1),
(2,2,1,3),
(3,1,2,4),
(4,3,1,5),
(5,4,1,6);

/*Table structure for table `surveys` */

DROP TABLE IF EXISTS `surveys`;

CREATE TABLE `surveys` (
  `SurveyNo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descript` varchar(200) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SurveyNo`),
  UNIQUE KEY `descript` (`descript`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `surveys` */

insert  into `surveys`(`SurveyNo`,`descript`,`visible`) values 
(1,'설문1',1),
(2,'설문2',1),
(3,'설문3',0),
(4,'설문4',0);

/*Table structure for table `teachers` */

DROP TABLE IF EXISTS `teachers`;

CREATE TABLE `teachers` (
  `TID` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL,
  `SCID` varchar(20) NOT NULL,
  `grade` int(2) NOT NULL,
  `class` int(2) NOT NULL,
  `mailAddr1` varchar(20) NOT NULL,
  `mailAddr2` varchar(20) NOT NULL,
  `mailcheck` tinyint(1) NOT NULL,
  PRIMARY KEY (`TID`),
  KEY `SCID` (`SCID`),
  CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`SCID`) REFERENCES `school_info` (`SCID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `teachers` */

insert  into `teachers`(`TID`,`name`,`SCID`,`grade`,`class`,`mailAddr1`,`mailAddr2`,`mailcheck`) values 
('test@gmail.com','교사1','T00001',1,10,'test','gmail.com',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
