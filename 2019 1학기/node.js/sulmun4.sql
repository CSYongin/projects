/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - sulmun4
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sulmun4` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sulmun4`;

/*Table structure for table `check_user` */

DROP TABLE IF EXISTS `check_user`;

CREATE TABLE `check_user` (
  `seq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `SID` (`SID`),
  CONSTRAINT `check_user_ibfk_1` FOREIGN KEY (`SID`) REFERENCES `sulmun`.`students` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `check_user` */

/*Table structure for table `result` */

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `SID` bigint(20) unsigned NOT NULL,
  `queSeq` int(10) unsigned NOT NULL,
  `queNo` bigint(20) unsigned NOT NULL,
  `result` varchar(255) NOT NULL,
  KEY `SID` (`SID`),
  KEY `queNo` (`queNo`),
  CONSTRAINT `result_ibfk_1` FOREIGN KEY (`SID`) REFERENCES `sulmun`.`students` (`SID`),
  CONSTRAINT `result_ibfk_2` FOREIGN KEY (`queNo`) REFERENCES `sulmun`.`questions` (`QID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `result` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
