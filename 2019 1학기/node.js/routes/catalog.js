var express = require('express');
var catalogrouter = express.Router();

var surveyController = require('../controllers/surveyController');

catalogrouter.post('/createSurvey',surveyController.createSurvey)
catalogrouter.post('/createQuestion',surveyController.createQuestion)
catalogrouter.post('/submitSurvey',surveyController.submitSurvey)
catalogrouter.post('/updateSurvey',surveyController.updateSurvey)
catalogrouter.post('/deleteSurvey',surveyController.deleteSurvey)

module.exports = catalogrouter;
