var express = require('express');
var pagerouter = express.Router();


var pageController = require('../controllers/pageController');

pagerouter.get('/', pageController.indexPage);
pagerouter.get('/manage', pageController.managementPage);
pagerouter.get('/startSurvey/', pageController.startSurveyPage);
pagerouter.get('/start', pageController.startPage);
pagerouter.get('/result', pageController.resultPage);
pagerouter.get('/create', pageController.createPage);
pagerouter.get('/detail/', pageController.updatePage);

module.exports = pagerouter;
