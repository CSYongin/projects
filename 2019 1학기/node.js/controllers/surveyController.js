var fs = require('fs');
var path = require('path');
var mysql = require('mysql');
var dbconfig = require('../DB/db.conn.js');
var dbsulmunconfig = require('../DB/db.sulmun.conn.js');

exports.createSurvey = function(req, res)  {
    // console.log(req.body.title)
    var query = 'INSERT INTO surveys(`descript`) VALUES(?)';
    var params = [req.body.title];
    dbsulmunconfig.query(query, params, function (error, result, fields) {
        if (error) {
            console.log(error);
        } else {
            //  console.log(result);
            dbsulmunconfig.query('SELECT * FROM q_type', function (error, q_result) {
                if (error) {
                    console.log(error);
                }
                else{
                    dbsulmunconfig.query('SELECT * FROM qt_type', function (error, t_result) {
                        if (error) {
                            console.log(error);
                        }
                        var query = 'CREATE DATABASE sulmun'+result.insertId+' CHARACTER SET utf8 COLLATE utf8_general_ci'
                        dbconfig.query(query, function (error, cdbresult) {
                            if(error){
                                console.log(error);
                            }
                            else{
                                var databasename = 'sulmun'+result.insertId;
                                var SurveyDBConfig = mysql.createConnection({
                                    host : 'localhost',
                                    user : 'root',
                                    password : 'cs1234',
                                    database : databasename
                                })
                                SurveyDBConfig.connect(function (error) {
                                    if(error){
                                        console.log(erorr);
                                    }
                                    else{
                                        console.log(databasename + "conn");
                                        var SurveyDBQuery1 = 'CREATE TABLE check_user(seq BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,SID BIGINT UNSIGNED NOT NULL,FOREIGN KEY(`SID`) REFERENCES sulmun.`students`(`SID`))';
                                        var SurveyDBQuery2 = 'CREATE TABLE result(SID BIGINT UNSIGNED NOT NULL,queSeq INT UNSIGNED NOT NULL,queNo BIGINT UNSIGNED NOT NULL,result VARCHAR(255) NOT NULL,FOREIGN KEY (`SID`) REFERENCES sulmun.`students`(`SID`),FOREIGN KEY (`queNo`) REFERENCES sulmun.`questions`(`QID`))';
                                        SurveyDBConfig.query(SurveyDBQuery1, function (error, ctresult1) {
                                            if(error){
                                                console.log(error);
                                            }
                                            else{
                                                SurveyDBConfig.query(SurveyDBQuery2, function (error, ctresult2) {
                                                    if(error){
                                                        console.log(error);
                                                    }
                                                    else{
                                                        res.render('survey_question.pug', {
                                                            surveytitle: req.body.title,
                                                            surveyNo: result.insertId,
                                                            questionType: q_result,
                                                            tendencyType: t_result
                                                        });
                                                    }
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        })
                    });
                }
            });
        }
    });
};
exports.createQuestion = function(req, res)  {
    var query = 'INSERT INTO questions(`descript`,`QType`,`Ttype`,`mcq1`,`mcq2`,`mcq3`,`mcq4`,`mcq5`) VALUES(?,1,?,?,?,?,?,?)';
    var params = [req.body.que_title , parseInt(req.body.que_tend) , req.body.que_index1 , req.body.que_index2 , req.body.que_index3 , req.body.que_index4 , req.body.que_index5];
    dbsulmunconfig.query(query, params, function (error, result) {
        if(error){
            console.log(error);
        }
        else{
            var query = 'INSERT INTO survey_manager(`surveyNO`,`queSeq`,`queNo`) VALUES(?,?,?)';
            var params = [parseInt(req.body.surveyNo), 1, parseInt(result.insertId)];
            dbsulmunconfig.query(query, params, function (error, result) {
                if(error){
                    console.log(error);
                }
                else{
                    res.redirect('/');
                }
            })
        }
    })
};
exports.submitSurvey = function(req, res)  {
    var reqbody = req.body;
    var arrayAnswer = [];
    for(var key in reqbody){
        if(req.body.hasOwnProperty(key)){
            item = req.body[key];
            arrayAnswer.push(item);
        }
    }
    var databasename = 'sulmun'+reqbody.surveyNo;
    var SurveyDBConfig = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'cs1234',
        database : databasename
    })
    SurveyDBConfig.connect(function (error) {
        if(error){
            console.log(error);
        }
        else{
            SurveyDBConfig.query('INSERT INTO check_user(`SID`) VALUES(1)',function (error, result) {
                if(error){
                    console.log(error);
                }
                else{
                    for(i=1; i<arrayAnswer.length; i++) {
                        var query = 'INSERT INTO result VALUES(1, ' + i + ', '+arrayAnswer[i]+')';
                        //console.log(query);
                        SurveyDBConfig.query(query,function (error, answerresult) {
                            if(error){
                                console.log(error);
                            }
                        })
                    }
                    res.redirect('/');
                }
            })
        }
    })
};
exports.updateSurvey = function(req, res)  {
    console.log("surveyNo ->" + req.body.surveyNo)
    var reqbody = req.body;
    var updateText = [];
    for(var key in reqbody){
        if(req.body.hasOwnProperty(key)){
            item = req.body[key];
            updateText.push(item);
            console.log(item);
        }
    }
};
exports.deleteSurvey = function(req, res)  {
    var surveyNo = req.body.delete_num;
    dbsulmunconfig.query('UPDATE surveys SET visible = 0 WHERE SurveyNo = '+mysql.escape(surveyNo), function (error, result) {
        if(error){
            console.log(error);
        }
        else{
            res.redirect('/manage');
        }
    })
};
