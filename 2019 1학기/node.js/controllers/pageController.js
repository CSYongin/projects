var fs = require('fs');
var mysql = require('mysql');
var dbconfig = require('../DB/db.sulmun.conn.js');

exports.indexPage = function(req, res)  {
    res.render('index')
};
exports.managementPage = function(req, res)  {
    dbconfig.query('SELECT * FROM surveys WHERE visible = 1',function (error, result, fields) {
        if(error){
            console.log(error);
        }
        res.render('managePage',{results:result});
    })
};
exports.startSurveyPage = function(req, res)  {
    var id = req.query;
    dbconfig.query('SELECT * FROM surveys WHERE visible =1 AND SurveyNo = '+mysql.escape(id.id), function (error, surveyDescript) {
        if(error){
            console.log(error);
        }
        else{
            var query = 'SELECT s.`descript`, sm.`queSeq`, q.`descript`, q.`QType`, q.`Ttype`, q.`mcq1`, q.`mcq2`, q.`mcq3`, q.`mcq4`, q.`mcq5` FROM surveys AS s LEFT JOIN survey_manager AS sm ON s.`SurveyNo` = sm.`surveyNo` LEFT JOIN questions AS q ON sm.`queNo` = q.`QID` WHERE s.`visible` =1 AND s.`SurveyNo` = ';
            dbconfig.query(query+mysql.escape(id.id),function (error, result) {
                if(error){
                    console.log(error);
                }
                else{
                    //console.log(surveyDescript);
                    //console.log(result);
                    res.render('startSurvey',{SurveyDescript:surveyDescript[0], questions:result})
                }
            })
        }
    })
};
exports.startPage = function(req, res)  {
    dbconfig.query('SELECT * FROM surveys WHERE visible = 1',function (error, result, fields) {
        if(error){
            console.log(error);
        }
        res.render('startPage',{results:result});
    })
};
exports.resultPage = function(req, res)  {
    res.render('resultPage')
};
exports.createPage = function(req, res)  {
    res.render('newSurvey')
};
exports.updatePage = function(req, res)  {
  var id = req.query;

  dbconfig.query('SELECT * FROM surveys WHERE SurveyNo = '+mysql.escape(id.id), function (error, surveyDescript) {
      if(error){
          console.log(error);
      }
      else{
          var query = 'SELECT s.`descript`, sm.`queSeq`, q.`descript`, q.`QType`, q.`Ttype`, q.`mcq1`, q.`mcq2`, q.`mcq3`, q.`mcq4`, q.`mcq5` FROM surveys AS s LEFT JOIN survey_manager AS sm ON s.`SurveyNo` = sm.`surveyNo` LEFT JOIN questions AS q ON sm.`queNo` = q.`QID` WHERE s.`visible` =1 AND s.`SurveyNo` = ';
          dbconfig.query(query+mysql.escape(id.id),function (error, result) {
              if(error){
                  console.log(error);
              }
              else{
                  console.log(surveyDescript);
                  console.log(result);
                  res.render('detailSurvey',{SurveyDescript:surveyDescript[0], questions:result, surveyNo:id.id})
              }
          })
      }
  })
};
