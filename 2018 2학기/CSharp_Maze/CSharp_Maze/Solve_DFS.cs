﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class Solve_DFS : Solve
    {
        public Solve_DFS(OpenMaze openmaze)
        {
            //Maze copymaze;
            maze = openmaze;
            mazeQueue = new Queue<Maze>();
            locationStack = new Stack<Location2D>();
            roots = new List<Location2D>();
            int crosscnt = 0;
            //Console.WriteLine("- 입구 - 미로 탐색 시작");
            //maze.display();
            //mazeQueue.Enqueue(copyMaze(maze));
            locationStack.Push(copyLocation((int)maze.EnterRow, (int)maze.EnterCol));
            while (locationStack.Count != 0)
            {
                Location2D here = locationStack.Pop();
                roots.Add(here);
                if (maze.getCurLoc(here) == 9)
                {
                    //Console.WriteLine("- 출구 - 미로 탐색 종료");
                    //maze.display();
                    maze.printFile("resultmaze_dfs.txt");
                    maze.printFileint("resultmaze_dfs_int.txt");
                    //mazeQueue.Enqueue(copyMaze(maze));
                    int cnt = 0;
                    foreach (Maze resultMaze in mazeQueue)
                    {
                        if (cnt == 0)
                        {
                            //Console.WriteLine("- 입구 - 미로 탐색 시작");
                        }
                        else if (cnt == mazeQueue.Count - 1)
                        {
                            //Console.WriteLine("- 출구 - 미로 탐색 종료");
                        }
                        else
                        {
                            //Console.WriteLine("{0}번째 교차점", cnt);
                        }
                        //resultMaze.display();
                        cnt++;
                    }
                    return;
                }
                else
                {
                    changeCurLoc(here);
                    if (ValidLoc(here) > 1)
                    {
                        //Console.WriteLine("{0}번째 교차점", ++crosscnt);
                        //maze.display();
                        //mazeQueue.Enqueue(copyMaze(maze));
                        if (isValidLoc(here.Row - 1, here.Col)) { locationStack.Push(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationStack.Push(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationStack.Push(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationStack.Push(copyLocation(here.Row, here.Col + 1)); }
                    }
                    else
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { locationStack.Push(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationStack.Push(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationStack.Push(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationStack.Push(copyLocation(here.Row, here.Col + 1)); }
                    }
                }
            }
        }
    }
}
