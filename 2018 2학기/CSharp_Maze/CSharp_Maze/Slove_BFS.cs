﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class Slove_BFS : Solve
    {
        public Slove_BFS(OpenMaze openmaze)
        {
            maze = openmaze;
            mazeQueue = new Queue<Maze>();
            locationQueue = new Queue<Location2D>();
            roots = new List<Location2D>();
            int progresscnt = 0;
            //Console.WriteLine("- 입구 - 미로 탐색 시작");
            //maze.display();
            locationQueue.Enqueue(copyLocation((int)maze.EnterRow, (int)maze.EnterCol));

            while (locationQueue.Count != 0)
            {
                Location2D here = locationQueue.Dequeue();
                roots.Add(here);
                if (maze.getCurLoc(here) == 9)
                {
                    //Console.WriteLine("- 출구 - 미로 탐색 종료");
                    //maze.display();
                    maze.printFile("resultmaze_bfs.txt");
                    maze.printFileint("resultmaze_bfs_int.txt");
                    int cnt = 0;
                    foreach (Maze resultMaze in mazeQueue)
                    {
                        if (cnt == 0)
                        {
                            //Console.WriteLine("- 입구 - 미로 탐색 시작");
                        }
                        else if (cnt == mazeQueue.Count - 1)
                        {
                            //Console.WriteLine("- 출구 - 미로 탐색 종료");
                        }
                        else
                        {
                            //Console.WriteLine("{0}번째 미로 진행 상황", cnt);
                        }
                        cnt++;
                    }
                    return;
                }
                else
                {
                    changeCurLoc(here);
                    //Console.WriteLine("{0}번째 미로 진행 상황",++progresscnt);
                    //maze.display();
                    if (ValidLoc(here) > 1)
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { locationQueue.Enqueue(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationQueue.Enqueue(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationQueue.Enqueue(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationQueue.Enqueue(copyLocation(here.Row, here.Col + 1)); }
                    }
                    else
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { locationQueue.Enqueue(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationQueue.Enqueue(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationQueue.Enqueue(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationQueue.Enqueue(copyLocation(here.Row, here.Col + 1)); }
                    }
                }
            }
        }
    }
}
