﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace CSharp_Maze
{
    class OpenMaze : Maze
    {
        public OpenMaze() { }
        public OpenMaze(string filename, int row, int col)
        {
            try
            {
                StreamReader mazereader = new StreamReader(filename); //새로운 파읽 읽기 스트림 생성
                if (mazereader == null)
                {
                    throw new Exception("해당 파일이 존재 하지 않습니다.");
                }
                else
                {
                    string line = "";
                    int temp = 0;
                    Row = row;
                    Col = col;
                    maze = new int[row, col];
                    
                    ArrayList arrline = new ArrayList();
                    while (temp < Row)
                    {
                        line = mazereader.ReadLine(); //파일을 한줄씩 읽음
                        if (line != null)
                        {
                            arrline.Add(line);
                            char[] mazeline = line.ToArray(); //한줄씩 읽은것을 다시 한문자씩 자름
                            for (int i = 0; i < col; i++)
                            {
                                if(mazeline[i] == '9')
                                {
                                    maze[temp, i] = 9;
                                    ExitRow = temp;
                                    ExitCol = i;
                                }
                                else if(mazeline[i] == '8')
                                {
                                    maze[temp, i] = 8;
                                    EnterRow = temp;
                                    EnterCol = i;
                                }
                                else if(mazeline[i] == '2')
                                {
                                    maze[temp, i] = 1;
                                }
                                else if (mazeline[i] == '1')
                                {
                                    maze[temp, i] = 1;
                                }
                                else if (mazeline[i] == '0')
                                {
                                    maze[temp, i] = 0;
                                }
                            }
                            temp++;
                        }
                    }
                    mazereader.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        ~OpenMaze() { }
        public void displayint()
        {
            for(int i = 0; i<Row; i++)
            {
                for(int j = 0; j<Col; j++)
                {
                    Console.Write(maze[i,j]);
                }
                Console.WriteLine();
            }
        }
    }
}
