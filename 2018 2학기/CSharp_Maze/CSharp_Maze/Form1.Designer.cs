﻿namespace CSharp_Maze
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CreateMazeBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ColSizeTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RowSizeTB = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ReGenMaze_btn = new System.Windows.Forms.Button();
            this.GenMazeBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BFSSolve_btn = new System.Windows.Forms.Button();
            this.DFSSolve_btn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.OpenBFSInt_btn = new System.Windows.Forms.Button();
            this.OpenDFSInt_btn = new System.Windows.Forms.Button();
            this.OpenGenInt_btn = new System.Windows.Forms.Button();
            this.OpenCreInt_btn = new System.Windows.Forms.Button();
            this.OpenGen_btn = new System.Windows.Forms.Button();
            this.OpenCre_btn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.CurGenLoc_Lable = new System.Windows.Forms.Label();
            this.CurGenCnt_Lable = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.OpenBFS_btn = new System.Windows.Forms.Button();
            this.OpenDFS_btn = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.CurSeekLoc_Lable = new System.Windows.Forms.Label();
            this.CurSeekCnt_Lable = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CreateMazeBtn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ColSizeTB);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.RowSizeTB);
            this.groupBox1.Location = new System.Drawing.Point(6, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 116);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "미로생성";
            // 
            // CreateMazeBtn
            // 
            this.CreateMazeBtn.Location = new System.Drawing.Point(99, 87);
            this.CreateMazeBtn.Name = "CreateMazeBtn";
            this.CreateMazeBtn.Size = new System.Drawing.Size(85, 23);
            this.CreateMazeBtn.TabIndex = 4;
            this.CreateMazeBtn.Text = "미로생성";
            this.CreateMazeBtn.UseVisualStyleBackColor = true;
            this.CreateMazeBtn.Click += new System.EventHandler(this.CreateMazeBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "열의크기";
            // 
            // ColSizeTB
            // 
            this.ColSizeTB.Location = new System.Drawing.Point(65, 52);
            this.ColSizeTB.Name = "ColSizeTB";
            this.ColSizeTB.Size = new System.Drawing.Size(100, 21);
            this.ColSizeTB.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "행의크기";
            // 
            // RowSizeTB
            // 
            this.RowSizeTB.Location = new System.Drawing.Point(65, 17);
            this.RowSizeTB.Name = "RowSizeTB";
            this.RowSizeTB.Size = new System.Drawing.Size(100, 21);
            this.RowSizeTB.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ReGenMaze_btn);
            this.groupBox2.Controls.Add(this.GenMazeBtn);
            this.groupBox2.Location = new System.Drawing.Point(6, 200);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(191, 53);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "미로구현";
            // 
            // ReGenMaze_btn
            // 
            this.ReGenMaze_btn.Location = new System.Drawing.Point(99, 20);
            this.ReGenMaze_btn.Name = "ReGenMaze_btn";
            this.ReGenMaze_btn.Size = new System.Drawing.Size(85, 23);
            this.ReGenMaze_btn.TabIndex = 1;
            this.ReGenMaze_btn.Text = "미로재구성";
            this.ReGenMaze_btn.UseVisualStyleBackColor = true;
            this.ReGenMaze_btn.Click += new System.EventHandler(this.ReGenMaze_btn_Click);
            // 
            // GenMazeBtn
            // 
            this.GenMazeBtn.Location = new System.Drawing.Point(8, 20);
            this.GenMazeBtn.Name = "GenMazeBtn";
            this.GenMazeBtn.Size = new System.Drawing.Size(85, 23);
            this.GenMazeBtn.TabIndex = 0;
            this.GenMazeBtn.Text = "미로구현";
            this.GenMazeBtn.UseVisualStyleBackColor = true;
            this.GenMazeBtn.Click += new System.EventHandler(this.GenMazeBtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BFSSolve_btn);
            this.groupBox3.Controls.Add(this.DFSSolve_btn);
            this.groupBox3.Location = new System.Drawing.Point(7, 259);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(191, 51);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "미로풀이";
            // 
            // BFSSolve_btn
            // 
            this.BFSSolve_btn.Location = new System.Drawing.Point(99, 21);
            this.BFSSolve_btn.Name = "BFSSolve_btn";
            this.BFSSolve_btn.Size = new System.Drawing.Size(85, 23);
            this.BFSSolve_btn.TabIndex = 1;
            this.BFSSolve_btn.Text = "너비우선탐색";
            this.BFSSolve_btn.UseVisualStyleBackColor = true;
            this.BFSSolve_btn.Click += new System.EventHandler(this.BFSSolve_btn_Click);
            // 
            // DFSSolve_btn
            // 
            this.DFSSolve_btn.Location = new System.Drawing.Point(8, 21);
            this.DFSSolve_btn.Name = "DFSSolve_btn";
            this.DFSSolve_btn.Size = new System.Drawing.Size(85, 23);
            this.DFSSolve_btn.TabIndex = 0;
            this.DFSSolve_btn.Text = "깊이우선탐색";
            this.DFSSolve_btn.UseVisualStyleBackColor = true;
            this.DFSSolve_btn.Click += new System.EventHandler(this.DFSSolve_btn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox1);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(881, 593);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "미로";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.OpenBFSInt_btn);
            this.groupBox5.Controls.Add(this.OpenDFSInt_btn);
            this.groupBox5.Controls.Add(this.OpenGenInt_btn);
            this.groupBox5.Controls.Add(this.OpenCreInt_btn);
            this.groupBox5.Location = new System.Drawing.Point(6, 316);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(92, 142);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "파일확인";
            // 
            // OpenBFSInt_btn
            // 
            this.OpenBFSInt_btn.Location = new System.Drawing.Point(8, 107);
            this.OpenBFSInt_btn.Name = "OpenBFSInt_btn";
            this.OpenBFSInt_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenBFSInt_btn.TabIndex = 5;
            this.OpenBFSInt_btn.Text = "BFS결과";
            this.OpenBFSInt_btn.UseVisualStyleBackColor = true;
            this.OpenBFSInt_btn.Click += new System.EventHandler(this.OpenBFSInt_btn_Click);
            // 
            // OpenDFSInt_btn
            // 
            this.OpenDFSInt_btn.Location = new System.Drawing.Point(8, 78);
            this.OpenDFSInt_btn.Name = "OpenDFSInt_btn";
            this.OpenDFSInt_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenDFSInt_btn.TabIndex = 4;
            this.OpenDFSInt_btn.Text = "DFS결과";
            this.OpenDFSInt_btn.UseVisualStyleBackColor = true;
            this.OpenDFSInt_btn.Click += new System.EventHandler(this.OpenDFSInt_btn_Click);
            // 
            // OpenGenInt_btn
            // 
            this.OpenGenInt_btn.Location = new System.Drawing.Point(8, 49);
            this.OpenGenInt_btn.Name = "OpenGenInt_btn";
            this.OpenGenInt_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenGenInt_btn.TabIndex = 3;
            this.OpenGenInt_btn.Text = "구현확인";
            this.OpenGenInt_btn.UseVisualStyleBackColor = true;
            this.OpenGenInt_btn.Click += new System.EventHandler(this.OpenGenInt_btn_Click);
            // 
            // OpenCreInt_btn
            // 
            this.OpenCreInt_btn.Location = new System.Drawing.Point(8, 20);
            this.OpenCreInt_btn.Name = "OpenCreInt_btn";
            this.OpenCreInt_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenCreInt_btn.TabIndex = 2;
            this.OpenCreInt_btn.Text = "생성확인";
            this.OpenCreInt_btn.UseVisualStyleBackColor = true;
            this.OpenCreInt_btn.Click += new System.EventHandler(this.OpenCreInt_btn_Click);
            // 
            // OpenGen_btn
            // 
            this.OpenGen_btn.Location = new System.Drawing.Point(7, 49);
            this.OpenGen_btn.Name = "OpenGen_btn";
            this.OpenGen_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenGen_btn.TabIndex = 1;
            this.OpenGen_btn.Text = "구현확인";
            this.OpenGen_btn.UseVisualStyleBackColor = true;
            this.OpenGen_btn.Click += new System.EventHandler(this.OpenGen_btn_Click);
            // 
            // OpenCre_btn
            // 
            this.OpenCre_btn.Location = new System.Drawing.Point(7, 20);
            this.OpenCre_btn.Name = "OpenCre_btn";
            this.OpenCre_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenCre_btn.TabIndex = 0;
            this.OpenCre_btn.Text = "생성확인";
            this.OpenCre_btn.UseVisualStyleBackColor = true;
            this.OpenCre_btn.Click += new System.EventHandler(this.OpenCre_btn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.groupBox10);
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.groupBox1);
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.groupBox2);
            this.groupBox6.Location = new System.Drawing.Point(899, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 592);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "미로메뉴";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox1);
            this.groupBox10.Location = new System.Drawing.Point(7, 143);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(190, 51);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Animation";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(112, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "애니메이션 효과";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.CurGenLoc_Lable);
            this.groupBox9.Controls.Add(this.CurGenCnt_Lable);
            this.groupBox9.Controls.Add(this.label7);
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Location = new System.Drawing.Point(6, 468);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(191, 56);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "미로 생성 탐색기";
            // 
            // CurGenLoc_Lable
            // 
            this.CurGenLoc_Lable.AutoSize = true;
            this.CurGenLoc_Lable.Location = new System.Drawing.Point(109, 39);
            this.CurGenLoc_Lable.Name = "CurGenLoc_Lable";
            this.CurGenLoc_Lable.Size = new System.Drawing.Size(69, 12);
            this.CurGenLoc_Lable.TabIndex = 2;
            this.CurGenLoc_Lable.Text = "CurGenLoc";
            // 
            // CurGenCnt_Lable
            // 
            this.CurGenCnt_Lable.AutoSize = true;
            this.CurGenCnt_Lable.Location = new System.Drawing.Point(109, 17);
            this.CurGenCnt_Lable.Name = "CurGenCnt_Lable";
            this.CurGenCnt_Lable.Size = new System.Drawing.Size(67, 12);
            this.CurGenCnt_Lable.TabIndex = 1;
            this.CurGenCnt_Lable.Text = "CurGenCnt";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "미로 생성 좌표 : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "미로 생성 순서 : ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.OpenBFS_btn);
            this.groupBox8.Controls.Add(this.OpenDFS_btn);
            this.groupBox8.Controls.Add(this.OpenGen_btn);
            this.groupBox8.Controls.Add(this.OpenCre_btn);
            this.groupBox8.Location = new System.Drawing.Point(105, 316);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(92, 142);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "미로확인";
            // 
            // OpenBFS_btn
            // 
            this.OpenBFS_btn.Location = new System.Drawing.Point(7, 107);
            this.OpenBFS_btn.Name = "OpenBFS_btn";
            this.OpenBFS_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenBFS_btn.TabIndex = 3;
            this.OpenBFS_btn.Text = "BFS결과";
            this.OpenBFS_btn.UseVisualStyleBackColor = true;
            this.OpenBFS_btn.Click += new System.EventHandler(this.OpenBFS_btn_Click);
            // 
            // OpenDFS_btn
            // 
            this.OpenDFS_btn.Location = new System.Drawing.Point(7, 78);
            this.OpenDFS_btn.Name = "OpenDFS_btn";
            this.OpenDFS_btn.Size = new System.Drawing.Size(78, 23);
            this.OpenDFS_btn.TabIndex = 2;
            this.OpenDFS_btn.Text = "DFS결과";
            this.OpenDFS_btn.UseVisualStyleBackColor = true;
            this.OpenDFS_btn.Click += new System.EventHandler(this.OpenDFS_btn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.CurSeekLoc_Lable);
            this.groupBox7.Controls.Add(this.CurSeekCnt_Lable);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Location = new System.Drawing.Point(6, 530);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(191, 56);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "미로 탐색 탐색기";
            // 
            // CurSeekLoc_Lable
            // 
            this.CurSeekLoc_Lable.AutoSize = true;
            this.CurSeekLoc_Lable.Location = new System.Drawing.Point(137, 39);
            this.CurSeekLoc_Lable.Name = "CurSeekLoc_Lable";
            this.CurSeekLoc_Lable.Size = new System.Drawing.Size(74, 12);
            this.CurSeekLoc_Lable.TabIndex = 2;
            this.CurSeekLoc_Lable.Text = "CurSeekLoc";
            // 
            // CurSeekCnt_Lable
            // 
            this.CurSeekCnt_Lable.AutoSize = true;
            this.CurSeekCnt_Lable.Location = new System.Drawing.Point(137, 17);
            this.CurSeekCnt_Lable.Name = "CurSeekCnt_Lable";
            this.CurSeekCnt_Lable.Size = new System.Drawing.Size(72, 12);
            this.CurSeekCnt_Lable.TabIndex = 1;
            this.CurSeekCnt_Lable.Text = "CurSeekCnt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "현재 미로 탐색 좌표 : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "현재 미로 탐색 순서 : ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(6, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(869, 567);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 614);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RowSizeTB;
        private System.Windows.Forms.Button CreateMazeBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ColSizeTB;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button GenMazeBtn;
        private System.Windows.Forms.Button ReGenMaze_btn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button DFSSolve_btn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button BFSSolve_btn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button OpenGen_btn;
        private System.Windows.Forms.Button OpenCre_btn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button OpenCreInt_btn;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button OpenGenInt_btn;
        private System.Windows.Forms.Button OpenDFS_btn;
        private System.Windows.Forms.Button OpenBFS_btn;
        private System.Windows.Forms.Button OpenBFSInt_btn;
        private System.Windows.Forms.Button OpenDFSInt_btn;
        private System.Windows.Forms.Label CurSeekLoc_Lable;
        private System.Windows.Forms.Label CurSeekCnt_Lable;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label CurGenLoc_Lable;
        private System.Windows.Forms.Label CurGenCnt_Lable;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

