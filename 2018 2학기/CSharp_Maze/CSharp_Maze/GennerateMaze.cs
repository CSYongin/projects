﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class GennerateMaze : Maze
    {
        Random rnum = new Random();
        string[] locationState = new string[4] { "위", "아래", "왼쪽", "오른쪽" };
        public GennerateMaze() { }
        public GennerateMaze(Maze createdMaze)
        {
            maze = createdMaze.getMaze;
            Row = createdMaze.Row;
            Col = createdMaze.Col;
            BackupMaze = mazeCopy(maze, Row, Col);
            int enterRow = createdMaze.EnterRow.GetValueOrDefault(), enterCol = createdMaze.EnterCol.GetValueOrDefault();
            int exitRow = createdMaze.ExitRow.GetValueOrDefault(), exitCol = createdMaze.ExitCol.GetValueOrDefault();
            StartLoc = setStart(enterRow, enterCol, findLocation(enterRow, enterCol));
            ExitLoc = new Location2D(exitRow, exitCol);
        }
        public Location2D StartLoc { get; set; }
        public Location2D ExitLoc { get; set; }
        public int[,] BackupMaze { get; set; }
        //입구, 출구의 위치 파악
        private int findLocation(int enterRow, int enterCol)
        {
            int lacation;
            if(enterRow == 0)
            {
                lacation = 0;
            }
            else
            {
                if(enterRow == Row - 1)
                {
                    lacation = 1;
                }
                else
                {
                    if (enterCol == 0)
                    {
                        lacation = 2;
                    }else
                    {
                        lacation = 3;
                    }
                }
            }
            return lacation;
        }
        //처음 시작할 좌표(입구 좌표가 아닌 입구의 바로 옆 인접 미로 내부의 좌표)
        private Location2D setStart(int enterRow, int enterCol, int location)
        {
            Location2D startLoc = null;
            switch (location)
            {
                case 0://입구가 미로의 위에 있을 경우
                    startLoc = new Location2D(enterRow + 1, enterCol);
                    //this.setCurLoc(startLoc, "passage");
                    setCurLoc(startLoc, "passage");
                    break;
                case 1://입구가 미로의 아래에 있을 경우
                    startLoc = new Location2D(enterRow - 1, enterCol);
                    //this.setCurLoc(startLoc, "passage");
                    setCurLoc(startLoc, "passage");
                    break;
                case 2://입구가 미로의 좌측에 있을 경우
                    startLoc = new Location2D(enterRow, enterCol + 1);
                    //this.setCurLoc(startLoc, "passage");
                    setCurLoc(startLoc, "passage");
                    break;
                case 3://입구가 미로의 우측에 있을 경우
                    startLoc = new Location2D(enterRow, enterCol - 1);
                    //this.setCurLoc(startLoc, "passage");
                    setCurLoc(startLoc, "passage");
                    break;
            }
            return startLoc;
        }
        //출구의 인접내부 좌표가 통로로 생성이 되어 있는지 파악
        private int findExit(Location2D exitLoc)
        {
            Location2D exit = null;
            int exit_state = 1;
            switch (findLocation(exitLoc.Row, exitLoc.Col))
            {
                case 0:
                    exit = new Location2D(exitLoc.Row + 1, exitLoc.Col);
                    exit_state = getCurLoc(exit);
                    break;
                case 1:
                    exit = new Location2D(exitLoc.Row - 1, exitLoc.Col);
                    exit_state = getCurLoc(exit);
                    break;
                case 2:
                    exit = new Location2D(exitLoc.Row, exitLoc.Col + 1);
                    exit_state = getCurLoc(exit);
                    break;
                case 3:
                    exit = new Location2D(exitLoc.Row, exitLoc.Col - 1);
                    exit_state = getCurLoc(exit);
                    break;
            }
            return exit_state;
        }
        //길을 만들고자 하는 위치의 주변 상황 파악
        //만약 만들고자 하는 위치에 주변에 이미 뚫려져 있는 길이 2개 이상이라면(1개는 바로 이웃한길) 생성 불가능
        private bool isVailToPath(Location2D entry)
        {
            int neighborPathCnt = 0;
            if (getCurLoc(entry.Row - 1, entry.Col) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row + 1, entry.Col) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row, entry.Col - 1) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row, entry.Col + 1) == 0)
            {
                neighborPathCnt++;
            }
            if (neighborPathCnt > 1 || neighborPathCnt < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //새로 만들어진 path들에 의해 새로 neighbors반환
        private List<Location2D> getNewNeighbors(List<Location2D> roots)
        {
            List<Location2D> neighbors = new List<Location2D>();
            foreach (Location2D entry in roots)
            {
                //위로 갈 수 있다면
                if (getCurLoc(entry.Row - 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row - 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row - 1, entry.Col));
                    }
                }
                //아래로 갈 수 있다면
                if (getCurLoc(entry.Row + 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row + 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row + 1, entry.Col));
                    }
                }
                //왼쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col - 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col - 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col - 1));
                    }
                }
                //아래쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col + 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col + 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col + 1));
                    }
                }
            }
            return neighbors;
        }
        //자동 미로 생성
        public List<Location2D> gen(Location2D start, Location2D exit, int[,] backupMaze)
        {
            List<Location2D> roots = new List<Location2D>();
            List<Location2D> neighbors = new List<Location2D>();

            //맨 처음 시작지점을 길들의 집합에 저장
            roots.Add(start);
            setCurLoc(start, "passage");
            //맨 처음 시작지점에서 갈 수 있는 이웃들을 집합에 저장
            foreach (Location2D entry in roots)
            {
                //위로 갈 수 있다면
                if (getCurLoc(entry.Row - 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row - 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row - 1, entry.Col));
                    }
                }
                //아래로 갈 수 있다면
                if (getCurLoc(entry.Row + 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row + 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row + 1, entry.Col));
                    }
                }
                //왼쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col - 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col - 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col - 1));
                    }
                }
                //아래쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col + 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col + 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col + 1));
                    }
                }
            }
            while (neighbors.Count > 0)
            {
                int neighbor_num = rnum.Next(0, neighbors.Count);
                Location2D toGoPath = neighbors[neighbor_num];
                if (isVailToPath(toGoPath))
                {
                    roots.Add(new Location2D(toGoPath.Row, toGoPath.Col)); 
                }
                else
                {
                    continue;
                }
                setCurLoc(toGoPath, "passage");
                neighbors = getNewNeighbors(roots);
            }
            if (findExit(exit) == 1)
            {//다시 재생성을 해야함;
                maze = mazeCopy(backupMaze, Row, Col);
                return gen(start, exit, backupMaze);
            }
            else
            {
                return roots;
            }
        }
        //자동 미로 재생성
        public List<Location2D> regen(Location2D start, Location2D exit, int[,] backupMaze)
        {
            maze = mazeCopy(backupMaze, Row, Col);
            List<Location2D> roots = new List<Location2D>();
            List<Location2D> neighbors = new List<Location2D>();

            //맨 처음 시작지점을 길들의 집합에 저장
            roots.Add(start);
            setCurLoc(start, "passage");
            //맨 처음 시작지점에서 갈 수 있는 이웃들을 집합에 저장
            foreach (Location2D entry in roots)
            {
                //위로 갈 수 있다면
                if (getCurLoc(entry.Row - 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row - 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row - 1, entry.Col));
                    }
                }
                //아래로 갈 수 있다면
                if (getCurLoc(entry.Row + 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row + 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row + 1, entry.Col));
                    }
                }
                //왼쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col - 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col - 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col - 1));
                    }
                }
                //아래쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col + 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col + 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col + 1));
                    }
                }
            }
            while (neighbors.Count > 0)
            {
                int neighbor_num = rnum.Next(0, neighbors.Count);
                Location2D toGoPath = neighbors[neighbor_num];
                if (isVailToPath(toGoPath))
                {
                    roots.Add(new Location2D(toGoPath.Row, toGoPath.Col));
                }
                else
                {
                    continue;
                }
                setCurLoc(toGoPath, "passage");
                neighbors = getNewNeighbors(roots);
            }
            if (findExit(exit) == 1)
            {//다시 재생성을 해야함
                return regen(start, exit, backupMaze);
            }
            else
            {
                return roots;
            }
        }
    }
}
