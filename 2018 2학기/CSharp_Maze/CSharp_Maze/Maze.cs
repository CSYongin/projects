﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    interface IMaze
    {
        int getCurLoc(int row, int col);
        int getCurLoc(Location2D entry);
        void setCurLoc(Location2D entry, string pathType);
        int[,] mazeCopy(int[,] origin, int row, int col);
        void display();
        void printFile(string filename);
        void printFileint(string filename);
    }
    class Maze : IMaze
    {
        protected int[,] maze;
        public int Row { get; set; }
        public int Col { get; set; }
        public int? EnterRow { get; set; } = null;
        public int? EnterCol { get; set; } = null;
        public int? ExitRow { get; set; } = null;
        public int? ExitCol { get; set; } = null;
        public int[,] getMaze { get { return maze; } }
        public int getCurLoc(int row, int col)
        {
            return maze[row, col];
        } //좌표를 통한 현재 위치의 정보 반환
        public int getCurLoc(Location2D entry)
        {
            return maze[entry.Row, entry.Col];
        } //Location2D를 통한 현재 위치의 정보 반환
        public void setCurLoc(Location2D entry, string pathType)
        {
            if (pathType == "cross")
            {
                maze[entry.Row, entry.Col] = 7;
            }
            else if (pathType == "path")
            {
                maze[entry.Row, entry.Col] = 6;
            }
            else if (pathType == "passage")
            {
                maze[entry.Row, entry.Col] = 0;
            }
            else
            {
                maze[entry.Row, entry.Col] = 5;
            }
        } //지나온의 위치를 pathType에 따라 지나온경로 변경
        public int[,] mazeCopy(int[,] origin, int row, int col)
        {
            int[,] backupMaze = new int[row, col];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    backupMaze[i, j] = origin[i, j];
                }
            }
            return backupMaze;
        }
        public void display()
        {
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                {
                    if (maze[i, j] == 1)
                    { //벽을 표시
                        Console.Write("■");
                    }
                    else if (maze[i, j] == 0)
                    { //지나갈수 있는 길을 표시
                        Console.Write("□");
                    }
                    else if (maze[i, j] == 2)
                    { // 최 외곽 벽과 랜덤 생성시 세워지는 벽을 표시
                        Console.Write("■");
                    }
                    else if (maze[i, j] == 8)
                    { //입구를 표시
                        Console.Write("☆");
                    }
                    else if (maze[i, j] == 9)
                    { //출구를 표시
                        Console.Write("★");
                    }
                    else if (maze[i, j] == 7)
                    { //교차점 표시
                        Console.Write("◇");
                    }
                    else if (maze[i, j] == 6)
                    { //지나온 길 표시
                        Console.Write("▒");
                    }
                    else if (maze[i, j] == 5)
                    { //갈 수 없는 길 표시
                        Console.Write("▩");
                    }
                }
                Console.WriteLine();
            }
        }
        public void printFile(string filename)
        {
            StreamWriter mazeWriter = new StreamWriter(filename, false);
            for (int i = 0; i < Row; i++)
            {
                for(int j = 0; j<Col; j++)
                {
                    if (maze[i, j] == 1)
                    { //벽을 표시
                        mazeWriter.Write("■");
                    }
                    else if (maze[i, j] == 0)
                    { //지나갈수 있는 길을 표시
                        mazeWriter.Write("□");
                    }
                    else if (maze[i, j] == 2)
                    { // 최 외곽 벽과 랜덤 생성시 세워지는 벽을 표시
                        mazeWriter.Write("■");
                    }
                    else if (maze[i, j] == 8)
                    { //입구를 표시
                        mazeWriter.Write("☆");
                    }
                    else if (maze[i, j] == 9)
                    { //출구를 표시
                        mazeWriter.Write("★");
                    }
                    else if (maze[i, j] == 7)
                    { //교차점 표시
                        mazeWriter.Write("◇");
                    }
                    else if (maze[i, j] == 6)
                    { //지나온 길 표시
                        mazeWriter.Write("▒");
                    }
                    else if (maze[i, j] == 5)
                    { //지나온 길 표시
                        mazeWriter.Write("▩");
                    }
                }
                mazeWriter.WriteLine();
            }
            mazeWriter.Close();
        }
        public void printFileint(string filename)
        {
            StreamWriter mazeWriter = new StreamWriter(filename, false);
            for (int r = 0; r < Row; r++)
            {
                for (int c = 0; c < Col; c++)
                {
                    mazeWriter.Write(maze[r, c]);
                }
                mazeWriter.WriteLine();
            }
            mazeWriter.Close();
        }
    }
}
