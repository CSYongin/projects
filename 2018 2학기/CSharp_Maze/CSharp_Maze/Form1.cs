﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Timers;
using System.Drawing.Drawing2D;
using System.IO;

namespace CSharp_Maze
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.Timer timer;
        CreateMaze newMaze;
        GennerateMaze genMaze;
        AnimationMaze aniMaze;
        OpenMaze open_dfs;
        OpenMaze open_bfs;
        List<Location2D> createRoots;
        List<Location2D> showCreateRoots;
        List<Location2D> showCreateNeighbors;
        List<Location2D> pathRoots;
        List<Rectangle> recs;
        List<Rectangle> aniRootRecs;
        List<Rectangle> aniNeighborRecs;
        int[,] cremaze;
        int[,] genmaze;
        int[,] a;
        int mazeRow = 0, mazeCol = 0;
        int timerCnt = 0;
        private int Xmin, Ymin, Cellwid, CellHgt;
        private Graphics g;
        Pen p;
        SolidBrush b;

        public Form1()
        {
            InitializeComponent();
            CurSeekCnt_Lable.Text = "";
            CurSeekLoc_Lable.Text = "";
            CurGenCnt_Lable.Text = "";
            CurGenLoc_Lable.Text = "";
            checkBox1.Checked = false;
            groupBox7.Enabled = false;
            groupBox9.Enabled = false;
            g = pictureBox1.CreateGraphics();
            p = new Pen(Color.Black, 1);
            b = new SolidBrush(Color.Black);
        }

        private void showGen()
        {
            showCreMaze();
            showCreateRoots = new List<Location2D>();
            showCreateNeighbors = new List<Location2D>();
            aniRootRecs = new List<Rectangle>();
            aniNeighborRecs = new List<Rectangle>();
            //a = new int[mazeRow, mazeCol];
            //for (int i = 0; i < mazeRow; i++)
            //{
            //    for (int j = 0; j < mazeCol; j++)
            //    {
            //        a[i, j] = cremaze[i, j];
            //    }
            //}
            timerCnt = 0;
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 10;
            timer.Enabled = true;
            timer.Tick += showGen_Tick;
        }

        private void showGen_Tick(object sender, EventArgs e)
        {
            if (timerCnt >= createRoots.Count)
            {
                timer.Enabled = false;
                showGenMaze();
                MessageBox.Show("미로 생성 과정 완료");
                timerCnt = 0;
            }
            else
            {
                CurGenCnt_Lable.Text = (timerCnt + 1).ToString();
                CurGenLoc_Lable.Text = (createRoots[timerCnt].Row + 1).ToString() + " , " + (createRoots[timerCnt].Col + 1).ToString();
                // 너무 느려 ㅠㅠ
                /*
                //showCreateRoots.Add(createRoots[timerCnt]);
                //aniMaze = new AnimationMaze(a, showCreateRoots);
                //showCreateNeighbors = aniMaze.getNewNeighbors(showCreateRoots);
                //foreach (var root in showCreateRoots)
                //{
                //    for(int i = 0; i<mazeRow; i++)
                //    {
                //        for (int j = 0; j < mazeCol; j++)
                //        {
                //            if (i == root.Row && j == root.Col)
                //            {
                //                aniRootRecs.Add(new Rectangle(Xmin + (Cellwid * j), Ymin + (CellHgt * i), Cellwid, CellHgt));
                //            }
                //        }
                //    }
                //}
                //foreach (var root in showCreateNeighbors)
                //{
                //    for (int i = 0; i < mazeRow; i++)
                //    {
                //        for (int j = 0; j < mazeCol; j++)
                //        {
                //            if (i == root.Row && j == root.Col)
                //            {
                //                aniNeighborRecs.Add(new Rectangle(Xmin + (Cellwid * j), Ymin + (CellHgt * i), Cellwid, CellHgt));
                //            }
                //        }
                //    }
                //}
                //foreach (Rectangle rec in aniNeighborRecs)
                //{
                //    g.FillRectangle(new SolidBrush(Color.FromArgb(255, 153, 153)), rec);
                //}
                //foreach (Rectangle rec in aniRootRecs)
                //{
                //    g.FillRectangle(new SolidBrush(Color.White), rec);
                //}
                */
                Rectangle rootrec = new Rectangle(Xmin + (Cellwid * createRoots[timerCnt].Col), Ymin + (CellHgt * createRoots[timerCnt].Row), Cellwid, CellHgt);
                g.FillRectangle(new SolidBrush(Color.White), rootrec);
                timerCnt++;
            }
        }

        private void showSeek()
        {
            showGenMaze();
            showCreateRoots = new List<Location2D>();
            showCreateNeighbors = new List<Location2D>();
            aniRootRecs = new List<Rectangle>();
            aniNeighborRecs = new List<Rectangle>();
            timerCnt = 0;
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 25;
            timer.Enabled = true;
            timer.Tick += showSeek_Tick;
        }

        private void showSeek_Tick(object sender, EventArgs e)
        {
            if (timerCnt >= pathRoots.Count)
            {
                timer.Enabled = false;
                MessageBox.Show("미로 탐색 과정 완료");
                timerCnt = 0;
            }
            else
            {
                CurSeekCnt_Lable.Text = (timerCnt + 1).ToString();
                CurSeekLoc_Lable.Text = (pathRoots[timerCnt].Row + 1).ToString() + " , " + (pathRoots[timerCnt].Col + 1).ToString();
                Rectangle rootrec = new Rectangle(Xmin + (Cellwid * pathRoots[timerCnt].Col), Ymin + (CellHgt * pathRoots[timerCnt].Row), Cellwid, CellHgt);
                g.FillRectangle(new SolidBrush(Color.Gray), rootrec);
                timerCnt++;
            }
        }

        private void showCreMaze()
        {
            g.Clear(Color.White);
            int cnt = 0;
            recs = new List<Rectangle>();
            for (int i = 0; i < mazeRow; i++)
            {
                for (int j = 0; j < mazeCol; j++)
                {
                    recs.Add(new Rectangle(Xmin + (Cellwid * j), Ymin + (CellHgt * i), Cellwid, CellHgt));
                }
            }
            foreach (Rectangle rec in recs)
            {
                //g.DrawRectangle(p, rec);
                if (cremaze[cnt / mazeCol, cnt % mazeCol] == 8)
                {
                    g.FillRectangle(new SolidBrush(Color.Blue), rec);
                    cnt++;
                }
                else if (cremaze[cnt / mazeCol, cnt % mazeCol] == 9)
                {
                    g.FillRectangle(new SolidBrush(Color.Red), rec);
                    cnt++;
                }
                else
                {
                    g.FillRectangle(b, rec);
                    cnt++;
                }
            }
        }

        private void showGenMaze()
        {
            g.Clear(Color.White);
            int cnt = 0;
            recs = new List<Rectangle>();
            for (int i = 0; i < mazeRow; i++)
            {
                for (int j = 0; j < mazeCol; j++)
                {
                    recs.Add(new Rectangle(Xmin + (Cellwid * j), Ymin + (CellHgt * i), Cellwid, CellHgt));
                }
            }
            foreach (Rectangle rec in recs)
            {
                //g.DrawRectangle(p, rec);
                if (genmaze[cnt / mazeCol, cnt % mazeCol] == 1 || genmaze[cnt / mazeCol, cnt % mazeCol] == 2)
                {
                    g.FillRectangle(b, rec);
                    cnt++;
                }
                else if (genmaze[cnt / mazeCol, cnt % mazeCol] == 8)
                {
                    g.FillRectangle(new SolidBrush(Color.Blue), rec);
                    cnt++;
                }
                else if (genmaze[cnt / mazeCol, cnt % mazeCol] == 9)
                {
                    g.FillRectangle(new SolidBrush(Color.Red), rec);
                    cnt++;
                }
                else
                {
                    g.DrawRectangle(p, rec);
                    cnt++;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                groupBox7.Enabled = true;
                groupBox9.Enabled = true;
            }else
            {
                groupBox7.Enabled = false;
                groupBox9.Enabled = false;
            }
        }

        private void CreateMazeBtn_Click(object sender, EventArgs e)
        {
            bool success = false;
            try
            {
                mazeRow = Int32.Parse(RowSizeTB.Text);
                mazeCol = Int32.Parse(ColSizeTB.Text);
                if (mazeRow < 3 || mazeCol < 3)
                {
                    throw new Exception("미로 생성에는 적어도 3보다 큰 값이 입력 되어야 합니다.");
                }
                else
                {
                    //if(mazeRow %2 ==0 && mazeCol % 2 == 0)
                    //{
                    //    throw new Exception("미로 생성에는 홀수를 입력 해 주세요.");
                    //}
                    Cellwid = pictureBox1.ClientSize.Width / (mazeCol + 2);
                    CellHgt = pictureBox1.ClientSize.Height / (mazeRow + 2);
                    if (Cellwid > CellHgt) Cellwid = CellHgt;
                    else CellHgt = Cellwid;
                    Xmin = (pictureBox1.ClientSize.Width - mazeCol * Cellwid) / 2;
                    Ymin = (pictureBox1.ClientSize.Height - mazeRow * CellHgt) / 2;
                    success = true;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (success == true)
            {
                MessageBox.Show("미로의 크기 : " + mazeRow + " X " + mazeCol);
                newMaze = new CreateMaze(mazeRow, mazeCol);
                genMaze = null;
                cremaze = new int[mazeRow, mazeCol];
                for(int i = 0; i<mazeRow; i++)
                {
                    for(int j = 0; j<mazeCol; j++)
                    {
                        cremaze[i, j] = newMaze.getMaze[i, j];
                    }
                }
                newMaze.printFile("createmaze.txt");
                newMaze.printFileint("createmaze_int.txt");
            }
            else
            {
                MessageBox.Show("값이 잘못 입력 되었습니다.");
            }
        }

        private void GenMazeBtn_Click(object sender, EventArgs e)
        {
            if (newMaze == null)
            {
                MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
            }
            else if(genMaze != null)
            {
                MessageBox.Show("이미 초기 미로 구현이 완료 되었습니다. 미로 재구현 버튼을 눌러 주세요.");
            }
            else
            {
                Maze downSizeMaze = new Maze();
                downSizeMaze = newMaze;
                genMaze = new GennerateMaze(downSizeMaze);
                createRoots = genMaze.gen(genMaze.StartLoc, genMaze.ExitLoc, genMaze.BackupMaze);
                genmaze = genMaze.getMaze;
                genMaze.printFile("genmaze.txt");
                genMaze.printFileint("genmaze_int.txt");
                if (checkBox1.Checked == true)
                {
                    showGen();
                }
                else
                {
                    showGenMaze();
                }
            }
        }

        private void ReGenMaze_btn_Click(object sender, EventArgs e)
        {
            
            if (newMaze == null || genMaze == null)
            {
                if(newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
            }
            else
            {
                createRoots = genMaze.regen(genMaze.StartLoc, genMaze.ExitLoc, genMaze.BackupMaze);
                genmaze = genMaze.getMaze;
                genMaze.printFile("genmaze.txt");
                genMaze.printFileint("genmaze_int.txt");
                if (checkBox1.Checked == true)
                {
                    showGen();
                }
                else
                {
                    showGenMaze();
                }
            }
        }

        private void DFSSolve_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
            }
            else
            {
                open_dfs = new OpenMaze("genmaze_int.txt", mazeRow, mazeCol);
                Solve_DFS solve_DFS = new Solve_DFS(open_dfs);
                pathRoots = solve_DFS.getRoots();
                if (checkBox1.Checked == true)
                {
                    showSeek();
                }
                else
                {

                }
            }
        }

        private void BFSSolve_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
            }
            else
            {
                open_bfs = new OpenMaze("genmaze_int.txt", mazeRow, mazeCol);
                Slove_BFS solve_BFS = new Slove_BFS(open_bfs);
                pathRoots = solve_BFS.getRoots();
                if (checkBox1.Checked == true)
                {
                    showSeek();
                }
                else
                {

                }
            }
        }

        private void OpenCreInt_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null)
            {
                MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
            }
            else
            {
                Process.Start(Application.StartupPath + "\\createmaze_int.txt");
            }
        }

        private void OpenCre_btn_Click(object sender, EventArgs e)
        {
            if(newMaze == null)
            {
                MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
            }
            else
            {
                Process.Start(Application.StartupPath + "\\createmaze.txt");
            }
        }

        private void OpenGenInt_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\genmaze_int.txt");
            }
        }

        private void OpenGen_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\genmaze.txt");
            }
        }

        private void OpenDFSInt_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null || open_dfs == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else if (genMaze == null)
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
                else if (open_dfs == null)
                {
                    MessageBox.Show("깊이우선 탐색이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("넓이우선 탐색이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\resultmaze_dfs_int.txt");
            }
        }

        private void OpenDFS_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null || open_dfs == null )
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else if (genMaze == null)
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
                else if (open_dfs == null)
                {
                    MessageBox.Show("깊이우선 탐색이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("넓이우선 탐색이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\resultmaze_dfs.txt");
            }
        }

        private void OpenBFSInt_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null || open_bfs == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else if (genMaze == null)
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
                else if (open_dfs == null)
                {
                    MessageBox.Show("깊이우선 탐색이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("넓이우선 탐색이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\resultmaze_bfs_int.txt");
            }
        }

        private void OpenBFS_btn_Click(object sender, EventArgs e)
        {
            if (newMaze == null || genMaze == null || open_bfs == null)
            {
                if (newMaze == null)
                {
                    MessageBox.Show("미로 생성이 완료 되지 않았습니다.");
                }
                else if (genMaze == null)
                {
                    MessageBox.Show("미로 구현이 완료 되지 않았습니다.");
                }
                else if (open_dfs == null)
                {
                    MessageBox.Show("깊이우선 탐색이 완료 되지 않았습니다.");
                }
                else
                {
                    MessageBox.Show("넓이우선 탐색이 완료 되지 않았습니다.");
                }
            }
            else
            {
                Process.Start(Application.StartupPath + "\\resultmaze_bfs.txt");
            }
        }

    }
}
