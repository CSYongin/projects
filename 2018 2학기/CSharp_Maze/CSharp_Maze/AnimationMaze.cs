﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class AnimationMaze : Maze
    {
        List<Location2D> roots;
        public AnimationMaze(int[,] maze, List<Location2D> roots)
        {
            this.maze = maze;
            this.roots = roots;
            foreach (var root in roots)
            {
                setCurLoc(root, "passage");
            }
        }
        private bool isVailToPath(Location2D entry)
        {
            int neighborPathCnt = 0;
            if (getCurLoc(entry.Row - 1, entry.Col) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row + 1, entry.Col) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row, entry.Col - 1) == 0)
            {
                neighborPathCnt++;
            }
            if (getCurLoc(entry.Row, entry.Col + 1) == 0)
            {
                neighborPathCnt++;
            }
            if (neighborPathCnt > 1 || neighborPathCnt < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public List<Location2D> getNewNeighbors(List<Location2D> roots)
        {
            List<Location2D> neighbors = new List<Location2D>();
            foreach (Location2D entry in roots)
            {
                //위로 갈 수 있다면
                if (getCurLoc(entry.Row - 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row - 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row - 1, entry.Col));
                    }
                }
                //아래로 갈 수 있다면
                if (getCurLoc(entry.Row + 1, entry.Col) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row + 1, entry.Col)))
                    {
                        neighbors.Add(new Location2D(entry.Row + 1, entry.Col));
                    }
                }
                //왼쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col - 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col - 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col - 1));
                    }
                }
                //아래쪽으로 갈 수 있다면
                if (getCurLoc(entry.Row, entry.Col + 1) == 1)
                {
                    if (isVailToPath(new Location2D(entry.Row, entry.Col + 1)))
                    {
                        neighbors.Add(new Location2D(entry.Row, entry.Col + 1));
                    }
                }
            }
            return neighbors;
        }
    }
}
