﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    interface ISolve
    {
        Maze copyMaze(Maze curMaze);
        Location2D copyLocation(int row, int col);
        List<Location2D> getRoots();
        void changeCurLoc(Location2D entry);
        bool isValidLoc(int row, int col);
        int ValidLoc(Location2D entry);
    }
    class Solve : ISolve
    {
        protected Stack<Location2D> locationStack;
        protected Queue<Location2D> locationQueue;
        protected Queue<Maze> mazeQueue;
        protected Maze maze;
        protected List<Location2D> roots;
        public Maze copyMaze(Maze curMaze)
        {
            Maze tempMaze = new Maze();
            return tempMaze;
        }
        public Location2D copyLocation(int row, int col)
        {
            Location2D tempLocation = new Location2D(row, col);
            return tempLocation;
        }
        public List<Location2D> getRoots()
        {
            return roots;
        }
        public void changeCurLoc(Location2D entry)
        {
            if (maze.getCurLoc(entry) == 8 || maze.getCurLoc(entry) == 9) { }
            else
            {
                if (ValidLoc(entry) > 1)
                {
                    maze.setCurLoc(entry, "cross");
                }// 교차점인 경우
                else if (ValidLoc(entry) == 1)
                {
                    maze.setCurLoc(entry, "path");
                }// 교차점이 아닌 갈 수 있는 경로
                else
                {
                    maze.setCurLoc(entry, "block");
                }// 더이상 갈 수 없는 경로
            }
        } //해당entry에 해당되는 좌표를 지나온경로 표시
        public bool isValidLoc(int row, int col)
        {
            if (row < 0 || col < 0 || row >= maze.Row || col >= maze.Col) { return false; }
            else
            {
                if (maze.getCurLoc(row, col) == 0 || maze.getCurLoc(row, col) == 9) { return true; }
                else { return false; }
            }
        } //해당 좌표에서 진행이 가능한지 불가능 한지 판단
        public int ValidLoc(Location2D entry)
        {
            int validPath = 0;
            if (isValidLoc(entry.Row - 1, entry.Col))
            {
                validPath++; //위로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row + 1, entry.Col))
            {
                validPath++; // 아래로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row, entry.Col - 1))
            {
                validPath++; // 좌로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row, entry.Col + 1))
            {
                validPath++; // 우로 진행 가능할 경우
            }
            return validPath;
        } //해당entry에 해당되는 좌표가 이동가능한 경로의 갯수 판단
    }
}
