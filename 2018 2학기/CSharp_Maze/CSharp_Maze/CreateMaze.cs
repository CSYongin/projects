﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class CreateMaze : Maze
    {
        Random rnum = new Random();
        public CreateMaze() { }
        public CreateMaze(int row, int col)
        {
            Row = row;
            Col = col;
            maze = new int[row, col];
            Location2D enterLocation = CreateEnterExit(row, col);
            Location2D exitLocation = CreateEnterExit(row, col, enterLocation);
            EnterRow = enterLocation.Row;
            EnterCol = enterLocation.Col;
            ExitRow = exitLocation.Row;
            ExitCol = exitLocation.Col;
            //Console.WriteLine("미로의 입구 : {0} X {1}", EnterRow, EnterCol);
            for (int r = 0; r < row; r++)
            {
                for(int c = 0; c<col; c++) 
                {
                    if (r == 0 || r == Row - 1 || c == 0 || c == Col - 1)
                    {
                        if(r == EnterRow && c == EnterCol)
                        {
                            maze[r, c] = 8;
                        }else if(r == ExitRow && c == ExitCol)
                        {
                            maze[r, c] = 9;
                        }else
                        {
                            maze[r, c] = 2;
                        }
                    }
                    else
                    {
                        maze[r, c] = 1;
                    }
                }
            }
        }
        public Location2D CreateEnterExit(int row, int col, Location2D enter = null)
        {
            //랜덤하게 숫자를 생성해서 상(0)하(1)좌(2)우(3)를 먼저 파악
            //그후 다시 랜덤하게 숫자를 생성해서 입구의 좌표 생성
            Location2D loc2D;
            int locationnum = rnum.Next(0, 4);
            int location = locationnum % 4; // 상하좌우를 판단하기 위한 난수
            if (location == 0 || location == 1)
            {
                int colnum = rnum.Next(1, col - 1); // 입구 좌표를 생성하기 위한 난수
                if (location == 0)
                {
                    int enteerCol = colnum % col;
                    loc2D = new Location2D(0, enteerCol);
                    if (enter == null)
                    {
                        return loc2D;
                    }// 입구 생성 단계인 경우 
                    else
                    {
                        if(enteerCol == enter.Col)
                        {
                            return CreateEnterExit(row, col, enter);
                        }// 입구와 출구의 좌표가 같다면 순환
                        else
                        {
                            return loc2D;
                        }// 입구와 출구의 좌표가 다르면 반환
                    }// 출구 생성 단계인 경우
                }// 입구와 출구가 위에 생성이 되어야 하는 경우
                else
                {
                    int enteerCol = colnum % col;
                    loc2D = new Location2D(row - 1, enteerCol);
                    if (enter == null)
                    {
                        return loc2D;
                    }// 입구 생성 단계인 경우 
                    else
                    {
                        if (enteerCol == enter.Col)
                        {
                            return CreateEnterExit(row, col, enter);
                        }// 입구와 출구의 좌표가 같다면 순환
                        else
                        {
                            return loc2D;
                        }// 입구와 출구의 좌표가 다르면 반환
                    }// 출구 생성 단계인 경우
                }// 입구와 출구가 아래 생성이 되어야 하는 경우
            }
            else
            {
                int rownum = rnum.Next(1, row - 1); // 입구 좌표를 생성하기 위한 난수
                if (location == 2)
                {
                    int enterRow = rownum % row;
                    loc2D = new Location2D(enterRow, 0);
                    if (enter == null)
                    {
                        return loc2D;
                    }// 입구 생성 단계인 경우 
                    else
                    {
                        if (enterRow == enter.Row)
                        {
                            return CreateEnterExit(row, col, enter);
                        }// 입구와 출구의 좌표가 같다면 순환
                        else
                        {
                            return loc2D;
                        }// 입구와 출구의 좌표가 다르면 반환
                    }// 출구 생성 단계인 경우
                }// 입구와 출구가 좌측에 생성이 되어야 하는 경우
                else
                {
                    int enterRow = rownum % row;
                    loc2D = new Location2D(enterRow, col - 1);
                    if (enter == null)
                    {
                        return loc2D;
                    }// 입구 생성 단계인 경우 
                    else
                    {
                        if (enterRow == enter.Row)
                        {
                            return CreateEnterExit(row, col, enter);
                        }// 입구와 출구의 좌표가 같다면 순환
                        else
                        {
                            return loc2D;
                        }// 입구와 출구의 좌표가 다르면 반환
                    }// 출구 생성 단계인 경우
                }// 입구와 출구가 우측에 생성이 되어야 하는 경우
            }
        }
    }
}
