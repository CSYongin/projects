﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Maze
{
    class BFSSolve : Solve
    {
        public BFSSolve(OpenMaze openmaze)
        {
            //Maze copymaze;
            maze = openmaze;
            mazeQueue = new Queue<Maze>();
            locationStack = new Stack<Location2D>();
            int crosscnt = 0;
            Console.WriteLine("- 입구 - 미로 탐색 시작");
            maze.display();
            //mazeQueue.Enqueue(copyMaze(maze));
            locationStack.Push(copyLocation((int)maze.EnterRow, (int)maze.EnterCol));
            while (locationStack.Count != 0)
            {
                Location2D here = locationStack.Pop();
                if (maze.getCurLoc(here) == 9)
                {
                    Console.WriteLine("- 출구 - 미로 탐색 종료");
                    maze.display();
                    maze.printFile();
                    //mazeQueue.Enqueue(copyMaze(maze));
                    int cnt = 0;
                    foreach (Maze resultMaze in mazeQueue)
                    {
                        if (cnt == 0)
                        {
                            Console.WriteLine("- 입구 - 미로 탐색 시작");
                        }
                        else if (cnt == mazeQueue.Count - 1)
                        {
                            Console.WriteLine("- 출구 - 미로 탐색 종료");
                        }
                        else
                        {
                            Console.WriteLine("{0}번째 교차점", cnt);
                        }
                        //resultMaze.display();
                        cnt++;
                    }
                    return;
                }
                else
                {
                    changeCurLoc(here);
                    if (ValidLoc(here) > 1)
                    {
                        Console.WriteLine("{0}번째 교차점", ++crosscnt);
                        maze.display();
                        //mazeQueue.Enqueue(copyMaze(maze));
                        if (isValidLoc(here.Row - 1, here.Col)) { locationStack.Push(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationStack.Push(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationStack.Push(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationStack.Push(copyLocation(here.Row, here.Col + 1)); }
                    }
                    else
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { locationStack.Push(copyLocation(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { locationStack.Push(copyLocation(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { locationStack.Push(copyLocation(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { locationStack.Push(copyLocation(here.Row, here.Col + 1)); }
                    }
                }
            }
        } //스택으로 구현한 미로 해결방법

        public void changeCurLoc(Location2D entry)
        {
            if (maze.getCurLoc(entry) == 8 || maze.getCurLoc(entry) == 9) { }
            else
            {
                if (ValidLoc(entry) > 1) 
                {
                    maze.setCurLoc(entry, "cross");
                }// 교차점인 경우
                else if (ValidLoc(entry) == 1) 
                {
                    maze.setCurLoc(entry, "path");
                }// 교차점이 아닌 갈 수 있는 경로
                else 
                {
                    maze.setCurLoc(entry, "block");
                }// 더이상 갈 수 없는 경로
            }
        } //해당entry에 해당되는 좌표를 지나온경로 표시
        public bool isValidLoc(int row, int col)
        {
            if (row < 0 || col < 0 || row >= maze.Row || col >= maze.Col) { return false; }
            else
            {
                if (maze.getCurLoc(row, col) == 0 || maze.getCurLoc(row, col) == 9) { return true; }
                else { return false; }
            }
        } //해당 좌표에서 진행이 가능한지 불가능 한지 판단
        public int ValidLoc(Location2D entry)
        {
            int validPath = 0;
            if (isValidLoc(entry.Row - 1, entry.Col))
            {
                validPath++; //위로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row + 1, entry.Col))
            {
                validPath++; // 아래로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row, entry.Col - 1))
            {
                validPath++; // 좌로 진행 가능할 경우
            }
            if (isValidLoc(entry.Row, entry.Col + 1))
            {
                validPath++; // 우로 진행 가능할 경우
            }
            return validPath;
        } //해당entry에 해당되는 좌표가 이동가능한 경로의 갯수 판단
    }
}
