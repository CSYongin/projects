#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include "Location2D.h"
using namespace std;

int max_row, max_col;
/*
struct Location2D
{
	int row;
	int col;
	Location2D(int r = 0, int c = 0) {
		row = r;
		col = c;
		//printf("입구 위치 = (%d,%d)", row, col);
	}
	bool isNeighbor(Location2D& p) {
		return (row == p.row && (col == p.col - 1 || col == p.col + 1)) || (col == p.col && (row == p.row - 1 || row == p.row + 1));
	}
	bool operator==(Location2D& p) {
		return row == p.row && col == p.col;
	}
	int getCurRow() {
		return row;
	}
	int getCurCol() {
		return col;
	}
};
*/
class CreateMaze
{
	/*
	각 숫자의 의미
	0 : 미로가 만들어졌을때 처음에 비어있는 길
	1 : 미로가 만들어졌을때 벽
	8 : 미로가 만들어졌을때 입구
	9 : 미로가 만들어졌을때 출구
	4 : 지나온 길
	5 : 교차점
	*/
	int ccnt = 0, rcnt = 1;
	int row, col;
	int** mat;
	int enterRow = NULL, enterCol = NULL; // 입구를 설정할지 출구를 설정할지 판별하는 요소
public:
	//CreateMaze(char* filename, int rows, int cols);
	CreateMaze(char* filename);
	~CreateMaze();

	int getColumCnt();
	int getRowCnt();
	int getEnterCol();
	int getEnterRow();
	int getCurLoc(int r, int c); //현재의 위치를 entry을 넘겨받아 해당 entry의 위치 정보를 return
	int getCurLoc(Location2D entry); //현재의 위치를 entry을 넘겨받아 해당 entry의 위치 정보를 return
	void changeCurLoc(Location2D entry); //진행 경로 확인후 현재 경로를 변경함
	bool isValidLoc(int r, int c); //진행 가능 경로 확인
	int getValidLoc(Location2D entry); //진행 가능 경로의 갯수 확인
	void display();
};
/*
CreateMaze::CreateMaze(char* filename, int rows, int cols)
	:row(rows),col(cols)
{
	if (rows == 0 || cols == 0) {
		printf("미로를 만들 조건이 만족하지 않습니다.\n");
	}
	mat = new int*[rows];
	for (int i = 0; i < rows; i++) {
		mat[i] = new int[cols];
	}
	FILE* fp = fopen(filename, "r");
	if (fp == nullptr) {
		cout << "해당 파일이 존재하지 않습니다." << endl;
	}
	char entry;
	int i = 0, j = 0;
	while ((entry = getc(fp)) != EOF) {
		if (i == 0 && entry == '0') {
			mat[i][j] = 8;
			j++;
		}
		else if (i != 0 && j == 0 && entry == '0') {
			mat[i][j] = 8;
			j++;
		}
		else if (i != 0 && j == col - 1 && entry == '0') {
			mat[i][j] = 9;
			j++;
		}
		else if (i == row - 1 && entry == '0') {
			mat[i][j] = 9;
			j++;
		}
		else {
			if (entry == '1') {
				mat[i][j] = 1;
				j++;
			}
			else if (entry == '0') {
				mat[i][j] = 0;
				j++;
			}
			else if (entry == '\n') {
				j = 0;
				i++;
			}
		}
	}
}
*/
CreateMaze::CreateMaze(char* filename)
{
	FILE* fp = fopen(filename, "r");
	if (fp == nullptr) {
		cout << "해당 파일이 존재하지 않습니다." << endl;
	}
	char entry;
	while ((entry = getc(fp)) != EOF) {
		if (entry == '\n') {
			rcnt++;
		}
		else {
			ccnt++;
		}
	}
	//cout << rcnt << "x" << ccnt << endl;
	ccnt = ccnt / rcnt;
	//cout << rcnt << "x" << ccnt << endl;
	max_row = row = rcnt;
	max_col = col = ccnt;
	if (row == 0 || col == 0) {
		printf("미로를 만들 조건이 만족하지 않습니다.\n");
	}
	mat = new int*[max_row];
	for (int i = 0; i < max_row; i++) {
		mat[i] = new int[max_col];
	}
	fp = fopen(filename, "r");
	int tempcol = 0, temprow = 0;
	//cout << "미로 생성 시작" << endl;
	while ((entry = getc(fp)) != EOF) {
		if (temprow == 0 && entry == '0') { // 맨 위첫줄에 0이 있다면
			if (enterRow == NULL&&enterCol == NULL) {
				enterRow = temprow;
				enterCol = tempcol;
				mat[temprow][tempcol] = 8;
				tempcol++;
			}
			else {
				mat[temprow][tempcol] = 9;
				tempcol++;
			}
		}
		else if (temprow != 0 && tempcol == 0 && entry == '0') { //맨 윗줄이 아니고, 맨 왼쪽에 0이 있다면
			if (enterRow == NULL&&enterCol == NULL) {
				enterRow = temprow;
				enterCol = tempcol;
				mat[temprow][tempcol] = 8;
				tempcol++;
			}
			else {
				mat[temprow][tempcol] = 9;
				tempcol++;
			}
		}
		else if (temprow != 0 && tempcol == col - 1 && entry == '0') { // 맨 윗줄이 아니고, 맨 오른족에 0이 있다면
			if (enterRow == NULL&&enterCol == NULL) {
				enterRow = temprow;
				enterCol = tempcol;
				mat[temprow][tempcol] = 8;
				tempcol++;
			}
			else {
				mat[temprow][tempcol] = 9;
				tempcol++;
			}
		}
		else if (temprow == row-1 && entry == '0') { //맨 아랫줄에 0이 있다면
			if (enterRow == NULL&&enterCol == NULL) {
				enterRow = temprow;
				enterCol = tempcol;
				mat[temprow][tempcol] = 8;
				tempcol++;
			}
			else {
				mat[temprow][tempcol] = 9;
				tempcol++;
			}
		}
		else {
			if (entry == '1') {
				mat[temprow][tempcol] = 1;
				tempcol++;
			}
			else if (entry == '0') {
				mat[temprow][tempcol] = 0;
				tempcol++;
			}
			else if (entry == '\n') {
				tempcol = 0;
				temprow++;
			}
		}
	}
	//cout << "row = " << temprow << " col = " << tempcol << endl;
	//cout << "미로 생성 완료" << endl;
}

CreateMaze::~CreateMaze()
{
}

int CreateMaze::getColumCnt() {
	return ccnt;
}

int CreateMaze::getRowCnt() {
	return rcnt;
}

int CreateMaze::getEnterCol() {
	return enterCol;
}
int CreateMaze::getEnterRow() {
	return enterRow;
}

int CreateMaze::getCurLoc(int r, int c) {
	return mat[r][c];
}

int CreateMaze::getCurLoc(Location2D entry) {
	return mat[entry.getCurRow()][entry.getCurCol()];
}

void CreateMaze::changeCurLoc(Location2D entry) {
	if (getCurLoc(entry) == 8|| getCurLoc(entry) == 9) {
	}
	else {
		if (getValidLoc(entry) > 1) { // 교차점인 경우
			mat[entry.getCurRow()][entry.getCurCol()] = 5;

			//cout << "교차점 처리 완료" << endl;
		}
		else if (getValidLoc(entry) == 1) { //교차점이 아닌 경로인 경우
			mat[entry.getCurRow()][entry.getCurCol()] = 4;
			//cout << "지나온 경로 처리 완료" << endl;
		}
	}
	
}

bool CreateMaze::isValidLoc(int r, int c) {
	if (r < 0 || c < 0 || r >= max_row || c >= max_col) {
		return false;
	}
	else {
		if (mat[r][c] == 0 || mat[r][c] == 9) {
			return true;
		}
		else {
			return false;
		}
	}
}

int CreateMaze::getValidLoc(Location2D entry) {
	int validPath = 0;
	if (isValidLoc(entry.getCurRow() - 1, entry.getCurCol())) {
		validPath++; // 위로 진행 가능하면
	}
	if (isValidLoc(entry.getCurRow() + 1, entry.getCurCol())) {
		validPath++; // 아래로 진행 가능하면
	}
	if (isValidLoc(entry.getCurRow(), entry.getCurCol() - 1)) {
		validPath++; // 왼쪽으로 진행 가능하면
	}
	if (isValidLoc(entry.getCurRow(), entry.getCurCol() + 1)) {
		validPath++; // 오른쪽으로 진행 가능하면
	}
	return  validPath;
}

void CreateMaze::display() {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (mat[i][j] == 1) { //벽을 표시
				printf("■");
			}
			else if (mat[i][j] == 0) { //지나갈수 있는 길을 표시
				printf("□");
			}
			else if (mat[i][j] == 8) { //입구를 표시
				printf("☆");
			}
			else if (mat[i][j] == 9) { //출구를 표시
				printf("★");
			}
			else if (mat[i][j] == NULL) {
				printf("■");
			}
			else if (mat[i][j] == 5) { //교차점 표시
				printf("⊙");
			}
			else if (mat[i][j] == 4) { //지나온 길 표시
				printf("▨");
			}
		}
		printf("\n");
	}
}

