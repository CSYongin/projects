#pragma once
#include "LocTreeNode.h"
#include "CircularQueue.h"

class LocTree
{
protected:
	LocTreeNode* root;
public:
	LocTree();
	~LocTree();

	void setRoot(LocTreeNode* node);
	LocTreeNode* getRoot();
	bool isEmpty();
	void levelorder();
};

LocTree::LocTree()
	:root(nullptr)
{
}

LocTree::~LocTree()
{
}

void LocTree::setRoot(LocTreeNode* node) {
	root = node;
}

LocTreeNode* LocTree::getRoot() {
	return root;
}

bool LocTree::isEmpty() {
	return root == nullptr;
}

void LocTree::levelorder() {
	if (!isEmpty()) {
		CircularQueue q;
		q.enqueue(root);
		while (!q.isEmpty())
		{
			LocTreeNode* n = q.dequeue();
			if (n != nullptr) {
				printf(" (%d,%d) ", n->getCurRow(), n->getCurCol());
				q.enqueue(n->getTop());
				q.enqueue(n->getDonw());
				q.enqueue(n->getLeft());
				q.enqueue(n->getRight());
			}
		}
	}
	printf("\n");
}