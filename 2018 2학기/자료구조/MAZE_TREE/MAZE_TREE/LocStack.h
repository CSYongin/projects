#pragma once
#include <stdio.h>
#include "Location2D.h"

#define MAX_STACK_SIZE 50

class LocStack
{
	int top;
	Location2D data[MAX_STACK_SIZE];
public:
	LocStack();
	~LocStack();

	bool isEmpty();
	bool isFull();
	void push(Location2D entity);
	Location2D pop();
	Location2D peek();
	void getsize();
	void display();
};

LocStack::LocStack()
{
	top = -1;
}

LocStack::~LocStack()
{
}

bool LocStack::isEmpty() {
	return top == -1;
}

bool LocStack::isFull() {
	return top == MAX_STACK_SIZE - 1;
}

void LocStack::push(Location2D entity) {
	if (isFull()) {
		printf("스택 포화 에러");
	}
	data[++top] = entity;
}

Location2D LocStack::pop() {
	if (isEmpty()) {
		printf("스택 공백 에러");
	}
	return data[top--];
}

Location2D LocStack::peek() {
	if (isEmpty()) {
		printf("스택 공백 에러");
	}
	return data[top];
}

void LocStack::getsize() {
	printf("현재 스택 갯수 = %d\n", top);
}