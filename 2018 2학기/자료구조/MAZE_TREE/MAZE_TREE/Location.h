#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

class Location
{
	int row;
	int col;
public:
	Location(int r = 0, int c = 0);
	~Location();

private:

};

Location::Location(int r, int c)
	:row(r),col(c)
{
}

Location::~Location()
{
}