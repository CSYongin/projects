#pragma once
#include "MazeNode.h"

class MazeLinkedStack
{
	int cnt = 0;
	MazeNode* top;
public:
	MazeLinkedStack();
	~MazeLinkedStack();

	bool isEmpty() { return top == nullptr; };
	void push(MazeNode* entry);
	MazeNode* pop();
	MazeNode* peek();
	void display();
};

MazeLinkedStack::MazeLinkedStack()
{
	top = nullptr;
}

MazeLinkedStack::~MazeLinkedStack()
{
	while (isEmpty()) {
		delete pop();
	}
}

void MazeLinkedStack::push(MazeNode* entry) {
	if (isEmpty()) {
		top = entry;
		cnt++;
		printf("현재 저장된 교차점의 수 = %d\n", cnt);
		for (MazeNode* p = top; p != nullptr; p = p->getLink()) {
			printf("현재 저장된 미로의 행크기 = %d, 열크기 = %d\n", p->getRowSize(), p->getColSize());
		}
	}
	else {
		entry->setLink(top);
		top = entry;
		cnt++;
		printf("현재 저장된 교차점의 수 = %d\n", cnt);
		for (MazeNode* p = top; p != nullptr; p = p->getLink()) {
			printf("현재 저장된 미로의 행크기 = %d, 열크기 = %d\n", p->getRowSize(), p->getColSize());
		}
	}
}
MazeNode* MazeLinkedStack::pop() {
	if (isEmpty()) {
		return nullptr;
	}
	else {
		MazeNode* entry = top;
		top = top->getLink();
		return entry;
	}
}
MazeNode* MazeLinkedStack::peek() {
	return top;
}

