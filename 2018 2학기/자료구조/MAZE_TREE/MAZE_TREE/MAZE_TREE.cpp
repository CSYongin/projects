// MAZE_TREE.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "CreateMaze.h"
#include "LocLinkedStack.h"
#include "MazeLinkedStack.h"
#include "MazeLinkedQueue.h"
#include <stdio.h>
#include <stdlib.h>

#include "LocTree.h"
#include "LocSrcTree.h"

using namespace std;

int** copyMaze(int** tempMaze, CreateMaze maze, int mazeMaxRow, int mazeMaxCol) {
	tempMaze = new int*[mazeMaxRow];
	for (int i = 0; i < mazeMaxRow; i++) {
		tempMaze[i] = new int[mazeMaxCol];
	}
	for (int i = 0; i < mazeMaxRow; i++) {
		for (int j = 0; j < mazeMaxCol; j++) {
			tempMaze[i][j] = maze.getCurLoc(i, j);
		}
	}
	return tempMaze;
}

void main()
{
	CreateMaze maze = CreateMaze("mazetest4.txt");
	//maze.display();
	int mazeMaxRow = maze.getRowCnt();
	int mazeMaxCol = maze.getColumCnt();
	int ** tempMaze = nullptr;
	int** copyMaze(int** tempMaze, CreateMaze maze, int mazeMaxRow, int mazeMaxCol);

	/*
	LocTree tree;
	LocTreeNode *exit = new LocTreeNode(new Location2DNode(5, 10), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n16d = new LocTreeNode(new Location2DNode(2, 11), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n15r2 = new LocTreeNode(new Location2DNode(4, 10), nullptr, exit, nullptr, nullptr);
	LocTreeNode *n15r1 = new LocTreeNode(new Location2DNode(1, 11), nullptr, n16d, nullptr, nullptr);
	LocTreeNode *n14d = new LocTreeNode(new Location2DNode(4, 9), nullptr, nullptr, nullptr, n15r2);
	LocTreeNode *n14r = new LocTreeNode(new Location2DNode(1, 10), nullptr, nullptr, nullptr, n15r1);
	LocTreeNode *n13d = new LocTreeNode(new Location2DNode(3, 9), nullptr, n14d, nullptr, nullptr);
	LocTreeNode *n13r = new LocTreeNode(new Location2DNode(1, 9), nullptr, nullptr, nullptr, n14r);
	LocTreeNode *n12r = new LocTreeNode(new Location2DNode(2, 9), nullptr, n13d, nullptr, nullptr);
	LocTreeNode *n12u = new LocTreeNode(new Location2DNode(1, 8), nullptr, nullptr, nullptr, n13r);
	LocTreeNode *n12d = new LocTreeNode(new Location2DNode(4, 7), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n11r = new LocTreeNode(new Location2DNode(2, 8), n12u, nullptr, nullptr, n12r);
	LocTreeNode *n11d = new LocTreeNode(new Location2DNode(3, 7), nullptr, n12d, nullptr, nullptr);
	LocTreeNode *n10r = new LocTreeNode(new Location2DNode(2, 7), nullptr, n11d, nullptr, n11r);
	LocTreeNode *n10u = new LocTreeNode(new Location2DNode(1, 6), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n9r = new LocTreeNode(new Location2DNode(2, 6), n10u, nullptr, nullptr, n10r);
	LocTreeNode *n8d = new LocTreeNode(new Location2DNode(4, 5), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n8u = new LocTreeNode(new Location2DNode(2, 5), nullptr, nullptr, nullptr, n9r);
	LocTreeNode *n7r = new LocTreeNode(new Location2DNode(3, 5), n8u, n8d, nullptr, nullptr);
	LocTreeNode *n6r = new LocTreeNode(new Location2DNode(3, 4), nullptr, nullptr, nullptr, n7r);
	LocTreeNode *n5d = new LocTreeNode(new Location2DNode(3, 3), nullptr, nullptr, nullptr, n6r);
	LocTreeNode *n4d = new LocTreeNode(new Location2DNode(2, 3), nullptr, n5d, nullptr, nullptr);
	LocTreeNode *n3r = new LocTreeNode(new Location2DNode(1, 3), nullptr, n4d, nullptr, nullptr);
	LocTreeNode *n3d = new LocTreeNode(new Location2DNode(3, 1), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n2r = new LocTreeNode(new Location2DNode(1, 2), nullptr, nullptr, nullptr, n3r);
	LocTreeNode *n2d = new LocTreeNode(new Location2DNode(2, 1), nullptr, n3d, nullptr, nullptr);
	LocTreeNode *n1 = new LocTreeNode(new Location2DNode(1, 1), nullptr, n2d, nullptr, n2r);
	LocTreeNode *root = new LocTreeNode(new Location2DNode(maze.getEnterRow(), maze.getEnterCol()),nullptr,nullptr,nullptr, n1);
	tree.setRoot(root);
	tree.levelorder();
	*/

	LocSrcTree tree;
	LocTreeNode *exit = new LocTreeNode(new Location2DNode(5, 10), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n16d = new LocTreeNode(new Location2DNode(2, 11), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n15r2 = new LocTreeNode(new Location2DNode(4, 10), nullptr, exit, nullptr, nullptr);
	LocTreeNode *n15r1 = new LocTreeNode(new Location2DNode(1, 11), nullptr, n16d, nullptr, nullptr);
	LocTreeNode *n14d = new LocTreeNode(new Location2DNode(4, 9), nullptr, nullptr, nullptr, n15r2);
	LocTreeNode *n14r = new LocTreeNode(new Location2DNode(1, 10), nullptr, nullptr, nullptr, n15r1);
	LocTreeNode *n13d = new LocTreeNode(new Location2DNode(3, 9), nullptr, n14d, nullptr, nullptr);
	LocTreeNode *n13r = new LocTreeNode(new Location2DNode(1, 9), nullptr, nullptr, nullptr, n14r);
	LocTreeNode *n12r = new LocTreeNode(new Location2DNode(2, 9), nullptr, n13d, nullptr, nullptr);
	LocTreeNode *n12u = new LocTreeNode(new Location2DNode(1, 8), nullptr, nullptr, nullptr, n13r);
	LocTreeNode *n12d = new LocTreeNode(new Location2DNode(4, 7), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n11r = new LocTreeNode(new Location2DNode(2, 8), n12u, nullptr, nullptr, n12r);
	LocTreeNode *n11d = new LocTreeNode(new Location2DNode(3, 7), nullptr, n12d, nullptr, nullptr);
	LocTreeNode *n10r = new LocTreeNode(new Location2DNode(2, 7), nullptr, n11d, nullptr, n11r);
	LocTreeNode *n10u = new LocTreeNode(new Location2DNode(1, 6), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n9r = new LocTreeNode(new Location2DNode(2, 6), n10u, nullptr, nullptr, n10r);
	LocTreeNode *n8d = new LocTreeNode(new Location2DNode(4, 5), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n8u = new LocTreeNode(new Location2DNode(2, 5), nullptr, nullptr, nullptr, n9r);
	LocTreeNode *n7r = new LocTreeNode(new Location2DNode(3, 5), n8u, n8d, nullptr, nullptr);
	LocTreeNode *n6r = new LocTreeNode(new Location2DNode(3, 4), nullptr, nullptr, nullptr, n7r);
	LocTreeNode *n5d = new LocTreeNode(new Location2DNode(3, 3), nullptr, nullptr, nullptr, n6r);
	LocTreeNode *n4d = new LocTreeNode(new Location2DNode(2, 3), nullptr, n5d, nullptr, nullptr);
	LocTreeNode *n3r = new LocTreeNode(new Location2DNode(1, 3), nullptr, n4d, nullptr, nullptr);
	LocTreeNode *n3d = new LocTreeNode(new Location2DNode(3, 1), nullptr, nullptr, nullptr, nullptr);
	LocTreeNode *n2r = new LocTreeNode(new Location2DNode(1, 2), nullptr, nullptr, nullptr, n3r);
	LocTreeNode *n2d = new LocTreeNode(new Location2DNode(2, 1), nullptr, n3d, nullptr, nullptr);
	LocTreeNode *n1 = new LocTreeNode(new Location2DNode(1, 1), nullptr, n2d, nullptr, n2r);
	LocTreeNode *root = new LocTreeNode(new Location2DNode(maze.getEnterRow(), maze.getEnterCol()), nullptr, nullptr, nullptr, n1);
	tree.setRoot(root);
	tree.levelorder();
	cout << n2d << endl;
	tree.search(new Location2DNode(2, 1));
	/*
	tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);

	MazeLinkedQueue mazeQ;
	mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol));

	LocLinkedStack LocS;
	LocS.push(new Location2DNode(maze.getEnterRow(), maze.getEnterCol()));
	while (LocS.isEmpty() == false)
	{
		Location2DNode* here = LocS.pop();
		//printf("현재 위치 : (%2d,%2d)\n", here->getCurRow(), here->getCurCol());
		if (maze.getCurLoc(*here) == 9) {
			//tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);
			//mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol)); // 교차점 상태시 미로큐에 저장
			//mazeQ.printFile();
			//mazeQ.display();
			return;
		}
		else {
			maze.changeCurLoc(*here);
			if (maze.getValidLoc(*here) > 1) {
				//cout << "현재 위치는 교차점 입니다." << endl;
				//tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);
				//mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol)); // 교차점 상태시 미로큐에 저장


				if (maze.isValidLoc(here->getCurRow() - 1, here->getCurCol())) {
					//printf("위 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow() - 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow() + 1, here->getCurCol())) {
					//printf("아래 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow() + 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() - 1)) {
					//printf("왼쪽 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() - 1));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() + 1)) {
					//printf("오른쪽 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() + 1));
				}
			}
			else {
				if (maze.isValidLoc(here->getCurRow() - 1, here->getCurCol())) {
					//printf("위 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow() - 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow() + 1, here->getCurCol())) {
					//printf("아래 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow() + 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() - 1)) {
					//printf("왼쪽 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() - 1));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() + 1)) {
					//printf("오른쪽 이동 가능\n");
					//LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() + 1));
				}
			}
		}
	}
	*/
}

