#pragma once
#include "Location2D.h"

class Location2DNode : public Location2D
{
	Location2DNode* link;
public:
	Location2DNode(int r = 0, int c = 0);
	~Location2DNode();

	Location2DNode* getLink() {
		return link;
	}
	void setLink(Location2DNode* entry) {
		link = entry;
	}
};

Location2DNode::Location2DNode(int r, int c)
	:Location2D(r,c)
{
	link = nullptr;
}

Location2DNode::~Location2DNode()
{
}