#pragma once
#include "Maze.h"

class MazeNode : public Maze
{
	MazeNode* link;
public:
	MazeNode(int** mazeentry, int r, int c);
	~MazeNode();

	MazeNode* getLink() {
		return link;
	}
	void setLink(MazeNode* entry) {
		link = entry;
	}
};

MazeNode::MazeNode(int** mazeentry, int r, int c)
	:Maze(mazeentry,r,c)
{
	link = nullptr;
}

MazeNode::~MazeNode()
{
}
