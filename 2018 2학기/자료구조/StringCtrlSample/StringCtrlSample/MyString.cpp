#include "stdafx.h"
#include "MyString.h"

CMyString::CMyString()
	: m_pszData(NULL)
	, m_nLength(0)
{
}
CMyString::CMyString(const CMyString &rhs)
	: m_pszData(NULL)
	, m_nLength(0) 
{
	this->SetString(rhs.GetString());
}

CMyString::~CMyString()
{
}

int CMyString::SetString(const char* pszParam)
{
	Release();
	// 기존 정보 전부 초기화

	if (pszParam == NULL) {
		return 0;
	}

	int nLength = strlen(pszParam);
	
	if (nLength == 0) {
		return 0;
	}

	//만약 받아온 문자열이 공백이거나, 아무것도 없을경우(strlen이 0인 경우) 프로그램 종료

	m_pszData = new char[nLength + 1];

	strcpy_s(m_pszData, sizeof(char)*(nLength + 1), pszParam);
	//strcpy에 문자열의 길이를 +1을 하는 경우는 배열의 마지막에 NULL 즉 \0이 들어오는경우를 생각해서 더한다. 
	//만약 치환할 문자열의 길이가 원본대상과 같을 경우 에러를 발생함
	//strcpy_s의 용법 => strcpy_s(치환할 대상, 길이, 원본대상)
	m_nLength = nLength;

	return nLength;
}

const char* CMyString::GetString() const
{
	return m_pszData;
}

void CMyString::Release()
{
	if (m_pszData != NULL) {
		delete[] m_pszData;
	}
	m_pszData = NULL;
	m_nLength = 0;
}

CMyString & CMyString::operator=(const CMyString & rhs)
{
	if (this != &rhs) {
		this->SetString(rhs.GetString());
	}

	return *this;
}
