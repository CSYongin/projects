﻿namespace MAZE_CSharp
{
    public class Location2D
    {
        public Location2D(int row = 0, int col = 0)
        {
            Row = row;
            Col = col;
        }
        public bool isNeighbor(Location2D p)
        {
            return (Row == p.Row && (Col == p.Col - 1 || Col == p.Col + 1)) || (Col == p.Col && (Row == p.Row - 1 || Row == p.Row + 1));
        }
        public int Row { get; set; }
        public int Col { get; set; }
    }
}