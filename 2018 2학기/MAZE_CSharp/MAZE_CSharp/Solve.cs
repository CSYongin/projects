﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAZE_CSharp
{
    interface ISolve
    {
        Maze copyMaze(Maze curMaze);
        Location2D copyLocation(int row, int col);
    }
    class Solve : ISolve
    {
        protected Stack<Location2D> locationStack;
        protected Queue<Maze> mazeQueue;
        protected OpenMaze maze;
        public Maze copyMaze(Maze curMaze)
        {
            Maze tempMaze = new Maze();
            tempMaze = curMaze;
            return tempMaze;
        }
        public Location2D copyLocation(int row, int col)
        {
            Location2D tempLocation = new Location2D(row, col);
            return tempLocation;
        }
    }
}
