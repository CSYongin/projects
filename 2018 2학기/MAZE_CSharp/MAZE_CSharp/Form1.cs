﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAZE_CSharp
{
    public partial class Form1 : Form
    {
        OpenMaze open;
        LinkedStackSolve solve;
        int mazeRow = 0, mazeCol = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void CreateMazeBtn_Click(object sender, EventArgs e)
        {
            mazeRow = Int32.Parse(RowTB.Text);
            mazeCol = Int32.Parse(ColTB.Text);
            MessageBox.Show("미로의 크기 : " + mazeRow + " X " + mazeCol);
        }
    }
}
