﻿namespace MAZE_CSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateMazeBtn = new System.Windows.Forms.Button();
            this.RowTB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ColTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CreateMazeBtn
            // 
            this.CreateMazeBtn.Location = new System.Drawing.Point(147, 78);
            this.CreateMazeBtn.Name = "CreateMazeBtn";
            this.CreateMazeBtn.Size = new System.Drawing.Size(75, 23);
            this.CreateMazeBtn.TabIndex = 0;
            this.CreateMazeBtn.Text = "미로생성";
            this.CreateMazeBtn.UseVisualStyleBackColor = true;
            this.CreateMazeBtn.Click += new System.EventHandler(this.CreateMazeBtn_Click);
            // 
            // RowTB
            // 
            this.RowTB.Location = new System.Drawing.Point(94, 18);
            this.RowTB.Name = "RowTB";
            this.RowTB.Size = new System.Drawing.Size(100, 21);
            this.RowTB.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ColTB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.CreateMazeBtn);
            this.groupBox1.Controls.Add(this.RowTB);
            this.groupBox1.Location = new System.Drawing.Point(864, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 109);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // ColTB
            // 
            this.ColTB.Location = new System.Drawing.Point(94, 51);
            this.ColTB.Name = "ColTB";
            this.ColTB.Size = new System.Drawing.Size(100, 21);
            this.ColTB.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "미로행의크기";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "미로열의크기 ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 598);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CreateMazeBtn;
        private System.Windows.Forms.TextBox RowTB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ColTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}