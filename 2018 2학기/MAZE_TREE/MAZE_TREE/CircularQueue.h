#pragma once
#include "LocTreeNode.h"
#include <stdlib.h>
#include <iostream>

#define MAX_QUEUE_SIZE 1000
using namespace std;

class CircularQueue
{
private:
	int front;
	int rear;
	LocTreeNode* data[MAX_QUEUE_SIZE];
public:
	CircularQueue();
	~CircularQueue();

	bool isEmpty();
	bool isFull();
	void enqueue(LocTreeNode* entry);
	LocTreeNode* dequeue();
};

CircularQueue::CircularQueue()
{
	front = rear = 0;
}

CircularQueue::~CircularQueue()
{
}

bool CircularQueue::isEmpty() {
	return front == rear;
}

bool CircularQueue::isFull() {
	return (rear + 1) % MAX_QUEUE_SIZE == front;
}

void CircularQueue::enqueue(LocTreeNode* entry) {
	if (isFull()) {
		cout << "큐가 포화상태입니다." << endl;
	}
	else {
		rear = (rear + 1) % MAX_QUEUE_SIZE;
		data[rear] = entry;
	}
}

LocTreeNode* CircularQueue::dequeue() {
	if (isEmpty()) {
		cout << "큐가 공백상태입니다." << endl;
	}
	else {
		front = (front + 1) % MAX_QUEUE_SIZE;
		return data[front];
	}
}