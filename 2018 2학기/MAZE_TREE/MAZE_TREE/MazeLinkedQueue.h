#pragma once
#include "MazeNode.h"

class MazeLinkedQueue
{
	int cnt = 0;
	MazeNode* front;
	MazeNode* rear;
public:
	MazeLinkedQueue();
	~MazeLinkedQueue();

	bool isEmpty();
	void enqueue(MazeNode* entry);
	MazeNode* dequeue();
	MazeNode* peek();
	void printFile();
	void display();
};

MazeLinkedQueue::MazeLinkedQueue()
	:front(nullptr),rear(nullptr)
{
}

MazeLinkedQueue::~MazeLinkedQueue()
{
}

bool MazeLinkedQueue::isEmpty() {
	return front == nullptr;
}

void MazeLinkedQueue::enqueue(MazeNode* entry) {
	if (isEmpty()) {
		front = rear = entry;
		cnt++;
		/*
		printf("현재 저장된 교차점의 수 = %d\n", ++cnt);
		for (MazeNode* p = front; p != nullptr; p = p->getLink()) {
			printf("현재 저장된 미로의 행크기 = %d, 열크기 = %d\n", p->getRowSize(), p->getColSize());
		}
		*/
	}
	else {
		rear->setLink(entry);
		rear = entry;
		cnt++;
		/*
		printf("현재 저장된 교차점의 수 = %d\n", ++cnt);
		for (MazeNode* p = front; p != nullptr; p = p->getLink()) {
			printf("현재 저장된 미로의 행크기 = %d, 열크기 = %d\n", p->getRowSize(), p->getColSize());
		}
		*/
	}
}

MazeNode* MazeLinkedQueue::dequeue() {
	if (isEmpty()) {
		return nullptr;
	}
	MazeNode* entry = front;
	front = front->getLink();
	if (front == nullptr) {
		rear = nullptr;
	}
	return entry;
}

MazeNode* MazeLinkedQueue::peek() {
	return front;
}

void MazeLinkedQueue::printFile() {
	MazeNode* p = rear;
	p->printFile();
}

void MazeLinkedQueue::display() {
	int mazecnt = 0;
	for (MazeNode* p = front; p != nullptr; p = p->getLink()) {
		if (mazecnt == 0) {
			printf("미로 출발\n");
			mazecnt++;
		}
		else if(mazecnt == cnt-1){
			printf("미로 탈출\n");
			mazecnt++;
		}
		else {
			printf("%2d번째 교차점 도착\n", mazecnt++);
		}
		p->display();
	}
}