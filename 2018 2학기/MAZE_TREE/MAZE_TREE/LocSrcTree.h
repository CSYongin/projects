#pragma once
#include "LocTree.h"

class LocSrcTree : public LocTree
{
public:
	LocSrcTree(void);
	~LocSrcTree(void);

	LocTreeNode* search(Location2DNode *entry);
	LocTreeNode* searchRecur(LocTreeNode* node, Location2DNode* entry);
};

LocSrcTree::LocSrcTree(void)
{
}

LocSrcTree::~LocSrcTree(void)
{
}

LocTreeNode* LocSrcTree::search(Location2DNode *entry) {
	LocTreeNode* node = searchRecur(root, entry);
	if (node != nullptr) {
		printf("탐색 성공 : 좌표가 (%d,%d) 인 노드 = %p\n", node->getCurRow(), node->getCurCol(), node);
	}
	else {
		printf("탐색 실패 : 좌표가 (%d,%d) 인 노드가 없습니다. \n", node->getCurRow(), node->getCurCol());
	}
	return node;
}

LocTreeNode* LocSrcTree::searchRecur(LocTreeNode* node, Location2DNode* entry) {
	if (node == nullptr) {
		return nullptr;
	}
	if (node->getCurRow() == entry->getCurRow() && node->getCurCol() == entry->getCurCol()) { //좌표값이 같으면 
		printf("1111현재 좌표(%d,%d)\n", node->getCurRow(), node->getCurCol());
		return node;
	}
	else if (node->getCurRow() > entry->getCurRow() && node->getCurCol() == entry->getCurCol()) { //현재 위치보다 위에 있을 경우
		printf("2222현재 좌표(%d,%d)\n", node->getCurRow(), node->getCurCol());
		return searchRecur(node->getTop(), entry);
	}
	else if (node->getCurRow() < entry->getCurRow() && node->getCurCol() == entry->getCurCol()) { //현재 위치보다 아래 있을 경우
		printf("3333현재 좌표(%d,%d)\n", node->getCurRow(), node->getCurCol());
		return searchRecur(node->getDonw(), entry);
	}
	else if (node->getCurRow() == entry->getCurRow() && node->getCurCol() > entry->getCurCol()) { //현재 위치보다 좌측에 있을 경우
		printf("4444현재 좌표(%d,%d)\n", node->getCurRow(), node->getCurCol());
		return searchRecur(node->getLeft(), entry);
	}
	else if (node->getCurRow() == entry->getCurRow() && node->getCurCol() < entry->getCurCol()) { //현재 위치보다 우측에 있을 경우
		printf("5555현재 좌표(%d,%d)\n", node->getCurRow(), node->getCurCol());
		return searchRecur(node->getRight(), entry);
	}
}
