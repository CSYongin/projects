#pragma once
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class FileFunc
{
	int ccnt = 1, rcnt = 0;
public:
	FileFunc();
	~FileFunc();

	void ReadMaze(char* filename);
	int getColumCnt();
	int getRowCnt();
};

FileFunc::FileFunc()
{
}

FileFunc::~FileFunc()
{
}

void FileFunc::ReadMaze(char* filename) {
	FILE* fp = fopen(filename, "r");
	if (fp == nullptr) {
		cout << "해당 파일이 존재하지 않습니다." << endl;
	}
	char entry;
	while ((entry = getc(fp)) != EOF) {
		if (entry == '\n') {
			ccnt++;
		}
		else {
			rcnt++;
		}
	}
	rcnt = rcnt / ccnt;
}

int FileFunc::getColumCnt() {
	return ccnt;
}

int FileFunc::getRowCnt() {
	return rcnt;
}