#pragma once
#include <stdio.h>
#include "Location2D.h"
#include "Location2DNode.h"

class LocTreeNode : public Location2D
{
protected:
	Location2DNode entry;
	LocTreeNode* top; // 위쪽 자식 노드의 포인터
	LocTreeNode* down; // 아래쪽 자식 노드의 포인터
	LocTreeNode* left; // 왼쪽 자식 노드의 포인터
	LocTreeNode* right; // 우측 자식 노드의 포인터
public:
	LocTreeNode(Location2DNode* entry= nullptr, LocTreeNode* top = nullptr, LocTreeNode* down = nullptr, LocTreeNode* left = nullptr, LocTreeNode* right = nullptr);
	~LocTreeNode();

	void setEntry(Location2DNode e);
	void setTop(LocTreeNode* t);
	void setDown(LocTreeNode* d);
	void setLeft(LocTreeNode* l);
	void setRight(LocTreeNode* r);
	Location2DNode getEntry();
	LocTreeNode* getTop();
	LocTreeNode* getDonw();
	LocTreeNode* getLeft();
	LocTreeNode* getRight();
	bool isLeaf();
};

LocTreeNode::LocTreeNode(Location2DNode* entry, LocTreeNode* t, LocTreeNode* d, LocTreeNode* l, LocTreeNode* r)
	:Location2D(entry->getCurRow(), entry->getCurCol()),top(t),down(d),left(l),right(r)
{
	//cout << "트리에 접근 성공" << endl;
	//cout << entry->getCurRow() << "," << entry->getCurCol() << endl;
	//cout << "트리의 주소 = " << entry << endl;
}

LocTreeNode::~LocTreeNode()
{
}

void LocTreeNode::setEntry(Location2DNode e) {
	entry = e;
}

void LocTreeNode::setTop(LocTreeNode* t) {
	top = t;
}

void LocTreeNode::setDown(LocTreeNode* d){
	down = d;
}

void LocTreeNode::setLeft(LocTreeNode* l){
	left = l;
}

void LocTreeNode::setRight(LocTreeNode* r){
	right = r;
}

Location2DNode LocTreeNode::getEntry() {
	return entry;
}

LocTreeNode* LocTreeNode::getTop() {
	return top;
}

LocTreeNode* LocTreeNode::getDonw() {
	return down;
}

LocTreeNode* LocTreeNode::getLeft() {
	return left;
}

LocTreeNode* LocTreeNode::getRight() {
	return right;
}

bool LocTreeNode::isLeaf() {
	return top == nullptr&&down == nullptr&&left == nullptr&&right == nullptr;
}