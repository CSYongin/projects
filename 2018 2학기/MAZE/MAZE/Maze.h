#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Maze
{
	int** maze;
	int row, col;
public:
	Maze(int** mazeentry = nullptr, int r = 0, int c = 0);
	~Maze();

	int getRowSize() {
		return row;
	}
	int getColSize() {
		return col;
	}
	void printFile();
	void display();
};

Maze::Maze(int** mazeentry, int r, int c)
	:maze(mazeentry),row(r),col(c)
{
}

Maze::~Maze()
{
}

void Maze::printFile() {
	FILE* pfp = fopen("resultmaze.txt", "w");
	if (pfp == nullptr) {
		printf("파일을 생성할수 없습니다.");
	}
	fputs("미로를 탈출한 이동한 경로를 표시 합니다.\n", pfp);
	fputs("■는 벽 □는 이동 가능한 경로를 표시합니다.\n", pfp);
	fputs("☆는 입구 ★는 출구를 표시합니다.\n", pfp);
	fputs("⊙는 교차점 ▨는 지나온 경로를 표시합니다.\n", pfp);
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (maze[i][j] == 1) { //벽을 표시
				//printf("■");
				fputs("■", pfp);
			}
			else if (maze[i][j] == 0) { //지나갈수 있는 길을 표시
				//printf("□");
				fputs("□", pfp);
			}
			else if (maze[i][j] == 8) { //입구를 표시
				//printf("☆");
				fputs("☆", pfp);
			}
			else if (maze[i][j] == 9) { //출구를 표시
				//printf("★");
				fputs("★", pfp);
			}
			else if (maze[i][j] == NULL) {
				//printf("■");
				fputs("■", pfp);
			}
			else if (maze[i][j] == 5) { //교차점 표시
				//printf("⊙");
				fputs("⊙", pfp);
			}
			else if (maze[i][j] == 4) { //지나온 길 표시
				//printf("▨");
				fputs("▨", pfp);
			}
		}
		fputs("\n", pfp);
	}
	fclose(pfp);
}

void Maze::display() {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (maze[i][j] == 1) { //벽을 표시
				printf("■");
			}
			else if (maze[i][j] == 0) { //지나갈수 있는 길을 표시
				printf("□");
			}
			else if (maze[i][j] == 8) { //입구를 표시
				printf("☆");
			}
			else if (maze[i][j] == 9) { //출구를 표시
				printf("★");
			}
			else if (maze[i][j] == NULL) {
				printf("■");
			}
			else if (maze[i][j] == 5) { //교차점 표시
				printf("⊙");
			}
			else if (maze[i][j] == 4) { //지나온 길 표시
				printf("▨");
			}
		}
		printf("\n");
	}
}