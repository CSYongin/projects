#pragma once
#include "Location2DNode.h"

class LocLinkedStack
{
	Location2DNode* top;
public:
	LocLinkedStack();
	~LocLinkedStack();

	bool isEmpty() { return top == nullptr; };
	void push(Location2DNode* entry);
	Location2DNode* pop();
	Location2DNode* peek();
};

LocLinkedStack::LocLinkedStack()
{
	top = nullptr;
}

LocLinkedStack::~LocLinkedStack()
{
	while (isEmpty()) {
		delete pop();
	}
}

void LocLinkedStack::push(Location2DNode* entry) {
	if (isEmpty()) {
		top = entry;
	}
	else {
		entry->setLink(top);
		top = entry;
	}
}
Location2DNode* LocLinkedStack::pop() {
	if (isEmpty()) {
		return nullptr;
	}
	else {
		Location2DNode* entry = top;
		top = top->getLink();
		return entry;
	}
}
Location2DNode* LocLinkedStack::peek() {
	return top;
}