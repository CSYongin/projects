// MAZE.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "CreateMaze.h"
#include "LocLinkedStack.h"
#include "MazeLinkedStack.h"
#include "MazeLinkedQueue.h"
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int** copyMaze(int** tempMaze, CreateMaze maze, int mazeMaxRow, int mazeMaxCol) {
	tempMaze = new int*[mazeMaxRow];
	for (int i = 0; i < mazeMaxRow; i++) {
		tempMaze[i] = new int[mazeMaxCol];
	}
	for (int i = 0; i < mazeMaxRow; i++) {
		for (int j = 0; j < mazeMaxCol; j++) {
			tempMaze[i][j] = maze.getCurLoc(i, j);
		}
	}
	return tempMaze;
}

void main()
{
	CreateMaze maze = CreateMaze("mazetest2.txt");
	//maze.display();
	int mazeMaxRow = maze.getRowCnt();
	int mazeMaxCol = maze.getColumCnt();
	int ** tempMaze = nullptr;
	int** copyMaze(int** tempMaze, CreateMaze maze, int mazeMaxRow, int mazeMaxCol);
	tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);
	/*
	tempMaze = new int*[mazeMaxRow];
	for (int i = 0; i < mazeMaxRow; i++) {
		tempMaze[i] = new int[mazeMaxCol];
	}
	for (int i = 0; i < mazeMaxRow; i++) {
		for (int j = 0; j < mazeMaxCol; j++) {
			tempMaze[i][j] = maze.getCurLoc(i, j);
		}
	}
	*/
	//MazeLinkedStack mazeS;
	//mazeS.push(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol));
	MazeLinkedQueue mazeQ;
	mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol));
	/*
	cout << "다시 가져온 미로 확인" << endl;
	for (int i = 0; i < mazeMaxRow; i++) {
		for (int j = 0; j < mazeMaxCol; j++) {
			printf("%d", tempMaze[i][j]);
		}
		printf("\n");
	}
	cout << "다시 가져온 미로 확인완료" << endl;
	*/
	/*
	//스택으로 구현
	Location2D entry(maze.getEnterRow(),maze.getEnterCol());
	maze.display();
	LocStack LocS;
	LocS.push(entry);
	while (LocS.isEmpty()==false)
	{
		Location2D here = LocS.pop();
		printf("현재 위치 : (%2d,%2d)\n", here.getCurRow(), here.getCurCol());
		if (maze.getCurLoc(here) == 9) {
			printf("출구\n");
			maze.display();
			return;
		}
		else if (maze.getCurLoc(here) == 4 || maze.getCurLoc(here) == 5) {

		}
		else {
			maze.changeCurLoc(here);
			if (maze.getValidLoc(here) > 1) {
				cout << "현재 위치는 교차점 입니다." << endl;
				if (maze.isValidLoc(here.getCurRow() - 1, here.getCurCol())) {
					printf("위 이동 가능\n");
					LocS.push(Location2D(here.getCurRow() - 1, here.getCurCol()));
				}
				if (maze.isValidLoc(here.getCurRow() + 1, here.getCurCol())) {
					printf("아래 이동 가능\n");
					LocS.push(Location2D(here.getCurRow() + 1, here.getCurCol()));
				}
				if (maze.isValidLoc(here.getCurRow(), here.getCurCol() - 1)) {
					printf("왼쪽 이동 가능\n");
					LocS.push(Location2D(here.getCurRow(), here.getCurCol() - 1));
				}
				if (maze.isValidLoc(here.getCurRow(), here.getCurCol() + 1)) {
					printf("오른쪽 이동 가능\n");
					LocS.push(Location2D(here.getCurRow(), here.getCurCol() + 1));
				}
			}
			else {
				if (maze.isValidLoc(here.getCurRow() - 1, here.getCurCol())) {
					printf("위 이동 가능\n");
					LocS.push(Location2D(here.getCurRow() - 1, here.getCurCol()));
				}
				if (maze.isValidLoc(here.getCurRow() + 1, here.getCurCol())) {
					printf("아래 이동 가능\n");
					LocS.push(Location2D(here.getCurRow() + 1, here.getCurCol()));
				}
				if (maze.isValidLoc(here.getCurRow(), here.getCurCol() - 1)) {
					printf("왼쪽 이동 가능\n");
					LocS.push(Location2D(here.getCurRow(), here.getCurCol() - 1));
				}
				if (maze.isValidLoc(here.getCurRow(), here.getCurCol() + 1)) {
					printf("오른쪽 이동 가능\n");
					LocS.push(Location2D(here.getCurRow(), here.getCurCol() + 1));
				}
			}
		}
	}
	*/
	//연결리스트 스택으로 구현
	LocLinkedStack LocS;
	LocS.push(new Location2DNode(maze.getEnterRow(), maze.getEnterCol()));
	while (LocS.isEmpty() == false)
	{
		Location2DNode* here = LocS.pop();
		//printf("현재 위치 : (%2d,%2d)\n", here->getCurRow(), here->getCurCol());
		if (maze.getCurLoc(*here) == 9) {
			tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);
			mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol)); // 교차점 상태시 미로큐에 저장
			mazeQ.printFile();
			mazeQ.display();
			return;
		}
		else {
			maze.changeCurLoc(*here);
			if (maze.getValidLoc(*here) > 1) {
				//cout << "현재 위치는 교차점 입니다." << endl;
				tempMaze = copyMaze(tempMaze, maze, mazeMaxRow, mazeMaxCol);
				mazeQ.enqueue(new MazeNode(tempMaze, mazeMaxRow, mazeMaxCol)); // 교차점 상태시 미로큐에 저장
				if (maze.isValidLoc(here->getCurRow() - 1, here->getCurCol())) {
					//printf("위 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow() - 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow() + 1, here->getCurCol())) {
					//printf("아래 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow() + 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() - 1)) {
					//printf("왼쪽 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() - 1));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() + 1)) {
					//printf("오른쪽 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() + 1));
				}
			}
			else {
				if (maze.isValidLoc(here->getCurRow() - 1, here->getCurCol())) {
					//printf("위 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow() - 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow() + 1, here->getCurCol())) {
					//printf("아래 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow() + 1, here->getCurCol()));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() - 1)) {
					//printf("왼쪽 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() - 1));
				}
				if (maze.isValidLoc(here->getCurRow(), here->getCurCol() + 1)) {
					//printf("오른쪽 이동 가능\n");
					LocS.push(new Location2DNode(here->getCurRow(), here->getCurCol() + 1));
				}
			}
		}
	}
}

