#pragma once

/*
struct Location2D
{
	int row;
	int col;
	Location2D(int r = 0, int c = 0) {
		row = r;
		col = c;
		//printf("�Ա� ��ġ = (%d,%d)", row, col);
	}
	bool isNeighbor(Location2D& p) {
		return (row == p.row && (col == p.col - 1 || col == p.col + 1)) || (col == p.col && (row == p.row - 1 || row == p.row + 1));
	}
	bool operator==(Location2D& p) {
		return row == p.row && col == p.col;
	}
	int getCurRow() {
		return row;
	}
	int getCurCol() {
		return col;
	}
};
*/
class Location2D
{
protected:
	int row;
	int col;
public:
	Location2D(int r = 0, int c = 0);
	~Location2D();

	bool isNeighbor(Location2D& p) {
		return (row == p.row && (col == p.col - 1 || col == p.col + 1)) || (col == p.col && (row == p.row - 1 || row == p.row + 1));
	}
	bool operator==(Location2D& p) {
		return row == p.row && col == p.col;
	}
	int getCurRow() {
		return row;
	}
	int getCurCol() {
		return col;
	}
};

Location2D::Location2D(int r, int c)
	:row(r),col(c)
{
}

Location2D::~Location2D()
{
}