<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/menu/showProducts/showProductsInclude.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/showProduct.css?ver=4">
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<%
					for(int i=0;i<productlist.size();i++){
						product = new ProductVO();
						product = productlist.get(i);
						item_img = product.getItem_img();
						pr_id = product.getPr_id();
						pr_name = product.getPr_name();
						pr_price = product.getPr_price();
						seller_id = product.getSeller_id();
						seller_name = product.getSeller_name();
					%>
					<div class="productWrapper">
						<div class="div1">
							<a href="/FreeProject/menu/showProducts/showProductDetail.jsp?pr_id=<%=pr_id%>"><img src="<%=item_img%>" style="width: 100px"></a>
						</div>	
						<div class="div2">
							<div class="div2_1">
								<a href="/FreeProject/menu/showProducts/showProductDetail.jsp?pr_id=<%=pr_id%>"><p><%=pr_name %></p></a>
							</div>
							<div class="div2_2">
								<p><%=pr_price %></p>
							</div>
						</div>	
						<div class="div3">
							<p><%=seller_name %></p>
						</div>							
					</div>
					<hr/>
					<%
					}
					%>
				</div>
			</div>
		</div>
	</body>
</html>