<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="vo.*,java.util.*" import="serviceSet.*" import="dao.*"%>
<%
	String cur_path = request.getRequestURI().toString();
	String pr_id = request.getParameter("pr_id");
	ProductService producService = ProductService.getInstance();
	ProductVO productinfo = producService.productShowProductDetail(pr_id);

	Boolean isLogin = false;
	if(session.isNew()||session.getAttribute("id")==null){
		isLogin = false;
	}else{
		isLogin = true;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/showProductDetail.css">
		<%-- <link rel="stylesheet" type="text/css" href="/FreeProject/css/showProductDetail.css"> --%>
		<script type="text/javascript">
			var pr_single_price = <%= productinfo.getPr_price()%>;
			var isLogin = <%=isLogin%>;
			console.log(isLogin);
			$(function(){
				$("#buy_cnt").change(function(){
					$("#buy_products_price").html(pr_single_price*$("#buy_cnt").val()+"원")
				});
				$("#product_to_cart").click(function(){
					if(!isLogin){
						var login_confirm = confirm("로그인이 필요한 서비스 입니다.\n로그인페이지로 이동 하시겠습니까?");
						if(login_confirm){
							var url = location.href;
							var parameters = (url.slice(url.indexOf('?')+1, url.length))
							location.href=("/FreeProject/member/login/login.jsp?URI=<%=cur_path%>?"+parameters);
						}
					}else{
						$.ajax({
							async : false,
							type : 'POST',
							url : "/FreeProject/Ajax/product_to_cart.jsp",
							dataType : "json",
							data : {
								cart_pr_id : $("#pr_id").val(),
								buy_cnt : $("#buy_cnt").val()
							},
							success : function(data){
								if(data.insertcartchk=="true"){
									var cart_show_chk = confirm("장바구니에 물건이 답겼습니다.\n장바구니로 가시겠습니까?");
									if(cart_show_chk){
										location.href="/FreeProject/menu/buyer/cart.jsp";
									}else{
										return false;
									}
								}else{
									alert("장바구니에 넣는데 실패했습니다.")
								}
							},
							error : function(error){
								alert("ajax 실행 실패");
								return false;
							}
						});
					}
				})
			});
		</script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="ProductInfoTop">

					</div>
					<div class="ProductInfoWrapper">
						<div class="ProductImg">
							<img src="<%= productinfo.getItem_img()%>" style="width:400px; float:left;">
						</div>
						<div class="ProductInfoContent">
							<div class="ProductInfo">
								<div>
									<input type="hidden" name="pr_id" id="pr_id"  value="<%= productinfo.getPr_id()%>">
									<h4><%= productinfo.getPr_name()%></h4>
								</div>
								<div>
									<h5><%= productinfo.getPr_price()%>원</h5>
								</div>
								<div>
									<div>
										<input type="number" name="buy_cnt" id="buy_cnt" value="1" required="" min="1">
									</div>
									<div>
										<h6 id="buy_products_price">총 금액<%= productinfo.getPr_price()%>원</h6>
									</div>
								</div>
							</div>
							<div class="SelectProductOrder">
								<input type="image" src="/FreeProject/img/cart.png" value="장바구니" name="product_to_cart" id="product_to_cart">
								<input type="image" src="/FreeProject/img/buy_1.png"value="바로구매">
							</div>
						</div>
					</div>
					<div  class="contents">
							상세정보
					</div>
					<div class="SellerInfo">
						<div class="SellerInfoTable">
							<table>
								<thead>
									<tr>
										<td></td>
										<td></td>
								    </tr>
								</thead>
								<tbody>
									<tr>
										<td>판매자</td>
										<td><%=productinfo.getSeller_name()%></td>
									</tr>
									<tr>
										<td>전화번호</td>
										<td><%=productinfo.getSeller_phone()%></td>
									</tr>
									<tr>
										<td>이메일</td>
										<td><%=productinfo.getSeller_email()%></td>
									</tr>
									<tr>
										<td>주소</td>
										<td>(<%=productinfo.getSeller_postcode()%>)
											<%=productinfo.getSeller_addr1()%>
											<%=productinfo.getSeller_addr2()%>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>