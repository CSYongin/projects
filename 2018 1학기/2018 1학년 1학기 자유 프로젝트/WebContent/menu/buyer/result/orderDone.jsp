<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, vo.*, dao.*, serviceSet.*" %>
<%
	int orderNo = Integer.parseInt(request.getParameter("OrderNO"));
	OrderService OService = OrderService.getInstance();
	ArrayList<OrderedDataVO> orderedlist = OService.orderDone(orderNo);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/orderDone.css">
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="finish">
						<h1>결제가 완료 되었습니다:)</h1>
					</div>
					<table>
						<thead>
							<tr>
								<th>주문번호</th>
								<th colspan="2">상품정보</th>
								<th>구매 수량</th>
								<th>구매 가격</th>
								<th>판매자</th>
							</tr>
						</thead>
						<tbody>
							<%
							for(OrderedDataVO ordereddata:orderedlist){
							%>
							<tr>
								<th><%=ordereddata.getOrder_seq() %></th>
								<th><img src="<%=ordereddata.getItem_img() %>" style="width:150px"></th>
								<th><%=ordereddata.getPr_name() %></th>
								<th><%=ordereddata.getBuy_cnt() %></th>
								<th><%=(ordereddata.getBuy_cnt()*ordereddata.getPr_price()) %></th>
								<th><%=ordereddata.getSeller_name() %></th>
							</tr>
							<%
							}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>