
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="vo.*,java.util.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	ArrayList<CartVO> Cartlist = (ArrayList<CartVO>)(session.getAttribute(session.getAttribute("id")+"_cart"));
%>
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/cart.css?ver=4">
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="cartTopInfo">
						<p>장바구니</p>
					</div>
					<div class="cartWrapper">
						<table>
							<thead>
								<tr>
									<td></td>
									<td>상품정보</td>
									<td>가격</td>
									<td>개수</td>
									<td>총가격</td>
									<td>판매자</td>
								</tr>
							</thead>
							<tbody>
								<%
								for(CartVO Cart:Cartlist){
									//System.out.println(Cart);
									String cart_id = Cart.getPr_id();
									int pr_cnt = Cart.getBuy_cnt();
									//System.out.println(cart_id+"/"+pr_cnt);
									ProductService producService = ProductService.getInstance();
									ProductVO productinfo = producService.productShowProductDetail(cart_id);
									%>
									<tr>
										<td class="td1" rowspan="2"><img src="<%=productinfo.getItem_img() %>" style="width:100px"></td>
										<td class="td2"><%=productinfo.getItem_name() %></td>
										<td class="td3" rowspan="2"><%=productinfo.getPr_price() %></td>
										<td class="td4" rowspan="2"><%=pr_cnt %></td>
										<td class="td3" rowspan="2"><%=productinfo.getPr_price()*pr_cnt %></td>
										<td class="td6" rowspan="2"><%=productinfo.getSeller_name() %></td>
									</tr>
									<tr>
										<td  style="border:1px solid black">선택한 상품</td>
									</tr>
									<%
								}
								%>
							</tbody>
						</table>
					</div>
					<div class="cartTotal">
						<input type="image" src="/FreeProject/img/order.png" value="주문하기"  onclick="location.href='/FreeProject/menu/buyer/order.jsp'" id="orderbtn">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>