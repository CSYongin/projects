<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="vo.*,java.util.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	ArrayList<CartVO> Cartlist = (ArrayList<CartVO>)(session.getAttribute(session.getAttribute("id")+"_cart"));
	String myinfo_id = session.getAttribute("id").toString();
	MemberService MemService = MemberService.getInstance();
	MemberVO member = MemService.memberMyInfoSearch(myinfo_id);
%>
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/order.css">
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="orderTopInfo">
						<h1>주문하기</h1>
					</div>
					<h2>1. 주문상품</h2>
						<div class="cartWrapper">
							<table>
								<thead>
									<tr>
										<td></td>
										<td>상품정보</td>
										<td>가격</td>
										<td>개수</td>
										<td>총가격</td>
										<td>판매자</td>
									</tr>
								</thead>
								<tbody>
									<%
									for(CartVO Cart:Cartlist){
										System.out.println(Cart);
										String cart_id = Cart.getPr_id();
										int pr_cnt = Cart.getBuy_cnt();
										System.out.println(cart_id+"/"+pr_cnt);
										ProductService producService = ProductService.getInstance();
										ProductVO productinfo = producService.productShowProductDetail(cart_id);
										%>
										<tr>
											<td class="td1" rowspan="2"><img src="<%=productinfo.getItem_img() %>" style="width:100px"></td>
											<td class="td2"><%=productinfo.getItem_name() %></td>
											<td class="td3" rowspan="2"><%=productinfo.getPr_price() %></td>
											<td class="td4" rowspan="2"><%=pr_cnt %></td>
											<td class="td3" rowspan="2"><%=productinfo.getPr_price()*pr_cnt %></td>
											<td class="td6" rowspan="2"><%=productinfo.getSeller_name() %></td>
										</tr>
										<tr>
											<td  style="border:1px solid black">선택한 상품</td>
										</tr>
										<%
									}
									%>
								</tbody>
							</table>
						</div>
					<h2>2.배송지 정보</h2>
						<div class="deliverWrapper">
							<table>
						 		<tbody>
									<tr>
										<th>
											<span class="bnk">아이디</span>
										</th>
										<td>
											<span class="id">
												<%=member.getUser_id()%>
											</span>
										</td>
										<th>
											<span class="bnk">성명</span>
										</th>
										<td>
											<span class="name"><%=member.getUser_name()%></span>
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">전화번호</span>
										</th>
											<td>
												<%=member.getUser_phone1() %><%=member.getUser_phone2() %><%=member.getUser_phone3() %>
											</td>			
									</tr>
									<tr>
										<th>
											<span class="dot">주소</span>
										</th>
										<td colspan="3">
											<input type="text" name="my_info_postcode" id="daum_postcode" placeholder="우편번호" readonly value="<%=member.getUser_postcode()%>">
											<input type="button" onclick="execDaumPostcode();" value="우편번호 찾기" name=""><br>
											<input type="text" name="my_info_addr1" id="daum_addr1" placeholder="주소" readonly value="<%=member.getUser_addr1()%>">
											<input type="text" name="my_info_addr2" id="daum_addr2" placeholder="상세주소" value="<%=member.getUser_addr2()%>">
											<input type="hidden" name="addr_chk" id="addr_chk" value="true">
										</td>
									</tr>
								</tbody>
							</table>						
						</div>	
					<div class="orderTotal">
						<input type="image" src="/FreeProject/img/pay.png" value="결제하기" onclick="location.href='/FreeProject/orderprocess.OrderProc'" id="paybtn">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>