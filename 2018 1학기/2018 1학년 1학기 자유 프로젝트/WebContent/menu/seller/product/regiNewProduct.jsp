<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/regiNewProduct.css">
		<script type="text/javascript">
			$(function(){
				$("#item_sel_btn").click(function(){
					var win_select_item = window.open("/FreeProject/search.IProc?class=all&pop=Y" ,"_blank","width=800,height=450,resizable=no,scrollbars=no,status=no");
				});
			});
		</script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="RegiProductMain">
						<form action="/FreeProject/regi.producProc" method="POST">	
							<h1>판매 물품을 등록해주세요</h1>
							<div class="RowGroup">
								<div class="ItemImgRow">
									<img name="item_img" id="item_img" src="" style="width: 100px">
								</div>
								<div class="ItemInfoRow">
									<label for="">itemNo.</label>
									<input type="text" name="item_id" id="item_id" readonly>
								</div>
								<div class="ItemInfoSelbtn">
									<input type="button" value="품목선택" name="item_sel_btn" id="item_sel_btn">
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<label for="new_pr_id">판매상품번호</label>
									<input type="text" name="new_pr_id" id="new_pr_id" autocomplete="off">
								</div>
								<div class="Join_Row">
									<label for="new_pr_name">판매상품명</label>
									<input type="text" name="new_pr_name" id="new_pr_name" autocomplete="off">
								</div>
								<div class="Join_Row">
									<label for="">판매수량</label>
									<input type="text" name="new_pr_cnt" id="new_pr_cnt" autocomplete="off">
								</div>
								<div class="Join_Row">
									<label for="">판매금액</label>
									<input type="text" name="new_pr_price" id="new_pr_price" autocomplete="off">
								</div>
							</div>
							<div>
								<input type="submit" value="판매물품 등록" name="regi_product_submit" id="regi_product_submit">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>