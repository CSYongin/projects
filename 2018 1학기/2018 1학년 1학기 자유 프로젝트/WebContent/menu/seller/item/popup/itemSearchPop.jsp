<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<!-- jQuery  -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<!-- bootstrap JS -->
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<!-- bootstrap CSS -->
		<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
		<title>Insert title here</title>
		<script>
			$(function(){
				$(".item_sel_btn").click(function(){
					var str=""
					var tdArr = new Array();
					var checkbtn = $(this);

					// checkBtn.parent() : checkBtn의 부모는 <td>이다.
		            // checkBtn.parent().parent() : <td>의 부모이므로 <tr>이다.
		            var tr = checkbtn.parent().parent();
		            var td = tr.children();

					var img_path = td.eq(0).children().attr('src');
		            var no = td.eq(1).text();

		            $(opener.document).find("#item_img").attr('src',img_path);
		            $(opener.document).find("#item_id").attr('value',no);
		            window.close();
				});
			});
		</script>
	</head>
	<body>
		<%
			ArrayList<ItemVO> itemlist = (ArrayList<ItemVO>)(request.getAttribute("itemlist"));
			ItemVO item = null;
			String item_id, item_name, item_manufac, item_calss, item_manu, item_img;
		%>
		<table>
			<thead>
				<tr>
					<th>품목이미지</th>
					<th>품목번호</th>
					<th>품목이름</th>
					<th>제조사</th>
					<th>대분류</th>
					<th>브랜드분류</th>
					<th>선택</th>
				</tr>
			</thead>
			<tbody>
		<%
			for(int i=0; i<itemlist.size(); i++){
				item = new ItemVO();
				item = itemlist.get(i);
				item_id = item.getItem_id();
				item_name = item.getItem_name();
				item_manufac = item.getItem_manu();
				item_calss = item.getItem_class_string();
				item_manu = item.getItem_manu_string();
				item_img = item.getItem_img();
		%>
				<tr>
					<td id="<%=i%>_img"><img src="<%=item_img%>" style="width: 100px"></td>
					<td id="<%=i%>_id"><%=item_id%></td>
					<td id="<%=i%>_name"><%=item_name%></td>
					<td id="<%=i%>_manufac"><%=item_manufac%></td>
					<td id="<%=i%>_calss"><%=item_calss%></td>
					<td id="<%=i%>_manu"><%=item_manu%></td>
					<td><input type="button" class="item_sel_btn"></td>
				</tr>
		<%
			}
		%>
			</tbody>
		</table>
	</body>
</html>