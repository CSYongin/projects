<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/JSPcontroller/grantcheck_a_s.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/regiNewItem.css">
		<!--jsp, jquery, json file-->
		<script src="/FreeProject/Jquery/itemRegiChk.js"></script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="RegiItemMain">
						<h1>품목을 등록해주세요</h1>
						<form action="/FreeProject/JSPcontroller/itemRegiControll.jsp" method="POST" id="reginewitemform" enctype="multipart/form-data">
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="ImgShowBox" style="width: 200px;">
										<img style="max-width: 100%" id="item_preview_img" >
									</div>
									<div class="ImgInputBox">
										<p class="img_uload_title">이미지 업로드</p>
										<input type="file" name="item_img_input_btn" id="item_img_input_btn" >
										<input type="hidden" name="new_item_img_chk" id="new_item_img_chk" value="false">
									</div>
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<label for="new_item_name">물품명</label>
									<input type="text" name="new_item_name" id="new_item_name" autocomplete="off">
								</div>
								<div class="Join_Row">
									<label for="new_item_manufac">제조사</label>
									<input type="text" name="new_item_manufac" id="new_item_manufac" autocomplete="off">
								</div>
								<div class="Join_Row">
									<label for="item_manu_select">제품군</label>
									<select name="item_manu_select" id="item_manu_select">
										<option value="" selected>선택하세요</option>
										<option value="0">악세사리</option>
										<option value="1">APPLE</option>
										<option value="2">SAMSUNG</option>
										<option value="3">LG</option>
										<option value="4">SONY</option>
										<option value="5">XIAOMI</option>
										<option value="6">ETC</option>
									</select>
									<input type="hidden" name="new_item_manu" id="new_item_manu">
									<input type="hidden" name="new_item_manu_chk" id="new_item_manu_chk" value="false">
								</div>
								<div class="Join_Row">
									<label for="item_class_select">대분류</label>
									<select name="item_class_select" id="item_class_select">
										<option value="" selected>선택하세요</option>
										<option value="1">휴대폰 케이스</option>
										<option value="2">필름,글라스</option>
										<option value="3">케이블</option>
										<option value="4">거치대</option>
										<option value="5">충전기</option>
										<option value="6">파우치</option>
										<option value="7">기타</option>
									</select>
									<input type="hidden" name="new_item_class" id="new_item_class">
									<input type="hidden" name="new_item_class_chk" id="new_item_class_chk" value="false">
								</div>
							</div>
							<div class="ErrorMsg" id="submitMsg" name="submitMsg" style="display: none">입력하신 정보를 다시 확인해 주세요.</div>
							<div>
								<input type="submit" value="등록" name="regi_item_submit" id="regi_item_submit">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>