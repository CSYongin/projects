<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setContentType("text/html; charset=utf-8"); %>
<%
	String pr_id = request.getParameter("cart_pr_id");
	int buy_cnt = Integer.parseInt(request.getParameter("buy_cnt"));
	boolean pw_chk_result = false;
	
	ArrayList<CartVO> cartlist = null;
	CartVO cart = null;
	
	if(session.getAttribute(session.getAttribute("id").toString()+"_cart")==null){
		cartlist = new ArrayList<CartVO>();
		cart = new CartVO(pr_id, buy_cnt);
		cartlist.add(cart);
		session.setAttribute(session.getAttribute("id").toString()+"_cart",cartlist);
		pw_chk_result = true;
		System.out.println(session.getAttribute(session.getAttribute("id").toString()+"_cart"));
	}else{
		cartlist = (ArrayList<CartVO>)session.getAttribute(session.getAttribute("id").toString()+"_cart");
		cart = new CartVO(pr_id, buy_cnt);
		cartlist.add(cart);
		session.setAttribute(session.getAttribute("id").toString()+"_cart",cartlist);
		pw_chk_result = true;
		System.out.println(session.getAttribute(session.getAttribute("id").toString()+"_cart"));
	}
	if(pw_chk_result){
		out.print("{\"insertcartchk\" :\"true\"}");
	}else{
		out.print("{\"insertcartchk\" :\"false\"}");
	}
%>