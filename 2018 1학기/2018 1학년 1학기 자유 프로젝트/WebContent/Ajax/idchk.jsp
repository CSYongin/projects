<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="vo.*" import="serviceSet.*" import="dao.*"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setContentType("text/html; charset=utf-8"); %>
<%
	String chk_id_val = request.getParameter("check_id");

	MemberService MemService = MemberService.getInstance();
	boolean chk_id_result = MemService.chkid(chk_id_val);
		
	if(chk_id_result==true){
		//회원 가입 불가능한 id, 이미 id 존재함
		out.print("{\"idchk\" :\"false\"}");
	}else{
		//회원 가능한 id
		out.print("{\"idchk\" :\"true\"}");
	}
%>