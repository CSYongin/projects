<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String header_curPath = (String)request.getRequestURI();
//현재 경로를 구해서 로그인창에 같이 보낸 후, 로그인을 하고 나면 해당 페이지로 바로 로그인 상태로 이동하기 위한 현재 경로 구하는 변수
String header_display_cur_user, header_display_login, header_display_logout, header_display_signup, header_display_admin_menu, header_display_seller_menu;
String header_mypage_path, header_myinfo_path;
String header_cur_session_id="", header_cur_session_name="", header_cur_session_grant="", header_cur_session_classify="", header_myorderinfo_path="";

if(session.getAttribute("id")!=null){
	//로그인 되어 있을 경우
	header_cur_session_id = session.getAttribute("id").toString();
	header_cur_session_name = session.getAttribute(header_cur_session_id+"_Name").toString();
	header_cur_session_grant = session.getAttribute(header_cur_session_id+"_Grant").toString();
	header_cur_session_classify = session.getAttribute(header_cur_session_id+"_classify").toString();

	header_display_admin_menu = "none";
	header_display_seller_menu = "none";
	header_display_cur_user = "block";
	header_display_login = "none";
	header_display_logout = "block";
	header_display_signup = "none";
	header_mypage_path = "/FreeProject/member/mypage/Index.jsp";
	header_myinfo_path = "/FreeProject/member/mypage/membership/myInfo.jsp";
	header_myorderinfo_path = "/FreeProject/member/mypage/membership/myOrderInfo.jsp";

	if(header_cur_session_grant.equals("admin")){
		header_display_admin_menu = "block";
		header_display_seller_menu = "block";
	}else if(header_cur_session_grant.equals("seller")){
		header_display_admin_menu = "none";
		header_display_seller_menu = "block";
	}
}else{
	//로그인이 안되어 있을 경우
	header_display_admin_menu = "none";
	header_display_seller_menu = "none";
	header_display_cur_user = "none";
	header_display_login = "block";
	header_display_logout = "none";
	header_display_signup = "block";
	header_mypage_path = "/FreeProject/member/login/login.jsp?URI=/FreeProject/member/mypage/Index.jsp";
	header_myinfo_path = "/FreeProject/member/login/login.jsp?URI=/FreeProject/member/mypage/membership/myInfo.jsp";
	header_myorderinfo_path = "/FreeProject/member/login/login.jsp?URI=/FreeProject/member/mypage/membership/myOrderInfo.jsp";
}
%>
<div class="HeaderWrapper">
	<div class="HeaderTopArea">
		<!-- 페이지 최상단 기본 메뉴 영역 -->
		<div class="AreaFlex">
			<div class="LayoutLeft">
				<!-- 관리자의 경우 물품관리/회원관리 메뉴가, 판매자의 경우 물품관리 메뉴가 생성되는 layout -->
				<ul class="LayoutLeftUl">
					<!--project_pageDB의 header_top_layoutleft-->
					<li class="HeaderAdminMenu" style="display: <%=header_display_admin_menu%>"><a href="#">회원관리</a></li>
					<li class="HeaderSellerMenu" style="display: <%=header_display_seller_menu%>"><a href="#">물품관리</a>
						<ul>
							<li><a href="/FreeProject/menu/seller/item/regiNewItem.jsp">품목 등록</a></li>
							<li><a href="/FreeProject/menu/seller/product/regiNewProduct.jsp">판매 물품 등록</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="LayoutRight">
				<!-- 기본적인 쇼핑몰의 로그인/회원가입/장바구니/주문배송/마이페이지/고객센터의 메뉴가 있는 layout -->
				<ul class="LayoutRightUl">
					<!--project_pageDB의 header_top_layoutright-->
					<li class="HeaderCurUserName" style="display: <%=header_display_cur_user%>"><a href="#"><%=header_cur_session_name %></a>님</li>
					<li class="HeaderLoginView" style="display: <%=header_display_login%>"><a href="/FreeProject/member/login/login.jsp?URI=<%=header_curPath%>">로그인</a></li>
					<li class="HeaderLogout" style="display: <%=header_display_logout%>"><a href="/FreeProject/logout.MemProc">로그아웃</a></li>
					<li class="HeaderMemberRegi" style="display: <%=header_display_signup%>"><a href="/FreeProject/member/signUp/choiceMemberType.jsp">회원가입</a></li>
					<li><a href="/FreeProject/menu/buyer/cart.jsp">장바구니</a></li>
					<li><a href="#">주문배송</a></li>
					<li class="MyPage"><a href="<%=header_mypage_path%>">마이페이지</a>
						<ul>
							<li><a href="<%=header_mypage_path%>">마이페이지 홈</a></li>
							<li><a href="<%=header_myinfo_path%>">회원정보</a></li>
							<li><a href="<%=header_myorderinfo_path%>">주문 내역</a></li>
						</ul>
					</li>
					<li><a href="#">고객센터</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="HeaderContainer">
		<div class="AreaFlex">
			<div class="LogoContainer"><h1 class="Logo"><a href="/FreeProject/index.jsp"><img src="/FreeProject/img/logo2.png"></a></h1></div>
			<div class="TotalSearch">
				<form method="GET" name="search_top" id="search_top">
					<div class="SearchInner">
						<input type="text" class="SearchInputBox" name="search_keywd" id="search_keywd" autocomplete="off">
						<input type="submit" class="SearchBtn" value="검색">
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="HeaderBotArea">
		<div class="AreaFlex">
			<div class="LayoutLeft">
				<ul class="ProductNaviUl">
					<li><a href="#">휴대폰케이스</a>
						<ul>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이스&Brend=SAMSUNG">Samsung</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이스&Brend=LG">LG</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이스&Brend=APPLE">APPLE</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이스&Brend=XIAOMI">Xiaomi</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이스&Brend=ETC">기타</a></li>
						</ul>
					</li>
					<li><a href="#">필름, 글라스</a>
						<ul>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=필름&Brend=SAMSUNG">Samsung</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=필름&Brend=LG">LG</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=필름&Brend=APPLE">APPLE</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=필름&Brend=XIAOMI">Xiaomi</a></li>
							<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=필름&Brend=ETC">기타</a></li>
						</ul>
					</li>
					<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=케이블">케이블</a></li>
					<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=거치대">거치대</a></li>
					<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=충전기">충전기</a></li>
					<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=파우치">파우치</a></li>
					<li><a href="/FreeProject/menu/showProducts/showProducts.jsp?Class=기타">기타</a></li>
				</ul>
			</div>
			<div class="LayoutRight"></div>
		</div>
	</div>
</div>