var sel_file;
$(function(){
	$("#item_img_input_btn").on("change", handleImgFileSelect);
	$("#item_manu_select").change(function(){
		if($("#item_manu_select option:selected").val()==""){
			$("#new_item_manu").attr('value','')
			$("#new_item_manu_chk").attr('value','false')
		}else{
			$("#new_item_manu").attr('value',$("#item_manu_select option:selected").val())
			$("#new_item_manu_chk").attr('value','true')
		}
	});
	$("#item_class_select").change(function(){
		if($("#item_class_select option:selected").val()==""){
			$("#new_item_class").attr('value','')
			$("#new_item_class_chk").attr('value','false')
		}else{
			$("#new_item_class").attr('value',$("#item_class_select option:selected").val())
			$("#new_item_class_chk").attr('value','true')
		}
	});
	$("#regi_item_submit").click(function(){
		var img_chk = $("#new_item_img_chk").val();
		var manu_chk = $("#new_item_manu_chk").val();
		var class_chk = $("#new_item_class_chk").val();
		var name_null_chk = $("#new_item_name").val();
		var manufac_null_chk = $("#new_item_manufac").val();

		if(img_chk==false||manu_chk==false||class_chk==false||name_null_chk==""||manufac_null_chk==""){
			if(img_chk==false){
				alert("상품 이미지를 등록해 주세요.")
			}else if(manu_chk==false){
				alert("제조사 분류를 등록해 주세요.")
			}else if(class_chk==false){
				alert("상품의 대분류를 등록해 주세요.")
			}else if(name_null_chk==""){
				alert("상품 이름을 입력해 주세요.")
				$("#new_item_name").focus();
			}else{
				alert("제조사를 입력해 주세요.")
				$("#new_item_manufac").focus();
			}
			event.preventDefault();
			event.stopPropagation();
			return false;
		}
	});
});
function handleImgFileSelect(e){
	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);

	filesArr.forEach(function(f){
		if(!f.type.match("image.*")){
			alert("확장자는 이미지 확장자만 가능합니다.");
			$("#new_item_img_chk").attr('value','false');
			return;
		}else{
			$("#new_item_img_chk").attr('value','true');
			sel_file = f;
			var reader = new FileReader();
			reader.onload = function(e){
				$("#item_preview_img").attr("src", e.target.result);
			}
			reader.readAsDataURL(f);
		}
	});
}