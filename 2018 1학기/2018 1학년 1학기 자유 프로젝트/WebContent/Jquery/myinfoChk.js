$(function(){
	//비밀번호 변경 클릭시 비밀번호 변경창으로 변경
	$("#pw_change_btn").click(function(){
		$("#pw_change_bf").css("display", "none")
		$("#pw_change_af").css("display", "block")
	});
	//비밀번호 변경 버튼 클릭시 비밀번호 유효성 검사
	$("#chk_input_chg_pw").click(function(){
		if($("#my_info_pw").val()==""){
			alert("비밀번호를 변경하려면 기존 비밀번호를 입력 하셔야 합니다.");
			$("#my_info_pw").focus();
			return;
		}else{
			$.ajax({
				type : "POST",
				url : "/FreeProject/Ajax/pwchk.jsp",
				dataType : "json",
				data : {
					cur_member_pw_chk : $("#my_info_pw").val()
				},
				success : function(data){
					if(data.passwordchk=="true"){
						if($("#my_info_new_pw").val()==""||$("#my_info_new_pw_confirm").val()==""){
							if($("#my_info_new_pw").val()==""){
								alert("새로운 비밀번호를 입력하세요");
								$("#my_info_new_pw").focus();
								return;
							}else{
								alert("비밀번호 확인을 입력하세요");
								$("#my_info_new_pw_confirm").focus();
								return;
							}
						}else if($("#my_info_new_pw").val()!=$("#my_info_new_pw_confirm").val()){
							alert("비밀번호가 일치하지 않습니다.\n비밀번호를 다시 확인하세요");
							$("#my_info_new_pw_confirm").focus();
								return;
						}else{
							document.my_ifno_form.action="/FreeProject/myInfo.MemProc?chgpw=Y";
							document.my_ifno_form.submit();
						}
					}else{
						alert("기존 비밀번호가 맞지 않습니다.");
						$("#my_info_pw").focus();
						return;
					}
				},
				error : function(){
					alert("ajax 실행 실패");
					return;
				}
			})
		}
	});
	$("#my_info_new_pw")
	.focusin(function(){
		$("#pw_confirm_chk").attr('value', 'false')
		$("#my_info_new_pw").attr('value', "");
	})
	.focusout(function(){
		if($("#my_info_new_pw").val()==""){
			$("#pw_confirm_chk").attr('value', 'false')
			// $("#pw1Msg").css("display", "block");
		}else{
			if($("#my_info_new_pw").val()!=$("#my_info_new_pw_confirm").val()){
				$("#pw_confirm_chk").attr('value', 'false')
				// $("#pw2Msg").css("display", "block")
				// $("#pw2Msg").html("비밀번호가 일치하지 않습니다.")
			}else{
				$("#pw_confirm_chk").attr('value', 'true')
				// $("#pw2Msg").css("display", "block")
				// $("#pw2Msg").html("사용 가능한 비밀번호 입니다..")
			}
		}
	});
	$("#my_info_new_pw_confirm")
	.focusin(function(){
		$("#pw_confirm_chk").attr('value', 'false')
		$("#my_info_new_pw_confirm").attr('value', "");
	})
	.focusout(function(){
		if($("#my_info_new_pw").val()==""){
			$("#pw_confirm_chk").attr('value', 'false')
			// $("#pw1Msg").css("display", "block");
		}else if($("#my_info_new_pw_confirm").val()==""){
			$("#pw_confirm_chk").attr('value', 'false')
			// $("#pw2Msg").css("display", "block");
		}else{
			if($("#my_info_new_pw").val()!=$("#my_info_new_pw_confirm").val()){
				$("#pw_confirm_chk").attr('value', 'false')
				// $("#pw2Msg").css("display", "block")
				// $("#pw2Msg").html("비밀번호가 일치하지 않습니다.")
			}else{
				$("#pw_confirm_chk").attr('value', 'true')
				// $("#pw2Msg").css("display", "block")
				// $("#pw2Msg").html("사용 가능한 비밀번호 입니다..")
			}
		}
	});
	$("#my_info_email1")
	.focusin(function(){
		$("#email_chk").attr('value', 'false')
		$("#my_info_email1").attr('value', "");
	})
	.focusout(function(){
		if($("#my_info_email1").val()==""){
			$("#email_chk").attr('value', 'false')
			// $("#emailMsg").css("display", "block");
		}else{
			if($("#email_selectbox option:selected").val()==""){
				// $("#email_chk").attr('value', 'false')
			}else{
				if($("#my_info_email2").val()==""){
					$("#email_chk").attr('value', 'false')
					// $("#emailMsg").css("display", "block");
				}else{
					$("#email_chk").attr('value', 'true')
					// $("#emailMsg").css("display", "none");
				}
			}
		}
	});
	$("#email_selectbox").change(function(){
		if($("#email_selectbox option:selected").val()==""){
			$("#my_info_email2").attr('readonly')
			$("#my_info_email2").attr('value','')
			$("#email_chk").attr('value', 'false')
			// $("#emailMsg").css("display", "block");
		}else if($("#email_selectbox option:selected").val()=="userinput"){
			$("#my_info_email2").removeAttr('readonly');
			$("#my_info_email2").attr('value',"");
			$("#my_info_email2").focus();
			$("#my_info_email2")
				.focusin(function(){
					$("#email_chk").attr('value', 'false')
	 				$("#my_info_email2").attr('value', "");
				})
				.focusout(function(){
					if($("#my_info_email1").val()==""||$("#my_info_email2").val()==""){
						$("#email_chk").attr('value', 'false')
						// $("#emailMsg").css("display", "block");
					}else{
						$("#email_chk").attr('value', 'true')
						// $("#emailMsg").css("display", "none");
					}
				})
		}else{
			$("#my_info_email2").attr('readonly')
			$("#my_info_email2").attr('value',$("#email_selectbox option:selected").val());
			if($("#my_info_email1").val()==""){
				$("#email_chk").attr('value', 'false')
				// $("#emailMsg").css("display", "block");
			}else{
				$("#email_chk").attr('value', 'true')
				// $("#emailMsg").css("display", "none");
			}

		}
	});
	$("#phone_selectbox").change(function(){
		$("#my_info_phone1").attr('value',$("#phone_selectbox option:selected").val())
		if($("#my_info_phone2").val()==""||$("#my_info_phone3").val()==""){
			$("#phone_chk").attr('value', 'false')
		}else{
			$("#phone_chk").attr('value', 'true')
		}
	});
	$("#my_info_phone2")
	.focusin(function(){
		$("#phone_chk").attr('value', 'false')
		$("#my_info_phone2").attr('value', "");
	})
	.focusout(function(){
		if($("#my_info_phone2").val()==""){
			$("#phone_chk").attr('value', 'false')
			// $("#phoneMsg").css("display", "block");
		}else{
			if($("#my_info_phone3").val()==""){
				$("#phone_chk").attr('value', 'false')
			}else{
				$("#phone_chk").attr('value', 'true')
			}
			// $("#phoneMsg").css("display", "none");
		}
	});
	$("#my_info_phone3")
	.focusin(function(){
		$("#phone_chk").attr('value', 'false')
		$("#my_info_phone3").attr('value', "");
	})
	.focusout(function(){
		if($("#my_info_phone3").val()==""){
			$("#phone_chk").attr('value', 'false')
			// $("#phoneMsg").css("display", "block");
		}else{
			$("#phone_chk").attr('value', 'true')
			// $("#phoneMsg").css("display", "none");
		}
	});
	$("#daum_addr1").change(function(){
		if($("#daum_addr2").val()==""){
			$("#addr_chk").attr('value', 'false');
		}else{
			$("#addr_chk").attr('value', 'true');
		}
	});
	$("#daum_addr2")
	.focusin(function(){
		$("#addr_chk").attr('value', 'false');
		$("#daum_addr2").attr('value', "");
	})
	.focusout(function(){
		if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
			// $("#addrMsg").css("display", "block");
		}else{
			if($("#daum_addr2").val()==""){
				// $("#addrMsg").css("display", "block");
			}else{
				$("#addr_chk").attr('value', 'true');
				// $("#addrMsg").css("display", "none");
			}
		}
	});
	$("#info_chg_submit").click(function(){
		if($("#my_info_pw").val()==""){
			alert("비밀번호를 변경하려면 기존 비밀번호를 입력 하셔야 합니다.");
			$("#my_info_pw").focus();
			return;
		}else{
			$.ajax({
				type : "POST",
				url : "/FreeProject/Ajax/pwchk.jsp",
				dataType : "json",
				data : {
					cur_member_pw_chk : $("#my_info_pw").val()
				},
				success : function(data){
					if(data.passwordchk=="true"){
						if($("#email_chk").val()=="false"||$("#phone_chk").val()=="false"||$("#addr_chk").val()=="false"){
							if($("#email_chk").val()=="false"){
								if($("#my_info_email1").val()==""||$("#my_info_email2").val()==""){
									if($("#my_info_email1").val()==""){
										alert("이메일 주소를 입력하세요.")
										$("#my_info_email1").focus();
										return;
									}else{
										alert("이메일 주소를 입력하세요.")
										$("#my_info_email2").focus();
										return;
									}
								}
							}else if($("#phone_chk").val()=="false"){
								if($("#my_info_phone2").val()==""||$("#my_info_phone3").val()==""){
									if($("#my_info_phone2").val()==""){
										alert("전화번호를 입력하세요.")
										$("#my_info_phone2").focus();
										return;
									}else{
										alert("전화번호를 입력하세요.")
										$("#my_info_phone3").focus();
										return;
									}
								}
							}else{
								if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""||$("#daum_addr2").val()==""){
									if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
										alert("기본 주소를 입력하세요.")
										return;
									}else{
										alert("상세 주소를 입력하세요.")
										$("#daum_addr2").focus();
										return;
									}
								}
							}
						}else{
							document.my_ifno_form.action="/FreeProject/myInfo.MemProc?chga=Y";
							document.my_ifno_form.submit();
						}
					}else{
						alert("기존 비밀번호가 맞지 않습니다.");
						$("#my_info_pw").focus();
						return;
					}
				},
				error : function(){
					alert("ajax 실행 실패");
					return;
				}
			})
		}
	});
	$("#withdrawal_btn").click(function(){
		//회원 탈퇴 버튼 클릭시
		var withdrawal_confrim = confirm("회원탈퇴시 모든 정보를 읽게 됩니다.\n그래도 진행하시겠습니까?")
		if(withdrawal_confrim==true){
			location.href = "/FreeProject/withdraw.MemProc";
		}else{
			return;
		}
	});
});