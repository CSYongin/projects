$(function(){
	//이하 회원가입시 각각 버튼의 focusin / focusout시 유효성 검사
	$("#new_usr_id")
	.focusin(function(){
		$("#input_new_usr_id_chk").attr('value', 'false')
		$("#new_usr_id").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_id").val()==""){
			$("#input_new_usr_id_chk").attr('value', 'false')
			$("#idMsg").css("display", "block");
		}else{
			//마우스를 떼는 순간 ajax를 통한 아이디 중복 검사 실행
			$.ajax({
				async : false,
				type : 'POST',
				url : "/FreeProject/Ajax/idchk.jsp",
				dataType : "json",
				data : {
					check_id : $("#new_usr_id").val()
				},
				success : function(data){
					if(data.idchk=="true"){
						$("#input_new_usr_id_chk").attr('value', 'true');
						$("#idMsg").css("display", "block")
						$("#idMsg").html("사용 가능한 아이디 입니다.")
					}else{
						$("#input_new_usr_id_chk").attr('value', 'false');
						$("#idMsg").css("display", "block")
						$("#idMsg").html("사용 불가능한 아이디 입니다.")
					}
				},
				error : function(error){
					alert("ajax 실행 실패");
					return false;
				}
			});
		}
	});
	$("#new_usr_pw1")
	.focusin(function(){
		$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
		$("#new_usr_pw1").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_pw1").val()==""){
			$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
			$("#pw1Msg").css("display", "block");
		}else{
			if($("#new_usr_pw1").val()!=$("#new_usr_pw2").val()){
				$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
				$("#pw1Msg").css("display", "none");
			}else{
				$("#input_new_usr_pw_confirm_chk").attr('value', 'true')
				$("#pw1Msg").css("display", "none");
			}
		}
	});
	$("#new_usr_pw2")
	.focusin(function(){
		$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
		$("#new_usr_pw2").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_pw1").val()==""){
			$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
			$("#pw1Msg").css("display", "block");
		}else if($("#new_usr_pw2").val()==""){
			$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
			$("#pw2Msg").css("display", "block");
		}else{
			if($("#new_usr_pw1").val()!=$("#new_usr_pw2").val()){
				$("#input_new_usr_pw_confirm_chk").attr('value', 'false')
				$("#pw2Msg").css("display", "block")
				$("#pw2Msg").html("비밀번호가 일치하지 않습니다.")
			}else{
				$("#input_new_usr_pw_confirm_chk").attr('value', 'true')
				$("#pw2Msg").css("display", "block")
				$("#pw2Msg").html("사용 가능한 비밀번호 입니다..")
			}
		}
	});
	$("#new_usr_name")
	.focusin(function(){
		$("#input_new_usr_name_chk").attr('value', 'false')
		$("#new_usr_name").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_name").val()==""){
			$("#input_new_usr_name_chk").attr('value', 'false')
			$("#nameMsg").css("display", "block");
		}else{
			$("#input_new_usr_name_chk").attr('value', 'true')
			$("#nameMsg").css("display", "none");
		}
	});
	$("#new_usr_email1")
	.focusin(function(){
		$("#input_new_usr_email_chk").attr('value', 'false')
		$("#new_usr_email1").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_email1").val()==""){
			$("#input_new_usr_email_chk").attr('value', 'false')
			$("#emailMsg").css("display", "block");
		}else{
			if($("#new_usr_email_selectbox option:selected").val()==""){
				$("#input_new_usr_email_chk").attr('value', 'false')
			}else{
				if($("#new_usr_email2").val()==""){
					$("#input_new_usr_email_chk").attr('value', 'false')
					$("#emailMsg").css("display", "block");
				}else{
					$("#input_new_usr_email_chk").attr('value', 'true')
					$("#emailMsg").css("display", "none");
				}
			}
		}
	});
	$("#new_usr_email_selectbox").change(function(){
		if($("#new_usr_email_selectbox option:selected").val()==""){
			$("#new_usr_email2").attr('readonly')
			$("#new_usr_email2").attr('value','')
			$("#input_new_usr_email_chk").attr('value', 'false')
			$("#emailMsg").css("display", "block");
		}else if($("#new_usr_email_selectbox option:selected").val()=="userinput"){
			$("#new_usr_email2").removeAttr('readonly');
			$("#new_usr_email2").attr('value',"");
			$("#new_usr_email2").focus();
			$("#new_usr_email2")
				.focusin(function(){
					$("#input_new_usr_email_chk").attr('value', 'false')
	 				$("#new_usr_email2").attr('value', "");
				})
				.focusout(function(){
					if($("#new_usr_email1").val()==""||$("#new_usr_email2").val()==""){
						$("#input_new_usr_email_chk").attr('value', 'false')
						$("#emailMsg").css("display", "block");
					}else{
						$("#input_new_usr_email_chk").attr('value', 'true')
						$("#emailMsg").css("display", "none");
					}
				})
		}else{
			$("#new_usr_email2").attr('readonly')
			$("#new_usr_email2").attr('value',$("#new_usr_email_selectbox option:selected").val());
			if($("#new_usr_email1").val()==""){
				$("#input_new_usr_email_chk").attr('value', 'false')
				$("#emailMsg").css("display", "block");
			}else{
				$("#input_new_usr_email_chk").attr('value', 'true')
				$("#emailMsg").css("display", "none");
			}

		}
	});
	$("#new_usr_phone1_selectbox").change(function(){
		$("#new_usr_phone1").attr('value',$("#new_usr_phone1_selectbox option:selected").val())
		if($("#new_usr_phone2").val()==""||$("#new_usr_phone3").val()==""){
			$("#input_new_usr_phone_chk").attr('value', 'false')
		}else{
			$("#input_new_usr_phone_chk").attr('value', 'true')
		}
	});
	$("#new_usr_phone2")
	.focusin(function(){
		$("#input_new_usr_phone_chk").attr('value', 'false')
		$("#new_usr_phone2").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_phone2").val()==""){
			$("#input_new_usr_phone_chk").attr('value', 'false')
			$("#phoneMsg").css("display", "block");
		}else{
			if($("#new_usr_phone3").val()==""){
				$("#input_new_usr_phone_chk").attr('value', 'false')
			}else{
				$("#input_new_usr_phone_chk").attr('value', 'true')
			}
			$("#phoneMsg").css("display", "none");
		}
	});
	$("#new_usr_phone3")
	.focusin(function(){
		$("#input_new_usr_phone_chk").attr('value', 'false')
		$("#new_usr_phone3").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_phone3").val()==""){
			$("#input_new_usr_phone_chk").attr('value', 'false')
			$("#phoneMsg").css("display", "block");
		}else{
			$("#input_new_usr_phone_chk").attr('value', 'true')
			$("#phoneMsg").css("display", "none");
		}
	});
	$("#daum_addr1").change(function(){
		if($("#daum_addr2").val()==""){
			$("#input_new_usr_addr_chk").attr('value', 'false');
		}else{
			$("#input_new_usr_addr_chk").attr('value', 'true');
		}
	});
	$("#daum_addr2")
	.focusin(function(){
		$("#input_new_usr_addr_chk").attr('value', 'false');
		$("#daum_addr2").attr('value', "");
	})
	.focusout(function(){
		if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
			$("#addrMsg").css("display", "block");
		}else{
			if($("#daum_addr2").val()==""){
				$("#addrMsg").css("display", "block");
			}else{
				$("#input_new_usr_addr_chk").attr('value', 'true');
				$("#addrMsg").css("display", "none");
			}
		}
	});
	$("#signUPbtn").click(function(){
		//회원가입 버튼 클릭시 유효성 검사 후, 맞지 않는 값이 있을 경우 error문구 표시
		var user_id_chk = $("#input_new_usr_id_chk").val();
		var user_pw_chk = $("#input_new_usr_pw_confirm_chk").val();
		var user_name_chk = $("#input_new_usr_name_chk").val();
		var user_phone_chk = $("#input_new_usr_phone_chk").val();
		var user_email_chk = $("#input_new_usr_email_chk").val();
		var user_addr_chk = $("#input_new_usr_addr_chk").val();

		if(user_id_chk=="false"||user_pw_chk=="false"||user_name_chk=="false"||user_phone_chk=="false"||user_email_chk=="false"||user_addr_chk=="false"){
			$("#submitMsg").css("display", "block");
			if(user_id_chk=="false"){
				if($("#new_usr_id").val()=""){
					$("#idMsg").css("display", "block")
					$("#idMsg").html("필수 항목입니다.")
					$("#new_usr_id").focus();
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else{
					$("#idMsg").css("display", "block")
					$("#idMsg").html("아이디의 조건이 맞지 않습니다. 다시 입력해 주세요.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}
			}else if(user_pw_chk=="false"){
				if($("#new_usr_pw1").val()==""){
					$("#pw1Msg").css("display", "block")
					$("#pw1Msg").html("필수 항목입니다.")
					$("#new_usr_id").focus();
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else if($("#new_usr_pw2").val()==""){
					$("#pw2Msg").css("display", "block")
					$("#pw2Msg").html("필수 항목입니다.")
					$("#new_usr_id").focus();
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else if($("#input_new_usr_pw_confirm_chk")=="false"){
					$("#pw2Msg").css("display", "block")
					$("#pw2Msg").html("비밀번호가 일치하지 않습니다. 다시 입력해 주세요.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}
			}else if(user_name_chk=="false"){
				if($("#new_usr_name").val()==""){
					$("#nameMsg").css("display", "block")
					$("#nameMsg").html("필수 항목입니다.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else{
					$("#nameMsg").css("display", "block")
					$("#nameMsg").html("입력된 값이 조건이 맞지 않습니다. 다시 입력해 주세요.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}
			}else if(user_phone_chk=="false"){
				if($("#new_usr_phone2").val()==""){
					$("#phoneMsg").css("display", "block")
					$("#phoneMsg").html("필수 항목입니다.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else if($("#new_usr_phone3").val()==""){
					$("#phoneMsg").css("display", "block")
					$("#phoneMsg").html("필수 항목입니다.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}
			}else{
				if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
					$("#phoneMsg").css("display", "block")
					$("#phoneMsg").html("주소찾기를 통해 주소를 입력해 주세요.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}else{
					$("#phoneMsg").css("display", "block")
					$("#phoneMsg").html("상세주소를 입력해 주세요.")
					event.preventDefault();
					event.stopPropagation();
					return false;
				}
			}
		}else{

		}
	})
});