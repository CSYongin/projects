// 회원가입 아이디 체크여부 확인(아이디 중복일 경우 = 0, 중복이 아닐경우 1)
var idck = 0;
$(function(){
	$("#id_chk").click(function(){
		//userid 를 param
		var userid = $("#new_usr_id").val();
		$.ajax({
			async : true,
			type : 'POST',
			data : {check_id : userid},
			url : "/FreeProject/Ajax/idchk.jsp",
			dataType : "json",
			contentType : "application/json; charset=UTF-8",
			success : function(data){
				if(data.idchk=="true"){
					alert("사용 가능한 아이디 입니다.");
					$("#input_new_usr_id_chk").attr('value', 'true');
				}else{
					alert("아이디가 존재합니다. 다른 아이디를 입력해 주세요.");
					$("#input_new_usr_id_chk").attr('value', 'false');
					$("#new_usr_id").focus();
				}
			},
			error : function(error){
				alert("error : "+error);
			}
		});
	});
});