<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex" style="position:relative; z-index:1001;">
					<div class="cycle-slideshow">
						<img src="/FreeProject/img/INDEX_IMG_1_2.png">
						<img src="/FreeProject/img/INDEX_IMG_2_2.png">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>