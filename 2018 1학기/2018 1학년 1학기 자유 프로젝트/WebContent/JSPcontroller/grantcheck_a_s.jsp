<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String get_cur_path = (String)request.getRequestURI();
if(session.getAttribute("id")==null||session.isNew()){
	response.sendRedirect("/FreeProject/member/login/login.jsp?URI="+get_cur_path);
}else{
	String get_cur_user_id = session.getAttribute("id").toString();
	String get_cur_user_grant = session.getAttribute(get_cur_user_id+"_Grant").toString();
	if(!(get_cur_user_grant.equals("admin")^get_cur_user_grant.equals("seller"))){
		//admin이나 seller가 아닌 일반 user일 경우 접근 불가능하게 처음 페이지로 돌려 버리는 로직 
		response.sendRedirect("/FreeProject/index.jsp");
	}
}
%>