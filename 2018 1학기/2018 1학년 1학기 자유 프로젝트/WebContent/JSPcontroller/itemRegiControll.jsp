<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.oreilly.servlet.*,com.oreilly.servlet.multipart.*,java.util.*,java.io.*,java.sql.*" %>
<%@ page import="controllerSet.DispatcherUtil, vo.*" %>
<%
	request.setCharacterEncoding("UTF-8");
	String dir = application.getRealPath("/img");//파일의 경로 불러오기

	ItemVO item = null;
	
	int max =10*1024*1024; // 업로드 파일의 최대 크기 지정 
	String filename=""; 
	String upload_file_dir="";
	try{ 
		/* 실제 파일 업로드 담당.
		인수 1) 폼에서 가져온 인자 값을 얻어오기 위해 request객체를 전달 
		인수 2) 업로드될 파일의 위치를 의미 
		인수 3) 최대 크기 
		인수 4) 파일 이름의 한글일 경우 문제가 되므로 처리할수 있도록 인코딩 타입지정. 
		인수 5) 인자는 똑같은 파일을 업로드할 경우 중복되지 않도록 자동으로 파일이름을 변환해주는 기능을 한다.(DefaultFileRenamePolicy)        
		*/ 
		MultipartRequest m = new MultipartRequest(request, dir, max, "utf-8", new DefaultFileRenamePolicy()); 
		//request객체의 getparameter메소드를 이용 폼에서 입력된 값을 받아온다.
		String item_name = m.getParameter("new_item_name");
		String item_manufac = m.getParameter("new_item_manufac");
		String item_manu_string = m.getParameter("new_item_manu");
		String item_class_string = m.getParameter("new_item_class");
		
		//업로드한 파일들을 Enumeration타입으로 반환한다. 
		Enumeration files=m.getFileNames();         
		String file =(String)files.nextElement(); 
		filename=m.getFilesystemName(file); //저장될 이름이다.
		//out.println(filename1+"<br>");
		upload_file_dir = dir+"\\"+filename;//DB에 저장될 파일의 전체 경로
		int find_base_dir = upload_file_dir.indexOf("FreeProject");//웹서버 이름 전까지의 index번호
		String db_upload_path = upload_file_dir.substring(73);//\FreeProject부터 주소 변환
		item = new ItemVO(item_name, item_manufac, item_manu_string, item_class_string, db_upload_path);
		
		request.setAttribute("NewitemObj", item);
		DispatcherUtil.forward(request, response, "/regi.IProc");
		/*
		System.out.println(find_base_dir);
		System.out.println(upload_file_dir);
		System.out.println(db_uploda_path);
		*/
		//String ofile1 =  m.getOriginalFileName("item_img_input_btn"); //원본파일 이름이다.
	}catch(Exception e){ 
		e.printStackTrace(new PrintWriter(out)); 
	} 
%>