<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="test/html; charset=UTF-8">
<meta name="Generator" content="sublime Text 3">
<meta name="Author" content="Woojun.kim / Sooa.jeong">
<meta name="Keywords" content="index.jsp">
<meta name="Description" content="">
<title></title>
<!--  common css file or link -->
<link rel="stylesheet" type="text/css" href="/FreeProject/css/initialize.css?ver=180601">
<link rel="stylesheet" type="text/css" href="/FreeProject/css/common_index.css?ver=180620_25">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="http://malsup.github.com/jquery.cycle2.js"></script>
<!--common jsp, jquery, json link-->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
<!--Daum 우편번호 서비스-->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="/FreeProject/Jquery/daumPostcode.js?ver=180601"></script>
<!--jsp, jquery, json file-->
<script src="/FreeProject/Jquery/common.js?ver=180609_01"></script>