<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/signUpSeller.css">
		<!--jsp, jquery, json file-->
		<script src="/FreeProject/Jquery/signUpChk.js?ver=180601"></script>
		<script type="text/javascript">
			$(function(){
			<%
				String regi_error_msg = (String)request.getAttribute("regi_error");
				if(regi_error_msg==null){

				}else{%>
					alert("<%=regi_error_msg%>");
				<%}
			%>
			});
		</script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<h1>판매자 회원 가입</h1>
					<div class="UserRegiAreaMain">
						<form action="/FreeProject/signUp.MemProc?signUp_path=seller" method="POST" id="reginewmemberForm">
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox Int_id">
										<span>
											<label class="Lable" id="id_lb" for="new_usr_id">아이디</label><br>
											<input type="text" name="new_usr_id" id="new_usr_id" value="" maxlength="15" autocomplete="off">
											<input type="hidden" name="input_new_usr_id_chk" id="input_new_usr_id_chk" value="false">
										</span>
										<div class="ErrorMsg" id="idMsg" name="idMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox Int_pw1">
										<span>
											<label class="Lable" id="pw_pw1" for="new_usr_pw1">비밀번호</label><br>
											<input type="password" name="new_usr_pw1" id="new_usr_pw1" value="" maxlength="16" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="pw1Msg" name="pw1Msg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox Int_pw2">
										<span>
											<label class="Lable" id="pw_pw2" for="new_usr_pw2">비밀번호 확인</label><br>
											<input type="password" name="new_usr_pw2" id="new_usr_pw2" value="" maxlength="16" autocomplete="off">
											<input type="hidden" name="input_new_usr_pw_confirm_chk" id="input_new_usr_pw_confirm_chk" value="false">
										</span>
										<div class="ErrorMsg" id="pw2Msg" name="pw2Msg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="name_lb" for=new_usr_name"">이름</label><br>
											<input type="text" name="new_usr_name" id="new_usr_name" value="" maxlength="10" autocomplete="off">
											<input type="hidden" name="input_new_usr_name_chk" id="input_new_usr_name_chk" value="false">
										</span>
										<div class="ErrorMsg" id="nameMsg" name="nameMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="email_lb" for="new_usr_email1">이메일</label><br>
											<input type="text" name="new_usr_email1" id="new_usr_email1" value="" size="8" maxlength="20" autocomplete="off">
											@
											<input type="text" name="new_usr_email2" id="new_usr_email2" value="" size="12" maxlength="20" autocomplete="off" readonly>
											<input type="hidden" name="input_new_usr_email_chk" id="input_new_usr_email_chk" value="false">
											<select name="new_usr_email_selectbox" id="new_usr_email_selectbox">
												<option value="" selected>선택하세요</option>
												<option value="naver.com">naver.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="nate.com">nate.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="userinput">직접입력</option>
											</select>
										</span>
										<div class="ErrorMsg" id="emailMsg" name="emailMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="phone_lb" for="new_usr_phone1">전화번호</label><br>
											<select id="new_usr_phone1_selectbox" onchange="execPhoneSelect(this.form)">
												<option value="010" selected>010</option>
												<option value="011">011</option>
												<option value="016">016</option>
												<option value="017">017</option>
												<option value="018">018</option>
												<option value="019">019</option>
											</select>
											<input type="hidden" name="new_usr_phone1" id="new_usr_phone1" value="010" size="4" value="010" readonly>
											-
											<input type="text" name="new_usr_phone2" id="new_usr_phone2" value="" size="4" maxlength="4" autocomplete="off">
											-
											<input type="text" name="new_usr_phone3" id="new_usr_phone3" value="" size="4" maxlength="4" autocomplete="off">
											<input type="hidden" name="input_new_usr_phone_chk" id="input_new_usr_phone_chk" value="false">
										</span>
										<div class="ErrorMsg" id="phoneMsg" name="phoneMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox">
										<div class="UserAddress" id="UserAddress">
											<label class="Label" id="addr_lb" for="daum_postcode">주소</label><br>
											<input type="text" name="new_usr_postcode" id="daum_postcode" placeholder="우편번호" readonly>
											<input type="button" onclick="execDaumPostcode();" value="우편번호 찾기" name=""><br>
											<input type="text" name="new_usr_addr1" id="daum_addr1" placeholder="주소" readonly><br>
											<input type="text" name="new_usr_addr2" id="daum_addr2" placeholder="상세주소">
											<input type="hidden" name="input_new_usr_addr_chk" id="input_new_usr_addr_chk" value="false">
										</div>
										<div class="ErrorMsg" id="addrMsg" name="addrMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="ErrorMsg" id="submitMsg" name="submitMsg" style="display: none">입력하신 정보를 다시 확인해 주세요.</div>
							<div>
								<input type="submit" value="가입하기" name="" id="signUPbtn">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>