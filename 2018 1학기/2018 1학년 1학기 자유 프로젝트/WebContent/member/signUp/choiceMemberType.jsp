<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/choiceMemberType.css?ver">
		<!--jsp, jquery, json file-->
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="choice">
					<br>
					<h2>가입을 환영합니다:)</h2>
					<br><br>
							<div>
								<label for="">일반 회원가입 </label>
								<input type="button" value="회원가입" onclick="location.href='signUpIndividual.jsp'">
							</div><br>
							<div>
								<label for="">판매자 회원가입 </label>
								<input type="button" value="회원가입" onclick="location.href='signUpSeller.jsp'">
							</div><br>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>