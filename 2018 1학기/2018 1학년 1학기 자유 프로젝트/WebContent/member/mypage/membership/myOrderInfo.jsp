<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, vo.*, dao.*, serviceSet.*" %>
<%
	String myinfo_id = session.getAttribute("id").toString();
	OrderService OService = OrderService.getInstance();

	ArrayList<OrderedDataVO> orderedcnt = OService.orderBuyerOrderedCnt(myinfo_id);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/myOrderInfo.css">
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="myOrderInfoWrapper">
						<h2>주문내역</h1>
							<table>
								<thead>
									<tr>
										<th>주문일(주문번호)</th>
										<th colspan="2">상품명</th>
										<th>판매자</th>
									</tr>
								</thead>
								
						<%
						for(OrderedDataVO orderNo:orderedcnt){
							System.out.println(orderNo.getOrder_seq());
							ArrayList<OrderedDataVO> orderedlist = OService.orderDone(orderNo.getOrder_seq());
						%>
								<tbody style="border-bottom:2px solid black">
						<%
							for(OrderedDataVO oredereddata:orderedlist){
						%>
								
									<tr>
										<th><%=oredereddata.getOrder_date() %> (<%=oredereddata.getOrder_seq() %>)</th>
										<th rowspan="2"><img src="<%=oredereddata.getItem_img() %>" style="width:50px"></th>
										<th rowspan="2"><%=oredereddata.getPr_name() %></th>
										<th rowspan="2"><%=oredereddata.getSeller_name() %></th>
									</tr>
									<tr>
										<th>결제금액 : <%=(oredereddata.getBuy_cnt()*oredereddata.getPr_price()) %></th>
									</tr>
								
						<%
							}
						%>
								</tbody>
						<%
						}
						%>
							</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>