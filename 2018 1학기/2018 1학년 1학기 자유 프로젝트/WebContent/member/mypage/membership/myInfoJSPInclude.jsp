<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="vo.*" import="serviceSet.*" import="dao.*"%>
<%
	String myinfo_id = session.getAttribute("id").toString();
	String myinfo_name = session.getAttribute(myinfo_id+"_Name").toString();
	MemberService MemService = MemberService.getInstance();
	MemberVO member = MemService.memberMyInfoSearch(myinfo_id);

	String myinfo_email1 = member.getUser_email1(), myinfo_email2 = member.getUser_email2();
	String emailNaver="", emailHanmail="", emailNate="", emailGmail="", emailEtc="";
	if(myinfo_email2.equals("naver.com")){
		emailNaver="selected";
	}else if(myinfo_email2.equals("hanmail.net")){
		emailHanmail="selected";
	}else if(myinfo_email2.equals("nate.com")){
		emailNate="selected";
	}else if(myinfo_email2.equals("gmail.com")){
		emailGmail="selected";
	}else{
		emailEtc="selected";
	}
	String myinfo_phone1 = member.getUser_phone1(), myinfo_phone2 = member.getUser_phone2(), myinfo_phone3 = member.getUser_phone3();
	String phone010="", phone011="", phone016="", phone017="", phone018="", phone019="";
	if(myinfo_phone1.equals("010")){
		phone010="selected";
	}else if(myinfo_phone1.equals("011")){
		phone011="selected";
	}else if(myinfo_phone1.equals("016")){
		phone016="selected";
	}else if(myinfo_phone1.equals("017")){
		phone017="selected";
	}else if(myinfo_phone1.equals("018")){
		phone018="selected";
	}else if(myinfo_phone1.equals("019")){
		phone019="selected";
	}
	String myinfo_addr1 = member.getUser_addr1(), myinfo_addr2 = member.getUser_addr2();
	int myinfo_postcode = member.getUser_postcode();
%>