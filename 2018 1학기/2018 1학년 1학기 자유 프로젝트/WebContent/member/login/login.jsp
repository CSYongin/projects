<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/headermeta.jsp" %>
		<link rel="stylesheet" type="text/css" href="/FreeProject/css/login.css?ver=180620_1">
		<!--jsp, jquery, json file-->
		<script src="/FreeProject/Jquery/loginChk.js"></script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="Content">
						<div class="Login">
							<h2>로그인</h2>
							<div class="LoginWrapper">
								<div class="LoginError">

								</div>
								<div class="MemberLogin">
									<div class="LoginForm">
										<form action="/FreeProject/login.MemProc" name="login_form" id="login_form" method="POST">
											<%String pastPath = (String)request.getParameter("URI");%>
											<input type="hidden" name="to_go_path" id="to_go_path" value="<%=pastPath%>">
											<div class="input">
												<span>
													<label for="login_usr_id">아이디</label>
													<input type="text" name="login_usr_id" id="login_usr_id" maxlength="12" autocomplete="off">
												</span><br>
												<span>
													<label for="login_usr_pw">비밀번호</label>
													<input type="password" name="login_usr_pw" id="login_usr_pw" maxlength="16" autocomplete="off">
												</span><br>
											</div>
											<div class="LoginBtn">
												<input type="submit" value="로그인" name="login_submit_btn" id="login_submit_btn"><br>
											</div>
										</form>
										<div>
											<a class="SignUpBtn" target="_blank" href="/FreeProject/member/signUp/choiceMemberType.jsp">회원가입</a>
											<a class="SearchIdBtn" href="#">아이디 찾기</a>
											<a class="SearchPwBtn" href="#">비밀번호 찾기</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>