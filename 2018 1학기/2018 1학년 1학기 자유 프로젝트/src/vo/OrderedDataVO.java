package vo;

import java.util.Date;

public class OrderedDataVO {
	private String pr_id, pr_name, item_img;
	private String seller_id, seller_name, seller_phone, seller_email, seller_addr1, seller_addr2;
	private String buyer_id, buyer_name, buyer_phone, buyer_email, buyer_addr1, buyer_addr2;
	private Date order_date;
	private int order_seq, total_price, pr_price, buy_cnt;
	private int seller_postcode, buyer_postcode;
	
	public OrderedDataVO(int order_seq, Date order_date, int total_price, String pr_id, String pr_name, int pr_price, String item_img, int buy_cnt, 
			String seller_id, String seller_name, String seller_phone, String seller_email, int seller_postcode, String seller_addr1, String seller_addr2, 
			String buyer_id, String buyer_name, String buyer_phone, String buyer_email, int buyer_postcode, String buyer_addr1, String buyer_addr2) {
		setOrder_seq(order_seq);
		setOrder_date(order_date);
		setTotal_price(total_price);
		setPr_id(pr_id);
		setPr_name(pr_name);
		setPr_price(pr_price);
		setItem_img(item_img);
		setBuy_cnt(buy_cnt);
		setSeller_id(seller_id);
		setSeller_name(seller_name);
		setSeller_phone(seller_phone);
		setSeller_email(seller_email);
		setSeller_postcode(seller_postcode);
		setSeller_addr1(seller_addr1);
		setSeller_addr2(seller_addr2);
		setBuyer_id(buyer_id);
		setBuyer_name(buyer_name);
		setBuyer_phone(buyer_phone);
		setBuyer_email(buyer_email);
		setBuyer_postcode(buyer_postcode);
		setBuyer_addr1(buyer_addr1);
		setBuyer_addr2(buyer_addr2);
	}
	public OrderedDataVO(int order_seq) {
		setOrder_seq(order_seq);
	}
	
	public String getPr_id() {
		return pr_id;
	}
	public void setPr_id(String pr_id) {
		this.pr_id = pr_id;
	}
	public String getPr_name() {
		return pr_name;
	}
	public void setPr_name(String pr_name) {
		this.pr_name = pr_name;
	}
	public String getItem_img() {
		return item_img;
	}
	public void setItem_img(String item_img) {
		this.item_img = item_img;
	}
	public String getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}
	public String getSeller_name() {
		return seller_name;
	}
	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
	}
	public String getSeller_phone() {
		return seller_phone;
	}
	public void setSeller_phone(String seller_phone) {
		this.seller_phone = seller_phone;
	}
	public String getSeller_email() {
		return seller_email;
	}
	public void setSeller_email(String seller_email) {
		this.seller_email = seller_email;
	}
	public String getSeller_addr1() {
		return seller_addr1;
	}
	public void setSeller_addr1(String seller_addr1) {
		this.seller_addr1 = seller_addr1;
	}
	public String getSeller_addr2() {
		return seller_addr2;
	}
	public void setSeller_addr2(String seller_addr2) {
		this.seller_addr2 = seller_addr2;
	}
	public String getBuyer_id() {
		return buyer_id;
	}
	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}
	public String getBuyer_name() {
		return buyer_name;
	}
	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}
	public String getBuyer_phone() {
		return buyer_phone;
	}
	public void setBuyer_phone(String buyer_phone) {
		this.buyer_phone = buyer_phone;
	}
	public String getBuyer_email() {
		return buyer_email;
	}
	public void setBuyer_email(String buyer_email) {
		this.buyer_email = buyer_email;
	}
	public String getBuyer_addr1() {
		return buyer_addr1;
	}
	public void setBuyer_addr1(String buyer_addr1) {
		this.buyer_addr1 = buyer_addr1;
	}
	public String getBuyer_addr2() {
		return buyer_addr2;
	}
	public void setBuyer_addr2(String buyer_addr2) {
		this.buyer_addr2 = buyer_addr2;
	}
	public Date getOrder_date() {
		return order_date;
	}
	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}
	public int getOrder_seq() {
		return order_seq;
	}
	public void setOrder_seq(int order_seq) {
		this.order_seq = order_seq;
	}
	public int getTotal_price() {
		return total_price;
	}
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
	public int getPr_price() {
		return pr_price;
	}
	public void setPr_price(int pr_price) {
		this.pr_price = pr_price;
	}
	public int getBuy_cnt() {
		return buy_cnt;
	}
	public void setBuy_cnt(int buy_cnt) {
		this.buy_cnt = buy_cnt;
	}
	public int getSeller_postcode() {
		return seller_postcode;
	}
	public void setSeller_postcode(int seller_postcode) {
		this.seller_postcode = seller_postcode;
	}
	public int getBuyer_postcode() {
		return buyer_postcode;
	}
	public void setBuyer_postcode(int buyer_postcode) {
		this.buyer_postcode = buyer_postcode;
	}
}
