package vo;

public class MemberVO {
	private String user_id, user_pw, user_name, user_phone1, user_phone2, user_phone3, user_email1, user_email2, user_addr1, user_addr2, user_grant_desc, user_classify_desc;
	private int user_postcode, user_grant, user_classify;

	public MemberVO() {
	}
		
	public MemberVO(String user_id, String user_pw, String user_name, String user_phone1, String user_phone2,
			String user_phone3, String user_email1, String user_email2, String user_addr1, String user_addr2,
			int user_postcode, int user_grant, int user_classify) {
		//회원 가입시 생성되는 member객체
		setUser_id(user_id);
		setUser_pw(user_pw);
		setUser_name(user_name);
		setUser_phone1(user_phone1);
		setUser_phone2(user_phone2);
		setUser_phone3(user_phone3);
		setUser_email1(user_email1);
		setUser_email2(user_email2);
		setUser_addr1(user_addr1);
		setUser_addr2(user_addr2);
		setUser_postcode(user_postcode);
		setUser_grant(user_grant);
		setUser_classify(user_classify);
	}
	public MemberVO(String user_pw, String user_name, String user_grant_desc, String user_classify_desc) {
		//로그인시 불러오는 member객체
		setUser_pw(user_pw);
		setUser_name(user_name);
		setUser_grant_desc(user_grant_desc);
		setUser_classify_desc(user_classify_desc);
	}
	public MemberVO(String user_id, String user_name, String user_phone1, String user_phone2, String user_phone3,
			String user_email1, String user_email2, int user_postcode, String user_addr1, String user_addr2) {
		setUser_id(user_id);
		setUser_name(user_name);
		setUser_phone1(user_phone1);
		setUser_phone2(user_phone2);
		setUser_phone3(user_phone3);
		setUser_email1(user_email1);
		setUser_email2(user_email2);
		setUser_addr1(user_addr1);
		setUser_addr2(user_addr2);
		setUser_postcode(user_postcode);
	}
	public MemberVO(String user_phone1, String user_phone2, String user_phone3,
			String user_email1, String user_email2, int user_postcode, String user_addr1, String user_addr2) {
		setUser_phone1(user_phone1);
		setUser_phone2(user_phone2);
		setUser_phone3(user_phone3);
		setUser_email1(user_email1);
		setUser_email2(user_email2);
		setUser_addr1(user_addr1);
		setUser_addr2(user_addr2);
		setUser_postcode(user_postcode);
	}
	
	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_pw() {
		return user_pw;
	}

	public void setUser_pw(String user_pw) {
		this.user_pw = user_pw;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_phone1() {
		return user_phone1;
	}

	public void setUser_phone1(String user_phone1) {
		this.user_phone1 = user_phone1;
	}

	public String getUser_phone2() {
		return user_phone2;
	}

	public void setUser_phone2(String user_phone2) {
		this.user_phone2 = user_phone2;
	}

	public String getUser_phone3() {
		return user_phone3;
	}

	public void setUser_phone3(String user_phone3) {
		this.user_phone3 = user_phone3;
	}

	public String getUser_email1() {
		return user_email1;
	}

	public void setUser_email1(String user_email1) {
		this.user_email1 = user_email1;
	}

	public String getUser_email2() {
		return user_email2;
	}

	public void setUser_email2(String user_email2) {
		this.user_email2 = user_email2;
	}

	public String getUser_addr1() {
		return user_addr1;
	}

	public void setUser_addr1(String user_addr1) {
		this.user_addr1 = user_addr1;
	}

	public String getUser_addr2() {
		return user_addr2;
	}

	public void setUser_addr2(String user_addr2) {
		this.user_addr2 = user_addr2;
	}

	public int getUser_postcode() {
		return user_postcode;
	}

	public void setUser_postcode(int user_postcode) {
		this.user_postcode = user_postcode;
	}

	public int getUser_grant() {
		return user_grant;
	}

	public void setUser_grant(int user_grant) {
		this.user_grant = user_grant;
	}

	public int getUser_classify() {
		return user_classify;
	}

	public void setUser_classify(int user_classify) {
		this.user_classify = user_classify;
	}
	public String getUser_grant_desc() {
		return user_grant_desc;
	}

	public void setUser_grant_desc(String user_grant_desc) {
		this.user_grant_desc = user_grant_desc;
	}

	public String getUser_classify_desc() {
		return user_classify_desc;
	}

	public void setUser_classify_desc(String user_classify_desc) {
		this.user_classify_desc = user_classify_desc;
	}

}
