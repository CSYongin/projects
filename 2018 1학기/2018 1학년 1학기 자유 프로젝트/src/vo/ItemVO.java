package vo;

public class ItemVO {
	private String item_id, item_name, item_manu, item_img, item_manu_string, item_class_string;
	private int item_manu_val, item_class_val;
	
	public ItemVO() {
		
	}
	public ItemVO(String item_name, String item_manu, String item_manu_string, String item_class_string, String item_img) {
		setItem_name(item_name);
		setItem_manu(item_manu);
		setItem_manu_string(item_manu_string);
		setItem_class_string(item_class_string);
		setItem_img(item_img);
	}
	public ItemVO(String item_name, String item_manu, int item_manu_val, int item_class_val, String item_img) {
		setItem_name(item_name);
		setItem_manu(item_manu);
		setItem_manu_val(item_manu_val);
		setItem_class_val(item_class_val);
		setItem_img(item_img);
	}
	public ItemVO(String item_id, String item_name, String item_manu, String item_class_string, String item_manu_string, String item_img) {
		setItem_id(item_id);
		setItem_name(item_name);
		setItem_manu(item_manu);
		setItem_class_string(item_class_string);
		setItem_manu_string(item_manu_string);
		setItem_img(item_img);
	}
	
	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getItem_manu() {
		return item_manu;
	}

	public void setItem_manu(String item_manu) {
		this.item_manu = item_manu;
	}

	public String getItem_img() {
		return item_img;
	}

	public void setItem_img(String item_img) {
		this.item_img = item_img;
	}

	public int getItem_manu_val() {
		return item_manu_val;
	}

	public void setItem_manu_val(int item_manu_val) {
		this.item_manu_val = item_manu_val;
	}

	public int getItem_class_val() {
		return item_class_val;
	}

	public void setItem_class_val(int item_class_val) {
		this.item_class_val = item_class_val;
	}
	
	public String getItem_manu_string() {
		return item_manu_string;
	}
	public void setItem_manu_string(String item_manu_string) {
		this.item_manu_string = item_manu_string;
	}
	public String getItem_class_string() {
		return item_class_string;
	}
	public void setItem_class_string(String item_class_string) {
		this.item_class_string = item_class_string;
	}
	
}
