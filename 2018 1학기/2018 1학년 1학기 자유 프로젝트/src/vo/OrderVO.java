package vo;

public class OrderVO {
	private String pr_id,user_id;
	private int order_seq,pr_cnt,total_price;
	
	
	public OrderVO(int cur_order_seq, String pr_id, int pr_cnt) {
		setOrder_seq(cur_order_seq);
		setPr_id(pr_id);
		setPr_cnt(pr_cnt);
	}
	public OrderVO(int total_price, String user_id) {
		setTotal_price(total_price);
		setUser_id(user_id);
	}
	public String getPr_id() {
		return pr_id;
	}
	public void setPr_id(String pr_id) {
		this.pr_id = pr_id;
	}
	public int getOrder_seq() {
		return order_seq;
	}
	public void setOrder_seq(int order_seq) {
		this.order_seq = order_seq;
	}
	public int getPr_cnt() {
		return pr_cnt;
	}
	public void setPr_cnt(int pr_cnt) {
		this.pr_cnt = pr_cnt;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getTotal_price() {
		return total_price;
	}
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
}
