package vo;

public class CartVO {
	private String pr_id;
	private int buy_cnt;
	
	public CartVO() {
		
	}
	public CartVO(String pr_id, int buy_cnt) {
		setPr_id(pr_id);
		setBuy_cnt(buy_cnt);
	}

	public String getPr_id() {
		return pr_id;
	}

	public void setPr_id(String pr_id) {
		this.pr_id = pr_id;
	}

	public int getBuy_cnt() {
		return buy_cnt;
	}

	public void setBuy_cnt(int buy_cnt) {
		this.buy_cnt = buy_cnt;
	}
	
	
}
