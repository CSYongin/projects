package vo;

public class ProductVO {
	private String pr_id, pr_name;
	private int pr_cnt, pr_price;

	private String item_id, item_name, item_manufac, item_manu_desc, item_class_desc, item_img;

	private String seller_id, seller_name, seller_phone, seller_email, seller_addr1, seller_addr2;
	private int seller_postcode;

	public ProductVO() {

	}
	public ProductVO(String pr_id, String item_id, String pr_name, int pr_cnt, int pr_price, String seller_id) {
		setPr_id(pr_id);
		setItem_id(item_id);
		setPr_name(pr_name);
		setPr_cnt(pr_cnt);
		setPr_price(pr_price);
		setSeller_id(seller_id);
	}
	public ProductVO(String item_img, String pr_id, String pr_name, int pr_price, String seller_id, String seller_name) {
		setItem_img(item_img);
		setPr_id(pr_id);
		setPr_name(pr_name);
		setPr_price(pr_price);
		setSeller_id(seller_id);
		setSeller_name(seller_name);
	}
	public ProductVO(String pr_id, String pr_name, int pr_cnt, int pr_price, 
			String item_name, String item_manufac, String item_manu_desc, String item_class_desc, String item_img, 
			String seller_id, String seller_name, String seller_phone, String seller_email, 
			int seller_postcode, String seller_addr1, String seller_addr2) {
		setPr_id(pr_id);
		setPr_name(pr_name);
		setPr_cnt(pr_cnt);
		setPr_price(pr_price);
		setItem_name(item_name);
		setItem_manufac(item_manufac);
		setItem_manu_desc(item_manu_desc);
		setItem_class_desc(item_class_desc);
		setItem_img(item_img);
		setSeller_id(seller_id);
		setSeller_name(seller_name);
		setSeller_phone(seller_phone);
		setSeller_email(seller_email);
		setSeller_postcode(seller_postcode);
		setSeller_addr1(seller_addr1);
		setSeller_addr2(seller_addr2);
	}

	public ProductVO(String pr_id, int pr_cnt_cal) {
		setPr_id(pr_id);
		setPr_cnt(pr_cnt_cal);
	}
	public String getPr_id() {
		return pr_id;
	}
	public void setPr_id(String pr_id) {
		this.pr_id = pr_id;
	}
	public String getPr_name() {
		return pr_name;
	}
	public void setPr_name(String pr_name) {
		this.pr_name = pr_name;
	}
	public int getPr_cnt() {
		return pr_cnt;
	}
	public void setPr_cnt(int pr_cnt) {
		this.pr_cnt = pr_cnt;
	}
	public int getPr_price() {
		return pr_price;
	}
	public void setPr_price(int pr_price) {
		this.pr_price = pr_price;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_manufac() {
		return item_manufac;
	}
	public void setItem_manufac(String item_manufac) {
		this.item_manufac = item_manufac;
	}
	public String getItem_manu_desc() {
		return item_manu_desc;
	}
	public void setItem_manu_desc(String item_manu_desc) {
		this.item_manu_desc = item_manu_desc;
	}
	public String getItem_class_desc() {
		return item_class_desc;
	}
	public void setItem_class_desc(String item_class_desc) {
		this.item_class_desc = item_class_desc;
	}
	public String getItem_img() {
		return item_img;
	}
	public void setItem_img(String item_img) {
		this.item_img = item_img;
	}
	public String getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}
	public String getSeller_name() {
		return seller_name;
	}
	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
	}
	public String getSeller_addr1() {
		return seller_addr1;
	}
	public void setSeller_addr1(String seller_addr1) {
		this.seller_addr1 = seller_addr1;
	}
	public String getSeller_addr2() {
		return seller_addr2;
	}
	public void setSeller_addr2(String seller_addr2) {
		this.seller_addr2 = seller_addr2;
	}
	public int getSeller_postcode() {
		return seller_postcode;
	}
	public void setSeller_postcode(int seller_postcode) {
		this.seller_postcode = seller_postcode;
	}
	public String getSeller_phone() {
		return seller_phone;
	}
	public void setSeller_phone(String seller_phone) {
		this.seller_phone = seller_phone;
	}
	public String getSeller_email() {
		return seller_email;
	}
	public void setSeller_email(String seller_email) {
		this.seller_email = seller_email;
	}
	
}
