package serviceSet;

import vo.*;
import java.util.*;

import dao.*;

public class ProductService {
	private static ProductService producService = new ProductService();
	public ProductDAO producDao = ProductDAO.getInstance();

	private ProductService(){}

	public static ProductService getInstance() {
		return producService;
	}
	public void productRegiNewProduct(ProductVO product) {
		producDao.productRegiNewProduct(product);
	}
	public ArrayList<ProductVO> productShowProductSimple(String sel_brend, String sel_class) {
		return producDao.productShowProductSimple(sel_brend, sel_class);
	}
	public ArrayList<ProductVO> productShowProductSimple(String sel_class) {
		return producDao.productShowProductSimple(sel_class);
	}
	public ProductVO productShowProductDetail(String pr_id) {
		return producDao.productShowProductDetail(pr_id);
	}
}
