package serviceSet;

import vo.*;
import java.util.*;

import dao.*;

public class ItemService {
	private static ItemService itemService = new ItemService();
	public ItemDAO itemDao = ItemDAO.getInstance();

	private ItemService(){}

	public static ItemService getInstance() {
		return itemService;
	}
	public void itemRegiNewItem(ItemVO item) {
		itemDao.itemRegiNewItem(item);
	}
	public ArrayList<ItemVO> itemSearchAll() {
		return itemDao.itemSearchAll();
	}
}
