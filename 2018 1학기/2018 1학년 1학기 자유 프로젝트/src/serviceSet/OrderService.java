package serviceSet;

import vo.*;

import java.util.ArrayList;

import dao.*;

public class OrderService {
	private static OrderService OService = new OrderService();
	public OrderDAO ODao = OrderDAO.getInstance();

	private OrderService(){}

	public static OrderService getInstance() {
		return OService;
	}
	public int orderGetLastSeq() {
		return ODao.orderGetLastSeq();
	}

	public void orderProcess(ArrayList<ProductVO> calproductcntlist, ArrayList<OrderVO> orderlist, OrderVO orders) {
		// TODO Auto-generated method stub
		ODao.orderProcess(calproductcntlist, orderlist, orders);
	}
	public ArrayList<OrderedDataVO> orderDone(int order_seq){
		return ODao.orderDone(order_seq);
	}
	public ArrayList<OrderedDataVO> orderBuyerOrderedCnt(String user_id){
		return ODao.orderBuyerOrderedCnt(user_id);
	}
}
