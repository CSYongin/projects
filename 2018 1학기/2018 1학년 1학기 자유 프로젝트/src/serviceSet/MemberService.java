package serviceSet;

import vo.*;
import dao.*;

public class MemberService {
	private static MemberService memService = new MemberService();
	public MemberDAO memDao = MemberDAO.getInstance();

	private MemberService(){}

	public static MemberService getInstance() {
		return memService;
	}
	public boolean chkid(String new_user_id) {
		//아이디 중복 검사 Ajax/chkid.jsp에서 사용 
		return memDao.chkid(new_user_id);
	}
	public void memberSignUp(MemberVO member) {
		//회원가입시 생성되는 객체
		memDao.memberSignUp(member);
	}
	public MemberVO memberLoginSearch(String login_user_id) {
		//로그인시 호출 생성되는 객체
		//비밀번호,이름,권한,등급을 가져옴
		return memDao.memberLoginSearch(login_user_id);
	}
	public MemberVO memberMyInfoSearch(String MyInfo_user_id) {
		//회원정보 수정페이지에 접근시 생성되는 객체
		return memDao.memberMyInfoSearch(MyInfo_user_id);
	}
	public String memberMyInfoPWSearch(String MyInfo_user_id) {
		//회원정보 수정시 비밀번호 일치 확인시 생성되는 객체
		return memDao.memberMyInfoPWSearch(MyInfo_user_id);
	}
	public void memberMyInfoPWUpdate(String MyInfo_user_id, String new_user_pw) {
		//회원정보 비밀번호 변경시 생성되는 객체
		memDao.memberMyInfoPWUpdate(MyInfo_user_id, new_user_pw);
	}
	public void memberMyInfoUpdate(String MyInfo_user_id, MemberVO member) {
		//회원정보 변경시 생성되는 객체 
		memDao.memberMyInfoUpdate(MyInfo_user_id, member);
	}
	public void memberWithdrwal(String Withdrwal_user_id) {
		memDao.memberWithdrwal(Withdrwal_user_id);
	}
}
