package controllerSet;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.memberControllerSet.*;

public class MemberFC extends HttpServlet{

	HashMap<String, Controller> list = null;
	public void init(ServletConfig sc) throws ServletException{
		list = new HashMap<String,Controller>();
		list.put("/signUp.MemProc", new MemberSignUpC());
		list.put("/login.MemProc", new MemberLoginC());
		list.put("/logout.MemProc", new MemberLogoutC());
		list.put("/myInfo.MemProc", new MemberMyInfoC());
		list.put("/withdraw.MemProc", new MemberWithdraw());
	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String url = request.getRequestURI();
		String contextPath = request.getContextPath();
		String path = url.substring(contextPath.length());

		Controller subController = list.get(path);
		subController.execute(request, response);
	}
}