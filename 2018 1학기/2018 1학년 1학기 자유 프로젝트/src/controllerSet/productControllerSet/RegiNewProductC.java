package controllerSet.productControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class RegiNewProductC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		boolean query_chk = false;
		
		if(session.isNew()||session.getAttribute("id")==null) {
			//비로그인상태 접근
			response.sendRedirect(request.getHeader("referer"));
		}else if(!((session.getAttribute(session.getAttribute("id").toString()+"_Grant").toString()).equals("admin")^
				(session.getAttribute(session.getAttribute("id").toString()+"_Grant").toString()).equals("seller"))) {
			response.sendRedirect(request.getHeader("referer"));
		}else {
			String item_id = request.getParameter("item_id");
			String pr_id = request.getParameter("new_pr_id");
			String pr_name = request.getParameter("new_pr_name");
			int pr_cnt = Integer.parseInt(request.getParameter("new_pr_cnt"));
			int pr_price = Integer.parseInt(request.getParameter("new_pr_price"));
			String seller_id = session.getAttribute("id").toString();
			
			ProductVO product = new ProductVO(pr_id, item_id, pr_name, pr_cnt, pr_price, seller_id);
			ProductService producService = ProductService.getInstance();
			producService.productRegiNewProduct(product);
			query_chk = ProductDAO.chkQuery();
			if(query_chk) {
				//성공시
				response.sendRedirect("/FreeProject/menu/seller/product/result/productRegiDone.jsp");
			}else {
				//실패시
				response.sendRedirect(request.getHeader("referer"));
			}
		}
	}
}
