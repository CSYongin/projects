package controllerSet.orderControllerSet;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class OrderProcessC implements Controller {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		OrderService OService = OrderService.getInstance();
		ProductService producService = ProductService.getInstance();
		boolean chk_query = false;
		
		String user_id = session.getAttribute("id").toString();
		int last_order_seq = OService.orderGetLastSeq();
		int cur_order_seq = last_order_seq+1;
//		System.out.println(last_order_seq);
		int total_price = 0;
		ArrayList<CartVO> cartlist = (ArrayList<CartVO>)session.getAttribute(user_id+"_cart");
		ArrayList<ProductVO> calproductcntlist = new ArrayList<ProductVO>();
		ArrayList<OrderVO> orderlist = new ArrayList<OrderVO>();
		
		for(int i=0;i<cartlist.size();i++) {
			CartVO cart = cartlist.get(i);
			String pr_id = cart.getPr_id();
			int pr_cnt = cart.getBuy_cnt();
			
			ProductVO product = producService.productShowProductDetail(pr_id);
			int cur_pr_cnt = product.getPr_cnt();
			int pr_price = product.getPr_price();
			total_price+=(pr_cnt*pr_price);
			
			ProductVO calproductcnt = new ProductVO(pr_id, (cur_pr_cnt-pr_cnt));
			calproductcntlist.add(calproductcnt);
//			System.out.println(calproductcnt.getPr_id()+" / "+calproductcnt.getPr_cnt());
//			System.out.println(calproductcnt);
			
			OrderVO ohp = new OrderVO(cur_order_seq, pr_id, pr_cnt);
			orderlist.add(ohp);
//			System.out.println(ohp.getOrder_seq()+" / "+ohp.getPr_id()+" / "+ohp.getPr_cnt());
//			System.out.println(ohp);
		}
		OrderVO orders = new OrderVO(total_price, user_id);
//		System.out.println(calproductcntlist);
//		System.out.println(orderlist);
//		System.out.println(orders.getTotal_price()+" / "+orders.getUser_id());
		OService.orderProcess(calproductcntlist, orderlist, orders);
		
		chk_query = OrderDAO.chkQuery();
		System.out.println(chk_query);
		if(chk_query) {
			session.removeAttribute(user_id+"_cart");
//			System.out.println("장바구니 session 삭제");
			response.sendRedirect("/FreeProject/menu/buyer/result/orderDone.jsp?OrderNO="+cur_order_seq);
		}
	}

}
