package controllerSet.itemControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class RegiNewItemC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		boolean query_chk = false;
		
		ItemVO item = (ItemVO)request.getAttribute("NewitemObj");
		String img_path = item.getItem_img();
		String db_upload_img_path = img_path.replaceAll("\\\\", "/");
		System.out.println(item.getItem_name()+" / "+item.getItem_manu()+" / "+item.getItem_manu_string()+" / "+item.getItem_class_string()+" / "+db_upload_img_path);
		int item_manu_val = Integer.parseInt(item.getItem_manu_string());
		int item_class_val = Integer.parseInt(item.getItem_class_string());
		
		ItemVO inputitem = new ItemVO(item.getItem_name(), item.getItem_manu(), item_manu_val, item_class_val, db_upload_img_path);
		System.out.println(inputitem.getItem_name()+" / "+inputitem.getItem_manu()+" / "+inputitem.getItem_manu_val()+" / "+inputitem.getItem_class_val()+" / "+inputitem.getItem_img());
		ItemService itemService = ItemService.getInstance();
		itemService.itemRegiNewItem(inputitem);
		query_chk = ItemDAO.chkQuery();
		
		if(query_chk) {
			//물품 등록 성공시
			response.sendRedirect("/FreeProject/menu/seller/item/result/itmeRegiDone.jsp");
		}else {
			//물품 등록 실패시 
			request.setAttribute("regi_error", "물품등록 도중 오류가 발생하였습니다.\n처음부터 다시 진행해 주십시오.");
			DispatcherUtil.forward(request, response, "");
		}
	}
}
