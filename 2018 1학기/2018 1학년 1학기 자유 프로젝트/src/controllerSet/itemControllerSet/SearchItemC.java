package controllerSet.itemControllerSet;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class SearchItemC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		boolean query_chk = false;
		
		String isPop = request.getParameter("pop");
		//popup으로 호출되는지, 아닌지 파악
		
		ItemService itemService = ItemService.getInstance();
		ArrayList<ItemVO> itemlist = itemService.itemSearchAll();
		
		if(isPop==null) {
			
		}else {
			request.setAttribute("itemlist", itemlist);
			DispatcherUtil.forward(request, response, "/menu/seller/item/popup/itemSearchPop.jsp");
		}
	}
}
