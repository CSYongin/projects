package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class MemberMyInfoC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String myInfo_chg_a = request.getParameter("chga");
		String myInfo_chg_pw = request.getParameter("chgpw");
		if(session.isNew()||session.getAttribute("id")==null) {
//			System.out.println("잘못된 접근입니다.");
			response.sendRedirect(request.getHeader("referer"));
		}else {
			if(myInfo_chg_a==null){
				if(myInfo_chg_pw.equals("Y")){
					String user_id = session.getAttribute("id").toString();
					String chg_member_pw = request.getParameter("my_info_new_pw_confirm");

					MemberService MemService = MemberService.getInstance();
					MemService.memberMyInfoPWUpdate(user_id, chg_member_pw);

					response.sendRedirect("/FreeProject/member/mypage/membership/myInfo.jsp");
				}else {
					System.out.println("잘못된 접근입니다.");
				}
			}else if(myInfo_chg_a.equals("Y")){
				String user_id = session.getAttribute("id").toString();
				String chg_member_email1 = request.getParameter("my_info_email1");
				String chg_member_email2 = request.getParameter("my_info_email2");
				String chg_member_phone1 = request.getParameter("my_info_phone1");
				String chg_member_phone2 = request.getParameter("my_info_phone2");
				String chg_member_phone3 = request.getParameter("my_info_phone3");
				int chg_member_postcode = Integer.parseInt(request.getParameter("my_info_postcode"));
				String chg_member_addr1 = request.getParameter("my_info_addr1");
				String chg_member_addr2 = request.getParameter("my_info_addr2");

				MemberVO member = new MemberVO(chg_member_phone1,chg_member_phone2,chg_member_phone3,chg_member_email1,chg_member_email2,chg_member_postcode,chg_member_addr1,chg_member_addr2);
				MemberService MemService = MemberService.getInstance();
				MemService.memberMyInfoUpdate(user_id, member);

				response.sendRedirect("/FreeProject/member/mypage/membership/myInfo.jsp");
			}else {
				System.out.println("잘못된 접근입니다.");
			}
		}
	}
}
