package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class MemberLoginC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String pastPath = request.getParameter("to_go_path");
		String loginPath = "/FreeProject/member/login/login.jsp";
		String togoPath = "/FreeProject/index.jsp";

		String log_usr_id = request.getParameter("login_usr_id");
		String log_usr_pw = request.getParameter("login_usr_pw");
		boolean query_chk = false;

		if(session.isNew()||session.getAttribute("id")==null) {
			if(pastPath.equals(loginPath)) {
				togoPath = "/FreeProject/index.jsp";
			}else {
				togoPath = pastPath;
			}
			MemberService MemService = MemberService.getInstance();
			MemberVO member = MemService.memberLoginSearch(log_usr_id);
			query_chk = MemberDAO.chkQuery();
			
			if(member==null||query_chk==false) {
//				System.out.println("아이디 없음");
				response.sendRedirect("/FreeProject/member/login/login.jsp?URI="+togoPath+"&error=logError1");
				return;
			}else {
//				System.out.println("아이디 같음");
				if(!log_usr_pw.equals(member.getUser_pw())) {
//					System.out.println("비밀번호 다름");
					response.sendRedirect("/FreeProject/member/login/login.jsp?URI="+togoPath+"&error=logError1");
					return;
				}else {
//					System.out.println("비밀번호 같음");
					session.setAttribute("id", log_usr_id);
					session.setAttribute(log_usr_id+"_Name", member.getUser_name());
					session.setAttribute(log_usr_id+"_Grant", member.getUser_grant_desc());
					session.setAttribute(log_usr_id+"_classify", member.getUser_classify_desc());
//					System.out.println("session 생성");
					response.sendRedirect(togoPath);
				}
			}
		}else {
//			System.out.println("이미 session 존재");
			//기존 로그인된 정보 삭제
			String last_id = (String)session.getAttribute("id");
			session.removeAttribute("id");
			session.removeAttribute(last_id+"_Name");
			session.removeAttribute(last_id+"_Grant");
			session.removeAttribute(last_id+"_Classify");
//			System.out.println("세션 삭제 성공");
			MemberService MemService = MemberService.getInstance();
			MemberVO member = MemService.memberLoginSearch(log_usr_id);
			session.setAttribute("id", log_usr_id);
			session.setAttribute(log_usr_id+"_Name", member.getUser_name());
			session.setAttribute(log_usr_id+"_Grant", member.getUser_grant_desc());
			session.setAttribute(log_usr_id+"_classify", member.getUser_classify_desc());
			response.sendRedirect(togoPath);
		}
	}
}
