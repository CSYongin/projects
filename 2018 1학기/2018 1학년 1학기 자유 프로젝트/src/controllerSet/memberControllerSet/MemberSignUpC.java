package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import serviceSet.*;
import dao.*;
import vo.*;

public class MemberSignUpC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		String path = request.getParameter("signUp_path");
		int new_user_grant=0;
		int new_user_classify=0;
		if(path.equals("individual")) {
			new_user_grant=20;
			new_user_classify=23;
		}else if(path.equals("seller")) {
			new_user_grant=10;
			new_user_classify=13;
		}
		String new_user_id = request.getParameter("new_usr_id");
		String new_user_pw = request.getParameter("new_usr_pw2");
		String new_user_name = request.getParameter("new_usr_name");
		String new_user_phone1 = request.getParameter("new_usr_phone1");
		String new_user_phone2 = request.getParameter("new_usr_phone2");
		String new_user_phone3 = request.getParameter("new_usr_phone3");
		String new_user_email1 = request.getParameter("new_usr_email1");
		String new_user_email2 = request.getParameter("new_usr_email2");
		int new_user_postcode = Integer.parseInt(request.getParameter("new_usr_postcode"));
		String new_user_addr1 = request.getParameter("new_usr_addr1");
		String new_user_addr2 = request.getParameter("new_usr_addr2");
		boolean query_chk = false;
		
//		System.out.println(new_user_id+"/"+new_user_pw+"/"+new_user_name+"/"+new_user_phone1+"/"+new_user_phone2+"/"+new_user_phone3+"/"+new_user_email1+"/"+new_user_email2+"/"+new_user_postcode+"/"+new_user_addr1+"/"+new_user_addr2+"/"+new_user_grant+"/"+new_user_classify);

		MemberVO member = new MemberVO(new_user_id, new_user_pw, new_user_name, new_user_phone1, new_user_phone2, new_user_phone3, new_user_email1, new_user_email2, new_user_addr1, new_user_addr2, new_user_postcode, new_user_grant, new_user_classify);
		MemberService MemService = MemberService.getInstance();
		MemService.memberSignUp(member);
		query_chk = MemberDAO.chkQuery();
		
		if(query_chk) {
			//회원가입 성공시
//			System.out.println("회원가입 성공");
			response.sendRedirect("/FreeProject/member/signUp/result/signUpDone.jsp");
		}else {
//			System.out.println("회원가입 실패");
			request.setAttribute("regi_error", "회원가입 도중 오류가 발생하였습니다.\n처음부터 다시 진행해 주십시오.");
			DispatcherUtil.forward(request, response, path);
		}
	}
}
