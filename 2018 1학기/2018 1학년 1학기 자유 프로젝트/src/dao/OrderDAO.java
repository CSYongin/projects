package dao;

import java.sql.*;
import java.util.ArrayList;

import vo.*;

public class OrderDAO {
	private static OrderDAO dao = new OrderDAO();
	private static boolean chk_query = false;

	private OrderDAO() {

	}
	public static OrderDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/project_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public int orderGetLastSeq() {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		int seq = 0;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT order_seq FROM orders ORDER BY order_seq DESC LIMIT 1;";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				seq = rs.getInt(1);
			}
//			System.out.println(query);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / chkid(String new_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return seq;
	}
	public void orderProcess(ArrayList<ProductVO> calproductcntlist, ArrayList<OrderVO> orderlist, OrderVO orders) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = null;
			for(ProductVO calcproduct:calproductcntlist) {
				query = "UPDATE v_productdata_all SET pr_cnt="+calcproduct.getPr_cnt()+" WHERE pr_id='"+calcproduct.getPr_id()+"';";
				System.out.println(query);
				stmt.executeUpdate(query);
			}
			query = "INSERT INTO orders(order_date,total_price,buyer_id) VALUES(NOW(),"+orders.getTotal_price()+",'"+orders.getUser_id()+"');";
			System.out.println(query);
			stmt.executeUpdate(query);
			for(OrderVO ohp:orderlist) {
				query = "INSERT INTO order_has_product VALUES("+ohp.getOrder_seq()+",'"+ohp.getPr_id()+"',"+ohp.getPr_cnt()+");";
				System.out.println(query);
				stmt.executeUpdate(query);
				chk_query=true;
			}
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / chkid(String new_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public ArrayList<OrderedDataVO> orderDone(int order_seq){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		ArrayList<OrderedDataVO> list = new ArrayList<OrderedDataVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = null;
			query = "SELECT * From v_orderdata_all WHERE order_seq="+order_seq+" ORDER BY order_seq DESC;";
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				OrderedDataVO orderdata = new OrderedDataVO(rs.getInt(1), rs.getDate(2), 
						rs.getInt(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getInt(8),
						rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getInt(13), rs.getString(14), rs.getString(15),
						rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19), rs.getInt(20), rs.getString(21), rs.getString(22));
				list.add(orderdata);
			}
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / chkid(String new_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
	public ArrayList<OrderedDataVO> orderBuyerOrderedCnt(String user_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		ArrayList<OrderedDataVO> list = new ArrayList<OrderedDataVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = null;
			query = "SELECT order_seq FROM orders WHERE buyer_id='"+user_id+"' ORDER BY order_seq DESC";
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				OrderedDataVO ordereddata = new OrderedDataVO(rs.getInt(1));
				list.add(ordereddata);
			}
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / chkid(String new_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
}
