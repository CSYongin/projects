package dao;

import java.sql.*;
import vo.*;

public class MemberDAO {
	private static MemberDAO dao = new MemberDAO();
	private static boolean chk_query = false;

	private MemberDAO() {

	}
	public static MemberDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/project_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}

	public boolean chkid(String new_user_id) {
		//아이디 중복 검사 Ajax/chkid.jsp에서 사용
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		boolean chkidquery = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT user_id FROM v_desc_userdata_all WHERE user_id='"+new_user_id+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				chkidquery = true;
			}
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / chkid(String new_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return chkidquery;
	}
	public void memberSignUp(MemberVO member) {
		//회원가입시 사용 DB
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "INSERT INTO userdata VALUES('"+member.getUser_id()+"',HEX(AES_ENCRYPT('"+member.getUser_pw()+"','csyongin')),'"+
					member.getUser_name()+"','"+member.getUser_phone1()+"','"+member.getUser_phone2()+"','"+member.getUser_phone3()+"','"+
					member.getUser_email1()+"','"+member.getUser_email2()+"','"+member.getUser_postcode()+"','"+member.getUser_addr1()+"','"+
					member.getUser_addr2()+"',"+member.getUser_grant()+","+member.getUser_classify()+");";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSingUp(MemberVO member)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public MemberVO memberLoginSearch(String user_id) {
		//로그인시 세션을 만들기 위해 사용하는 DB
		//회원 비밀번호, 회원이름, 회원 권한정보만 가져옴
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		MemberVO member = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT AES_DECRYPT(UNHEX(user_pw),'csyongin'),user_name,grant_desc,class_desc FROM v_desc_userdata_all WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				member = new MemberVO(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberLoginSearch(String user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return member;
	}
	public MemberVO memberMyInfoSearch(String user_id) {
		//회원정보 수정시 회원정보를 불러오기 위해 사용하는 DB
		//id,이름,전화번호,email,우편번호,주소를 불러옴
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		MemberVO member = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT user_id,user_name,user_phone1,user_phone2,user_phone3,user_email1,user_email2,user_postcode,user_addr1,user_addr2 FROM v_desc_userdata_all WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				member = new MemberVO(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getInt(8),rs.getString(9),rs.getString(10));
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberMyInfoSearch(String user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return member;
	}
	public String memberMyInfoPWSearch(String user_id) {
		//회원 정보 수정에서 비밀번호 수정시 기존 비밀번호 확인 Ajax에서 사용
		//Ajax/pwchk.jsp
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		String user_pw = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT AES_DECRYPT(UNHEX(user_pw),'csyongin') FROM v_desc_userdata_all WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				user_pw = rs.getString(1);
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberMyInfoSearch(String user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return user_pw;
	}
	public void memberMyInfoPWUpdate(String user_id, String new_user_pw) {
		//회원정보 수정에서 비밀번호 변경시 호출DB
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "UPDATE v_userdata_all SET user_pw=HEX(AES_ENCRYPT('"+new_user_pw+"','csyongin')) WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberMyInfoPWUpdate(String user_id, String new_user_pw)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public void memberMyInfoUpdate(String user_id, MemberVO member) {
		//회원정보 수정에서 회원정보 수정시 호출되는 DB
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "UPDATE v_userdata_all SET user_phone1='"+member.getUser_phone1()+"',user_phone2='"+member.getUser_phone2()+"',user_phone3='"+member.getUser_phone3()+
					"',user_email1='"+member.getUser_email1()+"',user_email2='"+member.getUser_email2()+"',user_postcode='"+member.getUser_postcode()+
					"',user_addr1='"+member.getUser_addr1()+"',user_addr2='"+member.getUser_addr2()+"' WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberMyInfoUpdate(String user_id, MemberVO member)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public void memberWithdrwal(String Withdrwal_user_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "DELETE FROM userdata WHERE user_id='"+Withdrwal_user_id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberWithdrwal(String Withdrwal_user_id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
}
