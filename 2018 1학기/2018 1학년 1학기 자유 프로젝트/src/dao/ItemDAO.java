package dao;

import java.sql.*;
import java.util.ArrayList;

import vo.*;

public class ItemDAO {
	private static ItemDAO dao = new ItemDAO();
	private static boolean chk_query = false;

	private ItemDAO() {

	}
	public static ItemDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/project_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public void itemRegiNewItem(ItemVO item) {
		//물품 등록시 사용
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "INSERT INTO item(item_name,item_manu,item_manu_val,item_class_val,item_img) VALUES('"+
			item.getItem_name()+"','"+item.getItem_manu()+"','"+item.getItem_manu_val()+"','"+item.getItem_class_val()+"','"+item.getItem_img()+"');";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ItemDao / itemRegiNewItem(ItemVO item)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public ArrayList<ItemVO> itemSearchAll(){
		//물품 전체 검색
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		ArrayList<ItemVO> itemlist = new ArrayList<ItemVO>();
		ItemVO item = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT * FROM v_desc_item_all;";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				item = new ItemVO(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
//				System.out.println(rs.getString(1)+" / "+rs.getString(2)+" / "+rs.getString(3)+" / "+rs.getString(4)+" / "+rs.getString(5)+" / "+rs.getString(6));
//				System.out.println(item.getItem_id()+" / "+item.getItem_name()+" / "+item.getItem_manu()+" / "+item.getItem_manu_string()+" / "+item.getItem_class_string()+" / "+item.getItem_img());
				itemlist.add(item);
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ItemDao / itemSearchAll()" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return itemlist;
	}
}
