package dao;

import java.sql.*;
import java.util.ArrayList;

import vo.*;

public class ProductDAO {
	private static ProductDAO dao = new ProductDAO();
	private static boolean chk_query = false;

	private ProductDAO() {

	}
	public static ProductDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/project_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public void productRegiNewProduct(ProductVO product) {
		//물품 등록시 사용
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "INSERT INTO products VALUES('"+product.getPr_id()+"','"+product.getItem_id()+"','"+product.getPr_name()+"','"+
			product.getPr_cnt()+"','"+product.getPr_price()+"',NOW(),'"+product.getSeller_id()+"');";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ProductDAO / productRegiNewProduct(ProductVO product)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public ArrayList<ProductVO> productShowProductSimple(String sel_brend, String sel_class){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ProductVO> productlist = new ArrayList<ProductVO>();
		ProductVO product = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT item_img, pr_id, pr_name, pr_price, seller_id, seller_name FROM v_desc_productdata_all WHERE manu_desc='"+sel_brend+"' AND class_desc='"+sel_class+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				product = new ProductVO(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getString(5),rs.getString(6));
				productlist.add(product);
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ProductDAO / productShowProductSimple(String sel_brend,String sel_class)" + e);
		}finally {
			close(conn, stmt, rs);
		}

		return productlist;
	}
	public ArrayList<ProductVO> productShowProductSimple(String sel_class){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ProductVO> productlist = new ArrayList<ProductVO>();
		ProductVO product = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT item_img, pr_id, pr_name, pr_price, seller_id, seller_name FROM v_desc_productdata_all WHERE class_desc='"+sel_class+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				product = new ProductVO(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getString(5),rs.getString(6));
				productlist.add(product);
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ProductDAO / productShowProductSimple(String sel_class)" + e);
		}finally {
			close(conn, stmt, rs);
		}

		return productlist;
	}
	public ProductVO productShowProductDetail(String pr_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ProductVO product = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT * FROM v_desc_productdata_all WHERE pr_id='"+pr_id+"';";
//			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				product = new ProductVO(rs.getString(1),rs.getString(2),rs.getInt(3),rs.getInt(4),
						rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),
						rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),
						rs.getInt(14),rs.getString(15),rs.getString(16));
			}
			chk_query=true;
		}catch(Exception e) {
			System.out.println("Error : dao.ProductDAO / productShowProductDetail" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return product;
	}
}
