/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - fruit
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fruit` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `buyerdata` */

DROP TABLE IF EXISTS `buyerdata`;

CREATE TABLE `buyerdata` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(12) NOT NULL,
  `apple_cnt` int(11) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `buyerdata_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `userdata` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `buyerdata` */

insert  into `buyerdata`(`seq`,`user_id`,`apple_cnt`,`money`) values 
(1,'user1',25,131500),
(2,'user2',NULL,5000);

/*Table structure for table `sellerdata` */

DROP TABLE IF EXISTS `sellerdata`;

CREATE TABLE `sellerdata` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(12) NOT NULL,
  `apple_cnt` int(11) DEFAULT NULL,
  `apple_price` int(11) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sellerdata_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `userdata` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sellerdata` */

insert  into `sellerdata`(`seq`,`user_id`,`apple_cnt`,`apple_price`,`money`) values 
(1,'seller1',25,3200,80000),
(2,'seller2',20,1500,7500),
(3,'seller3',5,2600,13000),
(4,'seller4',NULL,NULL,NULL);

/*Table structure for table `user_classification` */

DROP TABLE IF EXISTS `user_classification`;

CREATE TABLE `user_classification` (
  `user_class` int(11) NOT NULL,
  `class_value` varchar(10) NOT NULL,
  PRIMARY KEY (`user_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_classification` */

insert  into `user_classification`(`user_class`,`class_value`) values 
(1,'admin'),
(2,'seller'),
(3,'member');

/*Table structure for table `userdata` */

DROP TABLE IF EXISTS `userdata`;

CREATE TABLE `userdata` (
  `user_id` varchar(12) NOT NULL,
  `user_pw` varchar(16) NOT NULL,
  `user_classification` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_classification` (`user_classification`),
  CONSTRAINT `userdata_ibfk_1` FOREIGN KEY (`user_classification`) REFERENCES `user_classification` (`user_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `userdata` */

insert  into `userdata`(`user_id`,`user_pw`,`user_classification`) values 
('admin','cs1234',1),
('seller1','1111',2),
('seller2','2222',2),
('seller3','3333',2),
('seller4','4444',2),
('user1','1111',3),
('user2','2222',3),
('user3','3333',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
