package controllerSet.itemMapping;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import controllerSet.itemControllerSet.*;

public class ItemFC extends HttpServlet{

	HashMap<String, Controller> list = null;
	public void init(ServletConfig sc) throws ServletException{
		list = new HashMap<String,Controller>();
		list.put("/regi.IProc", new ItemRegiC());
		list.put("/search.IProc", new ItemSearchC());
	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String url = request.getRequestURI();
		String contextPath = request.getContextPath();
		String path = url.substring(contextPath.length());

		Controller subController = list.get(path);
		subController.execute(request, response);
	}
}