package controllerSet.bookControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class BookRentalC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String book_id = request.getParameter("input_rent_book_id");
		String user_id = request.getParameter("input_rent_user_id");
		
		libraryService LibService = libraryService.getInstance();
		LibService.rentBookProc(book_id, user_id);
	}
}
