package controllerSet.bookControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class BookRegiC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String book_id = request.getParameter("regi_book_id");
		String book_isbn = request.getParameter("input_regi_book_isbn");
		String user_id = "library";
		String book_state = "stand_by";
		
		bookVO book = new bookVO(book_id, book_isbn, user_id, book_state);
		bookService bookS = bookService.getInstance();
		bookS.bookRegister(book);
	}
}
