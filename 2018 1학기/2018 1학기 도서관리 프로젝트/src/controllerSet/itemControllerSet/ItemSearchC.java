package controllerSet.itemControllerSet;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class ItemSearchC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String isPop = request.getParameter("pop");
		
		ItemService IService = ItemService.getInstance();
		ArrayList<ItemVO> list = IService.itemSearch();
		ItemVO item = list.get(0);
		
		System.out.println(isPop);
		if(isPop==null) {
			
		}else {
			System.out.println(list);
			System.out.println(item.getItem_id());
			request.setAttribute("itemlist", list);
			DispatcherUtil.forward(request, response, "/item/popup/searchitem.jsp");
		}
	}
}
