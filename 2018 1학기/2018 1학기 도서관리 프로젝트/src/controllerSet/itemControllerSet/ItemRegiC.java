package controllerSet.itemControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class ItemRegiC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String isbn = request.getParameter("regi_book_isbn");
		String name = request.getParameter("input_regi_book_name");
		String author = request.getParameter("regi_book_author");
		String publisher = request.getParameter("regi_book_publiser");
		
		ItemVO item = new ItemVO(isbn, name, author, publisher);
		ItemService IService = ItemService.getInstance();
		IService.itemRegister(item);
		
		
	}
}
