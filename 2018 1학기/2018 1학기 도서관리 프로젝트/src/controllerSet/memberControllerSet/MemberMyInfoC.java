package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class MemberMyInfoC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String myInfo_chg_a = request.getParameter("chga");
		String myInfo_chg_pw = request.getParameter("chgpw");

		if(myInfo_chg_a==null){
			if(myInfo_chg_pw.equals("Y")){
				String user_id = session.getAttribute("id").toString();
				String chg_member_pw = request.getParameter("input_chg_member_pw");

				MemberService MemService = MemberService.getInstance();
				MemService.memberChangePW(user_id, chg_member_pw);

				response.sendRedirect("/Book/member/mypage/membership/myInfo.jsp");
			}else {
				System.out.println("잘못된 접근입니다.");
			}
		}else if(myInfo_chg_a.equals("Y")){
			String user_id = session.getAttribute("id").toString();
			String chg_member_email = request.getParameter("input_chg_member_email");
			String chg_member_phone = request.getParameter("input_chg_member_phone");
			int chg_member_postcode = Integer.parseInt(request.getParameter("input_chg_member_postcode"));
			String chg_member_addr1 = request.getParameter("input_chg_member_addr1");
			String chg_member_addr2 = request.getParameter("input_chg_member_addr2");
			
			MemberVO member = new MemberVO(user_id, chg_member_phone, chg_member_email, chg_member_postcode, chg_member_addr1, chg_member_addr2);
			MemberService MemService = MemberService.getInstance();
			MemService.memberUpdate(member);
			
			response.sendRedirect("/Book/member/mypage/membership/myInfo.jsp");
		}else {
			System.out.println("잘못된 접근입니다.");
		}
	}
}
