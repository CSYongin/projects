package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class MemberLoginC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String pastPath = request.getParameter("to_go_path");
		String loginPath = "/Book/member/login/login.jsp";
		String togoPath = "/Book/index.jsp";
		
		String log_usr_id = request.getParameter("login_usr_id");
		String log_usr_pw = request.getParameter("login_usr_pw");
		boolean query_chk = false;
		
		if(session.isNew()||session.getAttribute("id")==null) {
			if(pastPath.equals(loginPath)) {
				togoPath = "/Book/index.jsp";
			}else {
				togoPath = pastPath;
			}
			MemberService MemService = MemberService.getInstance();
			MemberVO member = MemService.memberSearch(log_usr_id);
			query_chk = MemberDAO.chkQuery();
			
			if(member==null) {
				System.out.println("아이디 없음");
				response.sendRedirect("/Book/member/login/login.jsp?URI="+togoPath+"&error=logError1");
				return;
			}else {
				System.out.println("아이디 같음");
				if(!log_usr_pw.equals(member.getUser_pw())) {
					System.out.println("비밀번호 다름");
					response.sendRedirect("/Book/member/login/login.jsp?URI="+togoPath+"&error=logError1");
					return;
				}else {
					System.out.println("비밀번호 같음");
					session.setAttribute("id", member.getUser_id());
					session.setAttribute(member.getUser_id()+"_Name", member.getUser_name());
					session.setAttribute(member.getUser_id()+"_Classify", member.getUser_classify());
					response.sendRedirect(togoPath);
				}
			}
		}else {
			System.out.println("이미 session 존재");
			String last_id = (String)session.getAttribute("id");
			session.removeAttribute("id");
			session.removeAttribute(last_id+"_Classify");
			System.out.println("세션 삭제 성공");
			MemberService MemService = MemberService.getInstance();
			MemberVO member = MemService.memberSearch(log_usr_id);
			session.setAttribute("id", member.getUser_id());
			session.setAttribute(member.getUser_id()+"_Name", member.getUser_name());
			session.setAttribute(member.getUser_id()+"_Classify", member.getUser_classify());
			response.sendRedirect(togoPath);
		}
	}
}
