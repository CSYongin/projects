package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class MemberSignUpC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		String path = request.getParameter("signUp_path");
		String new_usr_id = request.getParameter("input_new_usr_id");
		String new_usr_pw = request.getParameter("input_new_usr_pw");
		String new_usr_name = request.getParameter("input_new_usr_name");
		String new_usr_phone = request.getParameter("input_new_usr_phone");
		String new_usr_email = request.getParameter("input_new_usr_email");
		int new_usr_postcode = Integer.parseInt(request.getParameter("input_new_usr_postcode"));
		String new_usr_addr1 = request.getParameter("input_new_usr_addr1");
		String new_usr_addr2 = request.getParameter("input_new_usr_addr2");
		int new_usr_classify=0;
		boolean query_chk = false;
//		System.out.println(new_usr_id+"/"+new_usr_pw+"/"+new_usr_name+"/"+new_usr_phone+"/"+new_usr_email+"/"+new_usr_postcode+"/"+new_usr_addr1+"/"+new_usr_addr2+"/"+new_usr_classify);
		
		if(path.equals("individual")) {
			new_usr_classify = 3;
		}else if(path.equals("seller")) {
			new_usr_classify = 2;
		}
		MemberVO member = new MemberVO(new_usr_id, new_usr_pw, new_usr_name, new_usr_phone, new_usr_email, new_usr_postcode, new_usr_addr1, new_usr_addr2, new_usr_classify);
		MemberService MemService = MemberService.getInstance();
		MemService.memberSingUp(member);
		query_chk = MemberDAO.chkQuery();
		
		if(query_chk) {
			//회원가입 성공시
			System.out.println("회원가입 성공");
			
		}else {
			System.out.println("회원가입 실패");
		}
	}
}
