package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class MemberWithdraw implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
			
		if(session.isNew()||session.getAttribute("id")==null) {
			System.out.println("잘못된 접근입니다.");
			response.sendRedirect(request.getHeader("referer"));
		}else {
			String user_id = session.getAttribute("id").toString();
			MemberService MemService = MemberService.getInstance();
			MemService.memberWithdrwa(user_id);
			
			session.invalidate();
			
			response.sendRedirect("/Book/index.jsp");
		}
	}
}
