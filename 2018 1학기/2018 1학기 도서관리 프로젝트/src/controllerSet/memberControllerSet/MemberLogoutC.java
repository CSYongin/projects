package controllerSet.memberControllerSet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import controllerSet.*;
import dao.*;
import serviceSet.*;
import vo.*;

public class MemberLogoutC implements Controller{
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		
		if(session.isNew()||session.getAttribute("id")==null) {
			System.out.println("잘못된 접근입니다.");
			response.sendRedirect(request.getHeader("referer"));
		}else {
			session.invalidate();
			System.out.println("세션 삭제");
			response.sendRedirect("/Book/");
		}
	}
}
