package vo;

public class libraryVO {
	private String book_id, book_isbn, book_name, book_author, book_publisher, user_name, book_state, book_rent_date;
	
	public libraryVO() {
		
	}
	public libraryVO(String book_id,String book_name,String book_author,String book_publisher) {
		setBook_id(book_id);
		setBook_name(book_name);
		setBook_author(book_author);
		setBook_publisher(book_publisher);
	}
	public libraryVO(String book_id,String book_isbn, String book_name,String book_author,String book_publisher,String book_state) {
		setBook_id(book_id);
		setBook_isbn(book_isbn);
		setBook_name(book_name);
		setBook_author(book_author);
		setBook_publisher(book_publisher);
		setBook_state(book_state);
	}
	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}
	public String getBook_id() {
		return book_id;
	}
	public void setBook_isbn(String book_isbn) {
		this.book_isbn = book_isbn;
	}
	public String getBook_isbn() {
		return book_isbn;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_author(String book_author) {
		this.book_author = book_author;
	}
	public String getBook_author() {
		return book_author;
	}
	public void setBook_publisher(String book_publisher) {
		this.book_publisher = book_publisher;
	}
	public String getBook_publisher() {
		return book_publisher;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setBook_state(String book_state) {
		this.book_state = book_state;
	}
	public String getBook_state() {
		return book_state;
	}
	public void setBook_rent_date(String book_rent_date) {
		this.book_rent_date = book_rent_date;
	}
	public String getBook_rent_date() {
		return book_rent_date;
	}
}
