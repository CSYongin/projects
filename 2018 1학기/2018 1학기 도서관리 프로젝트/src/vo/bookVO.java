package vo;

public class bookVO {
	private String book_id, book_isbn, user_id, book_state;
	
	public bookVO() {
		
	}
	public bookVO(String book_id) {
		setBook_id(book_id);
	}
	public bookVO(String book_id,String book_isbn,String book_state) {
		setBook_id(book_id);
		setBook_isbn(book_isbn);
		setBook_state(book_state);
	}
	public bookVO(String book_id,String book_isbn,String user_id,String book_state) {
		setBook_id(book_id);
		setBook_isbn(book_isbn);
		setUser_id(user_id);
		setBook_state(book_state);
	}
	
	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}
	public String getBook_id() {
		return book_id;
	}
	public void setBook_isbn(String book_isbn) {
		this.book_isbn = book_isbn;
	}
	public String getBook_isbn() {
		return book_isbn;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setBook_state(String book_state) {
		this.book_state = book_state;
	}
	public String getBook_state() {
		return book_state;
	}
}
