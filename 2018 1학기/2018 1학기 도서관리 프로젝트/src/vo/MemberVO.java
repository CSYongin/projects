package vo;

public class MemberVO {
	private String user_id, user_pw, user_name, user_phone, user_email, user_addr1, user_addr2;
	private int new_usr_postcode, user_classify;

	public MemberVO() {
	}
	public MemberVO(String user_id, String user_phone, String user_email, int new_usr_postcode, String user_addr1, String user_addr2) {
		//회원정보 수정시 호출
		setUser_id(user_id);
		setUser_phone(user_phone);
		setUser_email(user_email);
		setUser_postcode(new_usr_postcode);
		setUser_addr1(user_addr1);
		setUser_addr2(user_addr2);
	}
	public MemberVO(String user_id, String user_pw, String user_name, String user_phone, String user_email, int new_usr_postcode, String user_addr1, String user_addr2, int user_classify) {
		//회원 가입시 모든 user정보 입력시 호출
		setUser_id(user_id);
		setUser_pw(user_pw);
		setUser_name(user_name);
		setUser_phone(user_phone);
		setUser_email(user_email);
		setUser_postcode(new_usr_postcode);
		setUser_addr1(user_addr1);
		setUser_addr2(user_addr2);
		setUser_classify(user_classify);
	}

	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_pw() {
		return user_pw;
	}
	public void setUser_pw(String user_pw) {
		this.user_pw = user_pw;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public int getUser_postcode() {
		return new_usr_postcode;
	}
	public void setUser_postcode(int new_usr_postcode) {
		this.new_usr_postcode = new_usr_postcode;
	}
	public String getUser_addr1() {
		return user_addr1;
	}
	public void setUser_addr1(String user_addr1) {
		this.user_addr1 = user_addr1;
	}
	public String getUser_addr2() {
		return user_addr2;
	}
	public void setUser_addr2(String user_addr2) {
		this.user_addr2 = user_addr2;
	}
	public int getUser_classify() {
		return user_classify;
	}
	public void setUser_classify(int user_classify) {
		this.user_classify = user_classify;
	}
}
