package vo;

public class ItemVO {
	private String item_id, item_name, author, publiser;
	
	public ItemVO() {
		
	}public ItemVO(String item_id, String item_name, String author, String publiser){
		setItem_id(item_id);
		setItem_name(item_name);
		setAuthor(author);
		setPublisher(publiser);
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAuthor() {
		return author;
	}
	public void setPublisher(String publiser) {
		this.publiser = publiser;
	}
	public String getPublisher() {
		return publiser;
	}
}
