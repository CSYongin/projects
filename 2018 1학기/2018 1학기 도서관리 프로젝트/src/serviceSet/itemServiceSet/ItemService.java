package serviceSet.itemServiceSet;

import vo.*;

import java.util.ArrayList;

import dao.itemDao.*;

public class ItemService {
	private static ItemService IService = new ItemService();
	public ItemDAO IDao = ItemDAO.getInstance();

	private ItemService(){}
	public static ItemService getInstance() {
		return IService;
	}
	public void itemRegister(ItemVO item) {
		IDao.itemRegister(item);
	}
	public ArrayList<ItemVO> itemSearch() {
		return IDao.itemSearch();
	}
	public ItemVO itemSearch(String id) {
		return IDao.itemSearch(id);
	}
}
