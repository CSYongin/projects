package serviceSet.memberServiceSet;

import vo.*;
import dao.memberDao.*;;

public class MemberService {
	private static MemberService memService = new MemberService();
	public MemberDAO memDao = MemberDAO.getInstance();

	private MemberService(){}

	public static MemberService getInstance() {
		return memService;
	}
	public void memberSingUp(MemberVO member) {
		memDao.memberSingUp(member);
	}
	public MemberVO memberSearch(String id) {
		return memDao.memberSearch(id);
	}
	public void memberChangePW(String id, String pw){
		memDao.memberChangePW(id, pw);
	}
	public void memberUpdate(MemberVO member) {
		memDao.memberUpdate(member);
	}
	public void memberWithdrwa(String id) {
		memDao.memberWithdrwa(id);
	}
}
