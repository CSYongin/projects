package serviceSet;

import vo.*;
import java.util.*;
import dao.*;

public class bookService {
	private static bookService BService = new bookService();
	public BookDAO Bdao = BookDAO.getInstance();

	private bookService(){}
	public static bookService getInstance() {
		return BService;
	}
	public void bookRegister(bookVO book) {
		Bdao.bookRegister(book);
	}
	public bookVO bookSearch(String book_id) {
		return Bdao.bookSearch(book_id);
	}
}
