package serviceSet;

import vo.*;
import java.util.*;
import dao.*;

public class libraryService {
	private static libraryService LibService = new libraryService();
	public LibDAO LDao = LibDAO.getInstance();

	private libraryService(){}
	public static libraryService getInstance() {
		return LibService;
	}
	public ArrayList<libraryVO> searchLibSmall(){
		return LDao.searchLibSmall();
	}
	public ArrayList<libraryVO> searchLib(String isbn){
		return LDao.searchLib(isbn);
	}
	public void rentBookProc(String book_id, String user_id) {
		LDao.rentBookProc(book_id, user_id);
	}
	public ArrayList<libraryVO> searchUserRental(String user_id){
		return LDao.searchUserRental(user_id);
	}
	public void returnBookProc(String book_id) {
		LDao.returnBookProc(book_id);
	}
}
