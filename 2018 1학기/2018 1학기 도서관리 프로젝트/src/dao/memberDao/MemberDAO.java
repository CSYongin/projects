package dao.memberDao;

import java.sql.*;
import vo.*;

public class MemberDAO {
	private static MemberDAO dao = new MemberDAO();
	private static boolean chk_query = false;

	private MemberDAO() {

	}
	public static MemberDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/book_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public void memberSingUp(MemberVO member) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String user_id = member.getUser_id();
			String user_pw = member.getUser_pw();
			String user_name = member.getUser_name();
			String user_phone = member.getUser_phone();
			String user_email = member.getUser_email();
			int user_postcode = member.getUser_postcode();
			String user_addr1 = member.getUser_addr1();
			String user_addr2 = member.getUser_addr2();
			int user_classify = member.getUser_classify();
			String query = "INSERT INTO userdata values('"+user_id+"','"+user_pw+"','"+user_name+"','"+user_phone+"','"+user_email+"','"+user_postcode+"','"+user_addr1+"','"+user_addr2+"','"+user_classify+"');";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSingUp(MemberVO member)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public MemberVO memberSearch(String id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		MemberVO member = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT * FROM userdata WHERE user_id='"+id+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				member = new MemberVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getInt(9));
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return member;
	}
	public void memberChangePW(String id, String pw){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try{
			conn = connect();
			stmt = conn.createStatement();
			String query = "UPDATE userdata SET user_pw='"+pw+"' WHERE user_id='"+id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e){
			System.out.println("Error : dao.memberDao / memberChangePW(String id, String pw)" + e);
		}finally{
			close(conn, stmt, rs);
		}
	}
	public void memberUpdate(MemberVO member) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String user_id = member.getUser_id();
			String user_phone = member.getUser_phone();
			String user_email = member.getUser_email();
			int user_postcode = member.getUser_postcode();
			String user_addr1 = member.getUser_addr1();
			String user_addr2 = member.getUser_addr2();
			String query = "UPDATE userdata SET user_phone='"+user_phone+"', user_email='"+user_email+"', user_postcode='"+user_postcode+"', user_addr1='"+user_addr1+"', user_addr2='"+user_addr2+"' WHERE user_id='"+user_id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSingUp(MemberVO member)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public void memberWithdrwa(String id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "DELETE FROM userdata WHERE user_id='"+id+"';";
//			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberWithdrwa(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
}
