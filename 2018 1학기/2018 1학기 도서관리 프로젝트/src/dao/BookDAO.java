package dao;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import vo.*;

public class BookDAO {
	private static BookDAO Bdao = new BookDAO();
	private static boolean chk_query = false;

	private BookDAO() {

	}
	public static BookDAO getInstance() {
		return Bdao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/book_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public void bookRegister(bookVO book) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			sdfDate.format(date).toString();
			conn = connect();
			stmt = conn.createStatement();
			String query = "INSERT INTO library(book_id,book_isbn,user_id,book_state) values('"+book.getBook_id()+"','"+book.getBook_isbn()+"','"+book.getUser_id()+"','"+book.getBook_state()+"');";
			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public bookVO bookSearch(String book_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		bookVO book = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT book_id FROM library WHERE book_id='"+book_id+"';";
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				book = new bookVO(rs.getString(1));
			}
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			
		}finally {
			close(conn, stmt, rs);
		}
		return book;
	}
}
