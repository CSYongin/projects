package dao;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import vo.*;

public class LibDAO {
	private static LibDAO dao = new LibDAO();
	private static boolean chk_query = false;

	private LibDAO() {

	}
	public static LibDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/book_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public ArrayList<libraryVO> searchLibSmall(){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		libraryVO library = null;
		ArrayList<libraryVO> list = new ArrayList<libraryVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT library.book_id,book_name,author,publisher FROM library LEFT JOIN bookdata ON library.book_isbn = bookdata.book_id ;";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				library = new libraryVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
				list.add(library);
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
	public ArrayList<libraryVO> searchLib(String isbn){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		libraryVO library = null;
		ArrayList<libraryVO> list = new ArrayList<libraryVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT library.book_id,book_isbn,book_name,author,publisher,book_state FROM library LEFT JOIN bookdata ON library.book_isbn = bookdata.book_id WHERE book_isbn='"+isbn+"';";
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				library = new libraryVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
				list.add(library);
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
	public ArrayList<libraryVO> searchUserRental(String user_id){
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		libraryVO library = null;
		ArrayList<libraryVO> list = new ArrayList<libraryVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT library.book_id,book_isbn,book_name,author,publisher,book_state FROM library LEFT JOIN bookdata ON library.book_isbn = bookdata.book_id WHERE user_id='"+user_id+"';";
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				library = new libraryVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
				list.add(library);
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
	public void rentBookProc(String book_id, String user_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			conn = connect();
			stmt = conn.createStatement();
			String query = "UPDATE library SET user_id='"+user_id+"', book_state='use', book_rent_date='"+sdfDate.format(date).toString()+"' WHERE book_id='"+book_id+"';";
			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public void returnBookProc(String book_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			conn = connect();
			stmt = conn.createStatement();
			String query = "UPDATE library SET user_id='library', book_state='stand_by', book_rent_date='1999-01-01' WHERE book_id='"+book_id+"';";
			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
}
