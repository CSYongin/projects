package dao.itemDao;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import vo.*;

public class ItemDAO {
	private static ItemDAO dao = new ItemDAO();
	private static boolean chk_query = false;

	private ItemDAO() {

	}
	public static ItemDAO getInstance() {
		return dao;
	}
	public Connection connect() {
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/book_datadb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String pwd = "cs1234";

			conn = DriverManager.getConnection(url,user,pwd);
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / getConnection : "+e);
			return null;
		}
		return conn;
	}
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / stmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if(conn != null) {
			try {
				conn.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / conn.close()" + e);
			}
		}
		if(pstmt != null) {
			try {
				pstmt.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / pstmt.close()" + e);
			}
		}
		if(rs != null) {
			try {
				rs.close();
			}catch(Exception e) {
				System.out.println("Error : dao.memberDao / rs.close()" + e);
			}
		}
	}
	public static boolean chkQuery() {
		//정상적인 DBquery 성공 여부 반환
		return chk_query;
	}
	public void itemRegister(ItemVO item) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			conn = connect();
			stmt = conn.createStatement();
			String query = "INSERT INTO bookdata(book_id, book_name, author, publisher, register_date) VALUES('"+item.getItem_id()+"','"+item.getItem_name()+"','"+item.getAuthor()+"','"+item.getPublisher()+"','"+sdfDate.format(date).toString()+"');";
			System.out.println(query);
			stmt.executeUpdate(query);
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
	}
	public ArrayList<ItemVO> itemSearch() {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		ItemVO item = null;
		ArrayList<ItemVO> list = new ArrayList<ItemVO>();
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT * FROM bookdata;";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				item = new ItemVO(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
				list.add(item);
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return list;
	}
	public ItemVO itemSearch(String id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		chk_query = false;
		ItemVO item = null;
		try {
			conn = connect();
			stmt = conn.createStatement();
			String query = "SELECT * FROM bookdata where book_id='"+id+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				item = new ItemVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}
			chk_query = true;
		}catch(Exception e) {
			System.out.println("Error : dao.memberDao / memberSearch(String id)" + e);
		}finally {
			close(conn, stmt, rs);
		}
		return item;
	}
}
