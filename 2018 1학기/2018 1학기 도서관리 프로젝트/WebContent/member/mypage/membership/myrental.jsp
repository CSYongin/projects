<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%
			String user_id = session.getAttribute("id").toString();
			libraryService LibService = libraryService.getInstance();
			ArrayList<libraryVO> list = LibService.searchUserRental(user_id);
			libraryVO lib = null;
		%>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<script type="text/javascript">
			function func_return_proc(rbi ,target){
				$.ajax({
					type : 'POST',
					data : {
						book_id : rbi
					},
					url : "/Book/Ajax/returnbook.jsp",
					dataType : "json",
					success : function(data){
						if(data.returnchk=="true"){
							$(target).parents("tr").detach();
						}
					},
					error : function(error){
						alert("error : "+error);
					}
				});
			};
		</script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div>
						<form action="" name="my_rental_list" id="my_rental_list">
							<table>
								<thead>
									<tr>
										<th>도서번호</th>
										<th>도서명</th>
										<th>저자명</th>
										<th>출판사</th>
										<th>반납</th>
									</tr>
								</thead>
								<tbody>
									<%
									for(int i=0;i<list.size();i++){
										lib = new libraryVO();
										lib = list.get(i);
										String book_id = lib.getBook_id();
										String book_isbn = lib.getBook_isbn();
										String book_name = lib.getBook_name();
										String book_author = lib.getBook_author();
										String book_publisher = lib.getBook_publisher();
									%>
										<tr>
											<td><%=book_id %></td>
											<td><%=book_name %></td>
											<td><%=book_author %></td>
											<td><%=book_publisher %></td>
											<td><input type="button" value="반납" name="return_book" id="return_book" onclick="func_return_proc('<%=book_id%>',this);"></td>
										</tr>
									<%
									}
									%>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>