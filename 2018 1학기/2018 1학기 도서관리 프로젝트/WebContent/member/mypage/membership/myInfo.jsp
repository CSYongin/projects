<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%
			String cur_member_id = session.getAttribute("id").toString();
			String cur_member_pw, cur_member_name , cur_member_addr1, cur_member_addr2, cur_member_phone, cur_member_phone1, cur_member_phone2, cur_member_phone3, cur_member_email, cur_member_email1, cur_member_email2;
			String pselect1="", pselect2="", pselect3="", pselect4="", eselect1="", eselect2="", eselect3="", eselect4="", eselect5="";
			int cur_member_postcode;
			MemberService MemService = MemberService.getInstance();
			MemberVO member = MemService.memberSearch(cur_member_id);

			cur_member_name = member.getUser_name();
			cur_member_pw = member.getUser_pw();
			cur_member_postcode = member.getUser_postcode();
			cur_member_addr1 = member.getUser_addr1();
			cur_member_addr2 = member.getUser_addr2();
			cur_member_phone = member.getUser_phone();
			cur_member_phone1 = cur_member_phone.substring(0, cur_member_phone.indexOf("-"));
			cur_member_phone2 = cur_member_phone.substring((cur_member_phone.indexOf("-")+1), cur_member_phone.lastIndexOf("-"));
			cur_member_phone3 = cur_member_phone.substring((cur_member_phone.lastIndexOf("-")+1));
			cur_member_email = member.getUser_email();
			cur_member_email1 = cur_member_email.substring(0, cur_member_email.indexOf("@"));
			cur_member_email2 = cur_member_email.substring((cur_member_email.indexOf("@")+1));

			if(cur_member_phone1.equals("010")){
				pselect1 = "selected";
			}else if(cur_member_phone1.equals("011")){
				pselect2 = "selected";
			}else if(cur_member_phone1.equals("017")){
				pselect3 = "selected";
			}else if(cur_member_phone1.equals("019")){
				pselect4 = "selected";
			}
			if(cur_member_email2.equals("naver.com")){
				eselect1 = "selected";
			}else if(cur_member_email2.equals("hanmail.net")){
				eselect2 = "selected";
			}else if(cur_member_email2.equals("nate.com")){
				eselect3 = "selected";
			}else if(cur_member_email2.equals("gmail.com")){
				eselect4 = "selected";
			}else{
				eselect5 = "selected";
			}
		%>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<!--jsp, jquery, json file-->
		<script src="/Book/Jquery/myinfoChk.js?ver=3"></script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<form action="" method="POST" name="my_ifno_form" id="my_ifno_form">
						<div class="MyinfoBasicTable">
							<table style="width: 100%">
								<colgroup>
									<col style="width: 16%">
									<col style="width: 27%">
									<col style="width: 17%">
									<col style="width: 40%">
								</colgroup>
								<tbody>
									<tr>
										<th>
											<span class="bnk">아이디</span>
										</th>
										<td>
											<span class="name">
												<span id="info-member_id"><%=cur_member_id%></span>
											</span>
										</td>
										<th>
											<span class="bnk">성명</span>
										</th>
										<td>
											<span class="name">
												<span id="info-member_name"><%=cur_member_name %></span>
											</span>
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">기존 비밀번호</span>
										</th>
										<td colspan="3">
											<input type="password" name="cur_member_pw" id="cur_member_pw" maxlength="12">
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">비밀번호 변경</span>
										</th>
										<td colspan="3">
											<div name="pw_change_bf" id="pw_change_bf" style="display: block">
												<a href="#" name="pw_change_btn" id="pw_change_btn">비밀번호 변경</a>
											</div>
											<div name="pw_change_af" id="pw_change_af" style="display: none">
												<input type="hidden" name="input_chg_member_pw" id="input_chg_member_pw">
												<p>
													<span class="dot">새로운 비밀번호</span>
													<input type="password" name="chg_member_pw" id="chg_member_pw" maxlength="16">
												</p>
												<p>
													<span class="dot">비밀번호 재확인</span>
													<input type="password" name="chg_member_pw_confirm" id="chg_member_pw_confirm" maxlength="16">
													<input type="button" value="비밀번호 변경" name="chk_input_chg_pw" id="chk_input_chg_pw">
												</p>
											</div>
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">이메일</span>
										</th>
										<td colspan="3">
											<input type="hidden" name="input_chg_member_email" id="input_chg_member_email" value="<%=cur_member_email%>">
											<input type="text" name="chg_member_email1" id="chg_member_email1" value="<%=cur_member_email1%>" size="8" maxlength="20" autocomplete="off">
											@
											<input type="text" name="chg_member_email2" id="chg_member_email2" value="<%=cur_member_email2%>" size="12" maxlength="20" autocomplete="off">
											<select id="chg_member_email2_email_selectbox" onchange="execEmailSelect(this.form)">
												<option value="" <%=eselect5%>>직접입력</option>
												<option value="naver.com" <%=eselect1%>>naver.com</option>
												<option value="hanmail.net" <%=eselect2%>>hanmail.net</option>
												<option value="nate.com" <%=eselect3%>>nate.com</option>
												<option value="gmail.com" <%=eselect4%>>gmail.com</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">전화번호</span>
										</th>
										<td colspan="3">
											<input type="hidden" name="input_chg_member_phone" id="input_chg_member_phone" value="<%=cur_member_phone%>">
											<select id="chg_member_phone1_selectbox" onchange="execPhoneSelect(this.form)">
												<option value="" >선택</option>
												<option value="010" <%=pselect1%>>010</option>
												<option value="011" <%=pselect2%>>011</option>
												<option value="017" <%=pselect3%>>017</option>
												<option value="019" <%=pselect4%>>019</option>
											</select>
											<input type="text" name="chg_member_phone1" id="chg_member_phone1" value="<%=cur_member_phone1%>" size="4" readonly>
											-
											<input type="text" name="chg_member_phone2" id="chg_member_phone2" value="<%=cur_member_phone2%>" size="4" maxlength="4" autocomplete="off">
											-
											<input type="text" name="chg_member_phone3" id="chg_member_phone3" value="<%=cur_member_phone3%>" size="4" maxlength="4" autocomplete="off">
										</td>
									</tr>
									<tr>
										<th>
											<span class="dot">주소</span>
										</th>
										<td colspan="3">
											<input type="hidden" name="input_chg_member_postcode" id="input_chg_member_postcode" value="<%=cur_member_postcode%>">
											<input type="hidden" name="input_chg_member_addr1" id="input_chg_member_addr1" value="<%=cur_member_addr1%>">
											<input type="hidden" name="input_chg_member_addr2" id="input_chg_member_addr2" value="<%=cur_member_addr2%>">
											<input type="text" name="chg_member_postcode" id="daum_postcode" placeholder="우편번호" readonly value="<%=cur_member_postcode%>">
											<input type="button" onclick="execDaumPostcode();" value="우편번호 찾기" name="">
											<input type="text" name="chg_member_addr1" id="daum_addr1" placeholder="주소" readonly value="<%=cur_member_addr1%>">
											<input type="text" name="chg_member_addr2" id="daum_addr2" placeholder="상세주소" value="<%=cur_member_addr2%>">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="MyinfoInput">
							<input type="button" value="회원 정보 수정" name="info_chg_submit" id="info_chg_submit">
							<input type="button" value="정보 수정 취소" onclick="history.back()">
							<input type="button" value="회원 탈퇴" name="withdrawal_btn" id="withdrawal_btn">
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>