<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<script src="/Book/Jquery/signUpChk.js?ver=3"></script>
		<script src="/Book/Jquery/signUpetc.js"></script>
		<script type="text/javascript">
			$(function(){
			<%
				String regi_error_msg = (String)request.getAttribute("regi_error");
				if(regi_error_msg==null){

				}else{%>
					alert("<%=regi_error_msg%>");
				<%}
			%>
			});
		</script>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="UserRegiAreaMain">
						<form action="/Book/signUp.MemProc" method="POST" id="reginewmemberForm">
							<input type="hidden" name="signUp_path" id="signUp_path" value="seller">
							<input type="hidden" name="input_new_usr_id" id="input_new_usr_id" value="">
							<input type="hidden" name="input_new_usr_id_chk" id="input_new_usr_id_chk" value="">
							<input type="hidden" name="input_new_usr_pw" id="input_new_usr_pw" value="">
							<input type="hidden" name="input_new_usr_name" id="input_new_usr_name" value="">
							<input type="hidden" name="input_new_usr_phone" id="input_new_usr_phone" value="">
							<input type="hidden" name="input_new_usr_email" id="input_new_usr_email" value="">
							<input type="hidden" name="input_new_usr_postcode" id="input_new_usr_postcode" value="">
							<input type="hidden" name="input_new_usr_addr1" id="input_new_usr_addr1" value="">
							<input type="hidden" name="input_new_usr_addr2" id="input_new_usr_addr2" value="">
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox Int_id">
										<span>
											<label class="Lable" id="id_lb" for="new_usr_id">아이디</label><br>
											<input type="text" name="new_usr_id" id="new_usr_id" value="" maxlength="15" autocomplete="off">
											<input type="button" name="id_chk" id="id_chk" value="중복확인">
										</span>
										<div class="ErrorMsg" id="idMsg" name="idMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox Int_pw1">
										<span>
											<label class="Lable" id="pw_pw1" for="new_usr_pw1">비밀번호</label><br>
											<input type="password" name="new_usr_pw1" id="new_usr_pw1" value="" maxlength="16" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="pw1Msg" name="pw1Msg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox Int_pw2">
										<span>
											<label class="Lable" id="pw_pw2" for="new_usr_pw2">비밀번호 확인</label><br>
											<input type="password" name="new_usr_pw2" id="new_usr_pw2" value="" maxlength="16" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="pw2Msg" name="pw2Msg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="name_lb" for=new_usr_name"">이름</label><br>
											<input type="text" name="new_usr_name" id="new_usr_name" value="" maxlength="10" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="nameMsg" name="nameMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="email_lb" for="new_usr_email1">이메일</label><br>
											<input type="text" name="new_usr_email1" id="new_usr_email1" value="" size="8" maxlength="20" autocomplete="off">
											@
											<input type="text" name="new_usr_email2" id="new_usr_email2" value="" size="12" maxlength="20" autocomplete="off">
											<select id="new_usr_email_selectbox" onchange="execEmailSelect(this.form)">
												<option value="" selected>직접입력</option>
												<option value="naver.com">naver.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="nate.com">nate.com</option>
												<option value="gmail.com">gmail.com</option>
											</select>
										</span>
										<div class="ErrorMsg" id="emailMsg" name="emailMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div class="Join_Row">
									<div class="RegiBox">
										<span>
											<label class="Label" id="phone_lb" for="new_usr_phone1">전화번호</label><br>
											<select id="new_usr_phone1_selectbox" onchange="execPhoneSelect(this.form)">
												<option value="" selected>선택</option>
												<option value="010">010</option>
												<option value="011">011</option>
												<option value="017">017</option>
												<option value="019">019</option>
											</select>
											<input type="text" name="new_usr_phone1" id="new_usr_phone1" value="" size="4" readonly>
											-
											<input type="text" name="new_usr_phone2" id="new_usr_phone2" value="" size="4" maxlength="4" autocomplete="off">
											-
											<input type="text" name="new_usr_phone3" id="new_usr_phone3" value="" size="4" maxlength="4" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="phoneMsg" name="phoneMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="RowGroup">
								<div class="Join_Row">
									<div class="RegiBox">
										<div class="UserAddress" id="UserAddress">
											<label class="Label" id="addr_lb" for="daum_postcode">주소</label><br>
											<input type="text" name="usr_postcode" id="daum_postcode" placeholder="우편번호" readonly>
											<input type="button" onclick="execDaumPostcode();" value="우편번호 찾기" name=""><br>
											<input type="text" name="usr_addr1" id="daum_addr1" placeholder="주소" readonly><br>
											<input type="text" name="usr_addr2" id="daum_addr2" placeholder="상세주소">
										</div>
										<div class="ErrorMsg" id="addrMsg" name="addrMsg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
							</div>
							<div class="ErrorMsg" id="submitMsg" name="submitMsg" style="display: none">입력하신 정보를 다시 확인해 주세요.</div>
							<div>
								<input type="submit" value="가입하기" name="" id="signUPbtn">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>