$(function(){
	//비밀번호 변경 클릭시 비밀번호 변경창으로 변경
	$("#pw_change_btn").click(function(){
		$("#pw_change_bf").css("display", "none")
		$("#pw_change_af").css("display", "block")
	});
	//비밀번호 변경 버튼 클릭시 비밀번호 유효성 검사
	$("#chk_input_chg_pw").click(function(){
		if($("#cur_member_pw").val()==""){
			alert("비밀번호를 변경하려면 기존 비밀번호를 입력 하셔야 합니다.");
			$("#cur_member_pw").focus();
			return;
		}else{
			$.ajax({
				type : "POST",
				url : "/Book/Ajax/pwchk.jsp",
				dataType : "json",
				data : {
					cur_member_pw_chk : $("#cur_member_pw").val()
				},
				success : function(data){
					if(data.passwordchk=="true"){
						if($("#chg_member_pw").val()==""||$("#chg_member_pw_confirm").val()==""){
							if($("#chg_member_pw").val()==""){
								alert("새로운 비밀번호를 입력하세요");
								$("#chg_member_pw").focus();
								return;
							}else{
								alert("비밀번호 확인을 입력하세요");
								$("#chg_member_pw").focus();
								return;
							}
						}else if($("#chg_member_pw").val()!=$("#chg_member_pw_confirm").val()){
							alert("비밀번호가 일치하지 않습니다.\n비밀번호를 다시 확인하세요");
							$("#chg_member_pw").focus();
								return;
						}else{
							$("#input_chg_member_pw").attr('value', $("#chg_member_pw_confirm").val());
							document.my_ifno_form.action="/Book/myInfo.MemProc?chgpw=Y";
							document.my_ifno_form.submit();
						}
					}else{
						alert("기존 비밀번호가 맞지 않습니다.");
						$("#cur_member_pw").focus();
						return;
					}
				},
				error : function(){
					alert("ajax 실행 실패");
					return;
				}
			})
		}
	});
	//이하 회원정보 변경시 focuin / focusout시 유효성 검사
	$("#chg_member_email1")
	.focusin(function(){
		$("#input_chg_member_email").attr('value', "");
	})
	.focusout(function(){
		if($("#chg_member_email1").val()==""){
			// $("#emailMsg").css("display", "block");
		}else{
			// $("#emailMsg").css("display", "none");
			if($("#chg_member_email1").val()==""||$("#chg_member_email2").val()==""){
				$("#input_chg_member_email").attr('value', "");
			}else{
				$("#input_chg_member_email").attr('value', $("#chg_member_email1").val()+"@"+$("#chg_member_email2").val());
			}
		}
	});
	$("#chg_member_email2")
	.focusin(function(){
		$("#input_chg_member_email").attr('value', "");
	})
	.focusout(function(){
		if($("#chg_member_email1").val()==""||$("#chg_member_email2").val()==""){
			// $("#emailMsg").css("display", "block");
		}else{
			// $("#emailMsg").css("display", "none");
			if($("#chg_member_email1").val()==""||$("#chg_member_email2").val()==""){
				$("#input_chg_member_email").attr('value', "");
			}else{
				$("#input_chg_member_email").attr('value', $("#chg_member_email1").val()+"@"+$("#chg_member_email2").val());
			}
		}
	});
	$("#chg_member_phone2")
	.focusin(function(){
		$("#input_chg_member_phone").attr('value', "");
	})
	.focusout(function(){
		if($("#chg_member_phone1").val()==""||$("#chg_member_phone2").val()==""){
			if($("#chg_member_phone1").val()==""){
				// $("#phoneMsg").css("display", "block");
			}else{
				// $("#phoneMsg").css("display", "block");
			}
		}else{
			// $("#phoneMsg").css("display", "none");
			if($("#chg_member_phone1").val()==""||$("#chg_member_phone2").val()==""||$("#chg_member_phone3").val()==""){
				$("#input_chg_member_phone").attr('value', "");
			}else{
				$("#input_chg_member_phone").attr('value', $("#chg_member_phone1").val()+"-"+$("#chg_member_phone2").val()+"-"+$("#chg_member_phone3").val());
			}
		}
	});
	$("#chg_member_phone3")
	.focusin(function(){
		$("#input_chg_member_phone").attr('value', "");
	})
	.focusout(function(){
		if($("#chg_member_phone1").val()==""||$("#chg_member_phone2").val()==""||$("#chg_member_phone3").val()==""){
			if($("#chg_member_phone1").val()==""){
				// $("#phoneMsg").css("display", "block");
			}else if($("#chg_member_phone2").val()==""){
				// $("#phoneMsg").css("display", "block");
			}else{
				// $("#phoneMsg").css("display", "block");
			}
		}else{
			// $("#phoneMsg").css("display", "none");
			if($("#chg_member_phone1").val()==""||$("#chg_member_phone2").val()==""||$("#chg_member_phone3").val()==""){
				$("#input_chg_member_phone").attr('value', "");
			}else{
				$("#input_chg_member_phone").attr('value', $("#chg_member_phone1").val()+"-"+$("#chg_member_phone2").val()+"-"+$("#chg_member_phone3").val());
			}
		}
	});
	$("#daum_addr2")
	.focusin(function(){
		$("#input_chg_member_addr2").attr('value', "");
	})
	.focusout(function(){
		if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
			// $("#addrMsg").css("display", "block");
		}else{
			if($("#daum_addr2").val()==""){
				// $("#addrMsg").css("display", "block");
			}else{
				// $("#addrMsg").css("display", "none");
				$("#input_chg_member_postcode").attr('value', $("#daum_postcode").val());
				$("#input_chg_member_addr1").attr('value', $("#daum_addr1").val());
				$("#input_chg_member_addr2").attr('value', $("#daum_addr2").val());
			}
		}
	});
	$("#info_chg_submit").click(function(){
		//회원정보 수정 버튼 클릭시 유효성에 맞지 않으면 error메세지 표시
		var user_phone = $("#input_chg_member_phone").val();
		var user_email = $("#input_chg_member_email").val();
		var user_zipcode = $("#input_chg_member_postcode").val();
		var user_addr1 = $("#input_chg_member_addr1").val();
		var user_addr2 = $("#input_chg_member_addr2").val();

		if($("#cur_member_pw").val()==""){
			alert("회원 정보를 수정하시려면 기존 비밀번호를 입력 하셔야 합니다.");
			$("#cur_member_pw").focus();
			return;
		}else{
			$.ajax({
				type : "POST",
				url : "/Book/Ajax/pwchk.jsp",
				dataType : "json",
				data : {
					cur_member_pw_chk : $("#cur_member_pw").val()
				},
				success : function(data){
					if(data.passwordchk=="true"){
						if(user_phone==""||user_email==""||user_zipcode==""||user_addr1==""||user_addr2==""){
							if(user_email==""){
								if($("#chg_member_email1").val()==""){
									alert("이메일 주소를 입력해 주세요.");
									$("#chg_member_email1").focus();
									return;
								}else{
									alert("이메일 주소를 입력해 주세요.");
									$("#chg_member_email2").focus();
									return;
								}
							}else if(user_phone==""){
								if($("#chg_member_phone1").val()==""){
									alert("전화번호 첫번째 번호를 입력해 주세요.");
									return;
								}else{
									if($("#chg_member_phone2").val()==""){
										alert("전화번호 가운데 번호를 입력해 주세요.");
										$("#chg_member_phone2").focus();
										return;
									}else{
										alert("전화번호 끝자리 번호를 입력해 주세요.");
										$("#chg_member_phone3").focus();
										return;
									}
								}
							}else if(user_zipcode==""||user_addr1==""||user_addr2==""){
								if(user_zipcode==""||user_addr1==""){
									alert("우편번호를 입력해 주세요.");
									return;
								}else{
									alert("상세주소주소를 입력해 주세요.");
									$("#daum_addr2").focus();
									return;
								}
							}
						}else{
							document.my_ifno_form.action="/Book/myInfo.MemProc?chga=Y";
							document.my_ifno_form.submit();
						}
					}else{
						alert("기존 비밀번호가 맞지 않습니다.");
						$("#cur_member_pw").focus();
						return;
					}
				},
				error : function(){
					alert("ajax 실행 실패");
					return;
				}
			})
		}
	});
	$("#withdrawal_btn").click(function(){
		//회원 탈퇴 버튼 클릭시 
		var withdrawal_confrim = confirm("회원탈퇴시 모든 정보를 읽게 됩니다.\n그래도 진행하시겠습니까?")
		if(withdrawal_confrim==true){
			location.href = "/Book/withdraw.MemProc";
		}else{
			return;
		}
	});
});