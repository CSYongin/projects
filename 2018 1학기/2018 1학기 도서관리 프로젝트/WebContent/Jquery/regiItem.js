var sel_file;
$(function(){
	$("#item_img_input_btn").on("change", handleImgFileSelect);
	$("#regi_book_isbn")
	.focusin(function(){
		$("#input_regi_book_isbn").attr('value', "");
	})
	.focusout(function(){
		if($("#regi_book_isbn").val()==""){
			$("#isbnMsg").css("display", "block");
		}else{
			$("#isbnMsg").css("display", "none");
			$("#input_regi_book_isbn").attr('value', $("#regi_book_isbn").val());
		}
	});
	$("#regi_book_name")
	.focusin(function(){
		$("#input_regi_book_name").attr('value', "");
	})
	.focusout(function(){
		if($("#regi_book_name").val()==""){
			$("#nameMsg").css("display", "block");
		}else{
			$("#nameMsg").css("display", "none");
			$("#input_regi_book_name").attr('value', $("#regi_book_name").val());
		}
	});
	$("#regi_book_author")
	.focusin(function(){
		$("#input_regi_book_author").attr('value', "");
	})
	.focusout(function(){
		if($("#regi_book_author").val()==""){
			$("#authorMsg").css("display", "block");
		}else{
			$("#authorMsg").css("display", "none");
			$("#input_regi_book_author").attr('value', $("#regi_book_author").val());
		}
	});
	$("#regi_book_publiser")
	.focusin(function(){
		$("#input_regi_book_publiser").attr('value', "");
	})
	.focusout(function(){
		if($("#regi_book_publiser").val()==""){
			$("#publiserMsg").css("display", "block");
		}else{
			$("#publiserMsg").css("display", "none");
			$("#input_regi_book_publiser").attr('value', $("#regi_book_publiser").val());
		}
	});
	$("#regi_book_submit").click(function(){
		if($("#input_regi_book_isbn").val()==""){
			$("#submitMsg").css("display", "block");
			alert("isbn번호를 입력해 주세요.");
			$("#input_regi_book_isbn").focus();
			return false;
		}else{
			$.ajax({
				type : "POST",
				url : "/Book/Ajax/isbnchk.jsp",
				dataType : "json",
				data : {
					check_isbn : $("#regi_book_isbn").val()
				},
				success : function(data){
					if(data.isbnschk=="true"){
						if($("#input_regi_book_name").val()==""||$("#input_regi_book_author").val()==""||$("#input_regi_book_publiser").val()==""){
							$("#submitMsg").css("display", "block");
							if($("#input_regi_book_name").val()==""){
								alert("도서명을 입력해 주세요.")
								$("#input_regi_book_name").focus();
								return false;
							}else if($("#input_regi_book_author").val()==""){
								alert("저자명을 입력해 주세요.")
								$("#input_regi_book_author").focus();
								return false;
							}else{
								alert("출판사 정보를 입력해 주세요.")
								$("#input_regi_book_author").focus();
								return false;
							}
						}else{
							document.register_item_form.action="/Book/regi.IProc";
							document.register_item_form.submit();
						}
					}else{
						alert("isbn이 이미 존재하는 책입니다.");
						return;
					}
				},
				error : function(error){
					alert("ajax 실행 실패");
					return;
				}
			});
		}
	});
});
function handleImgFileSelect(e){
	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);

	filesArr.forEach(function(f){
		if(!f.type.match("image.*")){
			alert("확장자는 이미지 확장자만 가능합니다.");
			return;
		}
		sel_file = f;
		var reader = new FileReader();
		reader.onload = function(e){
			$("#item_preview_img").attr("src", e.target.result);
		}
		reader.readAsDataURL(f);
	});
}