$(function(){
	$("#id_chk").click(function(){
		//신규 아이디 존재 여부 검사
		$.ajax({
			type : 'POST',
			url : "/Book/Ajax/idchk.jsp",
			dataType : "json",
			data : {
				check_id : $("#new_usr_id").val()
			},
			success : function(data){
				if(data.idchk=="true"){
					alert("사용 가능한 아이디 입니다.");
					$("#input_new_usr_id_chk").attr('value', 'true');
				}else{
					alert("아이디가 존재합니다. 다른 아이디를 입력해 주세요.");
					$("#input_new_usr_id_chk").attr('value', 'false');
					$("#new_usr_id").focus();
				}
			},
			error : function(error){
				alert("ajax 실행 실패");
			}
		});
	});
	//이하 회원가입시 각각 버튼의 focusin / focusout시 유효성 검사
	$("#new_usr_id")
	.focusin(function(){
		$("#input_new_usr_id").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_id").val()==""){
			$("#idMsg").css("display", "block");
		}else{
			$("#idMsg").css("display", "none");
			$("#input_new_usr_id").attr('value', $("#new_usr_id").val());
		}
	});
	$("#new_usr_pw1")
	.focusin(function(){
		$("#input_new_usr_pw").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_pw1").val()==""){
			$("#pw1Msg").css("display", "block");
		}else{
			$("#pw1Msg").css("display", "none");
		}
	});
	$("#new_usr_pw2")
	.focusin(function(){
		$("#input_new_usr_pw").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_pw1").val()==""){
			$("#pw1Msg").css("display", "block");
		}else if($("#new_usr_pw2").val()==""){
			$("#pw2Msg").css("display", "block");
		}else{
			if($("#new_usr_pw1").val()!=$("#new_usr_pw2").val()){
				$("#pw2Msg").html("비밀번호가 일치하지 않습니다.")
			}else{
				$("#pw2Msg").css("display", "none");
				$("#input_new_usr_pw").attr('value', $("#new_usr_pw2").val());
			}
		}
	});
	$("#new_usr_name")
	.focusin(function(){
		$("#input_new_usr_name").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_name").val()==""){
			$("#nameMsg").css("display", "block");
		}else{
			$("#nameMsg").css("display", "none");
			$("#input_new_usr_name").attr('value', $("#new_usr_name").val());
		}
	});
	$("#new_usr_email1")
	.focusin(function(){
		$("#input_new_usr_email").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_email1").val()==""){
			$("#emailMsg").css("display", "block");
		}else{
			$("#emailMsg").css("display", "none");
			if($("#new_usr_email1").val()==""||$("#new_usr_email2").val()==""){
				$("#input_new_usr_email").attr('value', "");
			}else{
				$("#input_new_usr_email").attr('value', $("#new_usr_email1").val()+"@"+$("#new_usr_email2").val());
			}
		}
	});
	$("#new_usr_email2")
	.focusin(function(){
		$("#input_new_usr_email").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_email1").val()==""||$("#new_usr_email2").val()==""){
			$("#emailMsg").css("display", "block");
		}else{
			$("#emailMsg").css("display", "none");
			if($("#new_usr_email1").val()==""||$("#new_usr_email2").val()==""){
				$("#input_new_usr_email").attr('value', "");
			}else{
				$("#input_new_usr_email").attr('value', $("#new_usr_email1").val()+"@"+$("#new_usr_email2").val());
			}
		}
	});
	$("#new_usr_phone2")
	.focusin(function(){
		$("#input_new_usr_phone").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_phone1").val()==""||$("#new_usr_phone2").val()==""){
			if($("#new_usr_phone1").val()==""){
				$("#phoneMsg").css("display", "block");
			}else{
				$("#phoneMsg").css("display", "block");
			}
		}else{
			$("#phoneMsg").css("display", "none");
			if($("#new_usr_phone1").val()==""||$("#new_usr_phone2").val()==""||$("#new_usr_phone3").val()==""){
				$("#input_new_usr_phone").attr('value', "");
			}else{
				$("#input_new_usr_phone").attr('value', $("#new_usr_phone1").val()+"-"+$("#new_usr_phone2").val()+"-"+$("#new_usr_phone3").val());
			}
		}
	});
	$("#new_usr_phone3")
	.focusin(function(){
		$("#input_new_usr_phone").attr('value', "");
	})
	.focusout(function(){
		if($("#new_usr_phone1").val()==""||$("#new_usr_phone2").val()==""||$("#new_usr_phone3").val()==""){
			if($("#new_usr_phone1").val()==""){
				$("#phoneMsg").css("display", "block");
			}else if($("#new_usr_phone2").val()==""){
				$("#phoneMsg").css("display", "block");
			}else{
				$("#phoneMsg").css("display", "block");
			}
		}else{
			$("#phoneMsg").css("display", "none");
			if($("#new_usr_phone1").val()==""||$("#new_usr_phone2").val()==""||$("#new_usr_phone3").val()==""){
				$("#input_new_usr_phone").attr('value', "");
			}else{
				$("#input_new_usr_phone").attr('value', $("#new_usr_phone1").val()+"-"+$("#new_usr_phone2").val()+"-"+$("#new_usr_phone3").val());
			}
		}
	});
	$("#daum_addr2")
	.focusin(function(){
		$("#input_new_usr_addr2").attr('value', "");
	})
	.focusout(function(){
		if($("#daum_postcode").val()==""||$("#daum_addr1").val()==""){
			$("#addrMsg").css("display", "block");
		}else{
			if($("#daum_addr2").val()==""){
				$("#addrMsg").css("display", "block");
			}else{
				$("#addrMsg").css("display", "none");
				$("#input_new_usr_postcode").attr('value', $("#daum_postcode").val());
				$("#input_new_usr_addr1").attr('value', $("#daum_addr1").val());
				$("#input_new_usr_addr2").attr('value', $("#daum_addr2").val());
			}
		}
	});
	$("#signUPbtn").click(function(){
		//회원가입 버튼 클릭시 유효성 검사 후, 맞지 않는 값이 있을 경우 error문구 표시 
		var user_id = $("#input_new_usr_id").val();
		var user_id_chk = $("#input_new_usr_id_chk").val();
		var user_pw = $("#input_new_usr_pw").val();
		var user_name = $("#input_new_usr_name").val();
		var user_phone = $("#input_new_usr_phone").val();
		var user_email = $("#input_new_usr_email").val();
		var user_zipcode = $("#input_new_usr_postcode").val();
		var user_addr1 = $("#input_new_usr_addr1").val();
		var user_addr2 = $("#input_new_usr_addr2").val();

		if(user_id==""||user_id_chk==""||user_id_chk=="false"||user_name==""||user_phone==""||user_email==""||user_zipcode==""||user_addr1==""||user_addr2==""){
			$("#submitMsg").css("display", "block");
			if(user_id==""){
				alert("아이디를 입력해 주세요.");
				$("#new_usr_id").focus();
				event.preventDefault();
				return false;
			}else if(user_id_chk==""||user_id_chk=="false"){
				alert("아이디를 중복 검사를 하지 않았습니다.");
				event.preventDefault();
				return false;
			}else if(user_pw==""){
				if($("#new_usr_pw1").val()==""){
					alert("비밀번호를 입력해 주세요.");
					$("#new_usr_pw1").focus();
					event.preventDefault();
				}else{
					if($("#new_usr_pw2").val()==""){
						alert("비밀번호 확인을 입력해 주세요.");
						$("#new_usr_pw2").focus();
						event.preventDefault();
					}else{
						alert("비밀번호가 일치하지 않습니다.");
						$("#new_usr_pw2").focus();
						event.preventDefault();
					}
				}
			}else if(user_name==""){
				alert("이름을 입력해 주세요.");
				$("#new_usr_name").focus();
				event.preventDefault();
			}else if(user_email==""){
				if($("#new_usr_email1").val()==""){
					alert("이메일 주소를 입력해 주세요.");
					$("#new_usr_email1").focus();
					event.preventDefault();
				}else{
					alert("이메일 주소를 입력해 주세요.");
					$("#new_usr_email2").focus();
					event.preventDefault();
				}
			}else if(user_phone==""){
				if($("#new_usr_phone1").val()==""){
					alert("전화번호 첫번째 번호를 입력해 주세요.");
					event.preventDefault();
				}else{
					if($("#new_usr_phone2").val()==""){
						alert("전화번호 가운데 번호를 입력해 주세요.");
						$("#new_usr_phone2").focus();
						event.preventDefault();
					}else{
						alert("전화번호 끝자리 번호를 입력해 주세요.");
						$("#new_usr_phone3").focus();
						event.preventDefault();
					}
				}
			}else if(user_zipcode==""||user_addr1==""||user_addr2==""){
				if(user_zipcode==""||user_addr1==""){
					alert("우편번호를 입력해 주세요.");
					event.preventDefault();
				}else{
					alert("상세주소주소를 입력해 주세요.");
					$("#daum_addr2").focus();
					event.preventDefault();
				}
			}
		}else{

		}
	})
});