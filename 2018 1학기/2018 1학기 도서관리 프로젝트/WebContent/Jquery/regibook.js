$(function(){
	$("#open_book_info").click(function(){
		var win_search_item = window.open("/Book/search.IProc?pop=Y" ,"_blank","width=800,height=450,resizable=no,scrollbars=no,status=no");
	})
	$("#regi_book_id").focusout(function(){
		if($("#regi_book_id").val()==""){
			$("#bookidMsg").css("display", "block");
		}else{
			$("#bookidMsg").css("display", "none");
		}
	})
	$("#regi_book_submit").click(function(){
		if($("#regi_book_id").val()==""){
			alert("등록할 도서 번호를 입력하지 않았습니다.\n도서번호를 입력해 주세요");
			$("#regi_book_id").focus();
			return false;
		}else{
			$.ajax({
				type : "POST",
				url : "/Book/Ajax/bookidchk.jsp",
				dataType : "json",
				data : {
					check_book_id : $("#regi_book_id").val()
				},
				success : function(data){
					if(data.bookidchk=="true"){
						if($("#input_regi_book_isbn").val()==""){
							alert("도서를 선택해 주세요.")
							return false;
						}else{
							document.regi_new_book_form.action="/Book/regi.BProc";
							document.regi_new_book_form.submit();
						}
					}else{
						alert("이미 등록된 도서 번호 입니다.\n다른 번호를 입력해 주세요")
						$("#regi_book_id").focus();
						return false;
					}
				},
				error : function(error){
					alert("ajax 실행 실패");
					return false;
				}
			});
		}
	})
});