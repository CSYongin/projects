<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<script type="text/javascript">
			function set_item_detail(seq){
				opener.parent.document.getElementById("input_regi_book_isbn").value = document.getElementById(seq+"_isbn").value;
				opener.parent.document.getElementById("regi_book_name").value = document.getElementById(seq+"_name").value;
				opener.parent.document.getElementById("regi_book_author").value = document.getElementById(seq+"_author").value;
				opener.parent.document.getElementById("regi_book_publiser").value = document.getElementById(seq+"_publisher").value;
				window.close();
			}
		</script>
	</head>
	<body>
		<%
			ArrayList<ItemVO> list = (ArrayList<ItemVO>)(request.getAttribute("itemlist"));
			ItemVO item = null;
			String isbn, name, author, publisher;
		%>
		<table>
			<thead>
				<tr>
					<th>도서명</th>
					<th>저자명</th>
					<th>출판사</th>
					<th>선택</th>
				</tr>
			</thead>
			<tbody>
		<%
			for(int i=0; i<list.size(); i++){
				item = new ItemVO();
				item = list.get(i);
				isbn = item.getItem_id();
				name = item.getItem_name();
				author = item.getAuthor();
				publisher = item.getPublisher();
		%>
				<tr>
					<input type="hidden" value="<%=isbn%>" name="<%=i%>_isbn" id="<%=i%>_isbn">
					<input type="hidden" value="<%=name%>" name="<%=i%>_name" id="<%=i%>_name">
					<input type="hidden" value="<%=author%>" name="<%=i%>_author" id="<%=i%>_author">
					<input type="hidden" value="<%=publisher%>" name="<%=i%>_publisher" id="<%=i%>_publisher">
					<td id="<%=i%>_name"><%=name%></td>
					<td id="<%=i%>_author"><%=author%></td>
					<td id="<%=i%>_publisher"><%=publisher%></td>
					<td><input type="button" onclick="set_item_detail('<%=i%>')"></td>
				</tr>
		<%
			}
		%>
			</tbody>
		</table>
	</body>
</html>