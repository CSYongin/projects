<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%
			String cur_member_id = session.getAttribute("id").toString();
			String cur_member_classify = session.getAttribute(cur_member_id+"_Classify").toString();
			
			if(cur_member_classify.equals("1") ^ cur_member_classify.equals("2")){
				System.out.println("도서관리 가능");
			}else{
				response.sendRedirect("/Book/");
			}
		%>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<script src="/Book/Jquery/regiItem.js"></script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="SideBarWrapper">

					</div>
					<div class="ContentWrapper">
						<div class="RegisterItemWrapper">
							<form action="" method="POST" name="register_item_form" id="register_item_form">
								<input type="hidden" name="input_regi_book_isbn" id="input_regi_book_isbn" value="">
								<input type="hidden" name="input_regi_book_name" id="input_regi_book_name" value="">
								<input type="hidden" name="input_regi_book_author" id="input_regi_book_author" value="">
								<input type="hidden" name="input_regi_book_publiser" id="input_regi_book_publiser" value="">
									<div class="RegiBox ItemImg" style="width: 200px;">
										<div class="ImgShowBox">
											<img style="max-width: 100%" id="item_preview_img" >
										</div>
										<div class="ImgInputBox">
											<p class="img_uload_title">이미지 업로드</p>
											<input type="file" name="item_img_input_btn" id="item_img_input_btn" >
										</div>
									</div>
									<div class="RegiBox EssentialValueInputBox">
										<div class="JoinRow">
											<span>
												<label for="regi_book_isbn">ISBN : </label>
												<input type="text" name="regi_book_isbn" id="regi_book_isbn" autocomplete="off">
											</span>
											<div class="ErrorMsg" id="isbnMsg" name="isbnMsg" style="display: none">필수 항목입니다.</div>
										</div>
										<div class="JoinRow">
											<span>
												<label for="regi_book_author">도서명 : </label>
												<input type="text" name="regi_book_name" id="regi_book_name" autocomplete="off">
											</span>
											<div class="ErrorMsg" id="nameMsg" name="nameMsg" style="display: none">필수 항목입니다.</div>
										</div>
										<div class="JoinRow">
											<span>
												<label for="regi_book_author">저자 : </label>
												<input type="text" name="regi_book_author" id="regi_book_author" autocomplete="off">
											</span>
											<div class="ErrorMsg" id="authorMsg" name="authorMsg" style="display: none">필수 항목입니다.</div>
										</div>
										<div class="JoinRow">
											<span>
												<label for="regi_book_publiser">출판사 : </label>
												<input type="text" name="regi_book_publiser" id="regi_book_publiser" autocomplete="off">
											</span>
											<div class="ErrorMsg" id="publiserMsg" name="publiserMsg" style="display: none">필수 항목입니다.</div>
										</div>
									</div>
								</div>
								<div class="RowGroup">
								</div>
								<div class="ErrorMsg" id="submitMsg" name="submitMsg" style="display: none">입력하신 정보를 다시 확인해 주세요.</div>
								<div>
									<input type="button" value="등록" name="regi_book_submit" id="regi_book_submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>