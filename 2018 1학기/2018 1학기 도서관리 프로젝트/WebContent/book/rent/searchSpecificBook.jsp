<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%
			String specific_isbn = request.getParameter("isbn");
			String cur_user_id = session.getAttribute("id").toString();
		%>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<script type="text/javascript">
			$(function(){
				$("#rent_impossible").click(function(){
					alert("대출 불가능한 도서 입니다.");
					return false;
				});
			});
			function func_rent_proc(book_id, user_id){
				document.getElementById("input_rent_book_id").value=book_id;
				document.getElementById("input_rent_user_id").value=user_id;
				document.rental_book_form.action="/Book/rental.BProc";
				document.rental_book_form.submit();
			};
		</script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<%
						libraryService LibService = libraryService.getInstance();
						ArrayList<libraryVO> list = LibService.searchLib(specific_isbn);
						libraryVO lib = null;
					%>
					<div class="">
						<form action="" method="POST" name="rental_book_form" id="rental_book_form">
							<input type="hidden" value="" name="input_rent_book_id" id="input_rent_book_id">
							<input type="hidden" value="" name="input_rent_user_id" id="input_rent_user_id">
							<table>
								<thead>
									<tr>
										<th>도서번호</th>
										<th>도서명</th>
										<th>저자명</th>
										<th>출판사</th>
										<th>도서상태</th>
										<th>대출 신청</th>
									</tr>
								</thead>
								<tbody>
									<%
										for(int i=0;i<list.size();i++){
											lib = new libraryVO();
											lib = list.get(i);
											String book_id = lib.getBook_id();
											String book_isbn = lib.getBook_isbn();
											String book_name = lib.getBook_name();
											String book_author = lib.getBook_author();
											String book_publisher = lib.getBook_publisher();
											String book_state = lib.getBook_state();

											%>
											<tr>
												<td><%=book_id %></td>
												<td><%=book_name %></td>
												<td><%=book_author %></td>
												<td><%=book_publisher %></td>
												<%
													if(book_state.equals("use")){
												%>
												<td>대출중</td>
												<td><input type="button" value="대출" name="rent_impossible" id="rent_impossible" ></td>
												<%
													}else{
												%>
												<td>대출가능</td>
												<td><input type="button" value="대출" name="rent_available" id="rent_available" onclick="func_rent_proc('<%=book_id%>', '<%=cur_user_id%>');"></td>
											</tr>
												<%
												}
										}
									%>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>