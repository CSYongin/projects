<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" import="vo.*" import="serviceSet.*" import="dao.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<%
						ItemService IService = ItemService.getInstance();
						ArrayList<ItemVO> list = IService.itemSearch();
						ItemVO item = null;
					%>
					<div class="">
						<table>
							<thead>
								<tr>
									<th>도서명</th>
									<th>저자명</th>
									<th>출판사</th>
									<th>도서 정보 확인</th>
								</tr>
							</thead>
							<tbody>
								<%
									for(int i=0;i<list.size();i++){
										item = new ItemVO();
										item = list.get(i);
										String book_isbn = item.getItem_id();
										String book_name = item.getItem_name();
										String book_author = item.getAuthor();
										String book_publisher = item.getPublisher();
										%>
											<tr>
												<td><%=book_name%></td>
												<td><%=book_author%></td>
												<td><%=book_publisher%></td>
												<td><input type="button" value="해당 도서 정보 확인" onclick="location.href='/Book/book/rent/searchSpecificBook.jsp?isbn=<%=book_isbn %>'"></td>
											</tr>
										<%
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>