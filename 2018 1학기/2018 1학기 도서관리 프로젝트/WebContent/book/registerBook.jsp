<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<%
			String cur_member_id = session.getAttribute("id").toString();
			String cur_member_classify = session.getAttribute(cur_member_id+"_Classify").toString();

			if(cur_member_classify.equals("1") ^ cur_member_classify.equals("2")){
				System.out.println("도서관리 가능");
			}else{
				response.sendRedirect("/Book/");
			}
		%>
		<%@ include file="/jspInclude/headmetadata.jsp" %>
		<script src="/Book/Jquery/regibook.js"></script>
	</head>
	<body>
		<div class="WholeWrapper">
			<%@ include file="/jspInclude/header.jsp" %>
			<div class="HomeWrapper">
				<div class="AreaFlex">
					<div class="SideBarWrapper">

					</div>
					<div class="ContentWrapper">
						<div class="RegisterBookWrapper">
							<form action="" method="POST" name="regi_new_book_form" id="regi_new_book_form">
								<input type="hidden" name="input_regi_book_isbn" id="input_regi_book_isbn">
								<div class="RegiBox">
									<div class="JoinRow">
										<span>
											<label for="">도서 번호 : </label>
											<input type="text" name="regi_book_id" id="regi_book_id" autocomplete="off">
										</span>
										<div class="ErrorMsg" id="bookidMsg" name="bookidMsg" style="display: none">필수 항목입니다.</div>
									</div>
									<div class="JoinRow">
										<span>
											<input type="button" value="도서 검색" name="open_book_info" id="open_book_info"><br>
											<span>
												<label for="regi_book_author">도서명 : </label>
												<input type="text" name="regi_book_name" id="regi_book_name" readonly="">
											</span>
											<span>
												<label for="regi_book_author">저자 : </label>
												<input type="text" name="regi_book_author" id="regi_book_author" readonly="">
											</span>
											<span>
												<label for="regi_book_publiser">출판사 : </label>
												<input type="text" name="regi_book_publiser" id="regi_book_publiser" readonly="">
											</span>
										</span>
										<div class="ErrorMsg" id="Msg" name="Msg" style="display: none">필수 항목입니다.</div>
									</div>
								</div>
								<div>
									<input type="button" value="도서 등록" name="regi_book_submit" id="regi_book_submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>