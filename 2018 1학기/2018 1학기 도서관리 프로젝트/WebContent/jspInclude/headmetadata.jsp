<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<meta http-equiv="Content-Type" content="test/html; charset=UTF-8">
<meta name="Generator" content="sublime Text 3">
<meta name="Author" content="Woojun.kim / ">
<meta name="Keywords" content="index.jsp">
<meta name="Description" content="">
<title></title>
<!--  common css file or link -->
<link rel="stylesheet" type="text/css" href="/Book/css/initialize.css">
<link rel="stylesheet" type="text/css" href="/Book/css/common_index.css">
<!--common jsp, jquery, json link-->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
<!--Daum 우편번호 서비스-->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="/Book/Jquery/daumPostcode.js"></script>
<!--jsp, jquery, json file-->
<script src="/Book/Jquery/common.js"></script>