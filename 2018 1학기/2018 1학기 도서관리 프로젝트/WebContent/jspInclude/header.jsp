<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<body>
			<div class="HeaderWrapper">
				<%
					String curPath = (String)request.getRequestURI();
					//현재 경로를 구해서 로그인창에 같이 보낸 후, 로그인을 하고 나면 해당 페이지로 바로 로그인 상태로 이동하기 위한 현재 경로 구하는 변수
					String display_cur_user, display_login, display_logout, display_signup, display_admin_menu, display_seller_menu, mypage_path, myrental_path, myinfo_path;
					String cur_id, cur_name="", temp;
					int cur_id_classify;
					if(session.getAttribute("id")!=null){
						//로그인 되어 있을 경우
						cur_id = session.getAttribute("id").toString();
						cur_name = session.getAttribute(cur_id+"_Name").toString();
						temp = session.getAttribute(cur_id+"_Classify").toString();
						cur_id_classify = Integer.parseInt(temp);

						display_admin_menu = "none";
						display_seller_menu = "none";
						display_cur_user = "block";
						display_login = "none";
						display_logout = "block";
						display_signup = "none";
						mypage_path = "/Book/member/mypage/Index.jsp";
						myrental_path = "/Book/member/mypage/membership/myrental.jsp";
						myinfo_path = "/Book/member/mypage/membership/myInfo.jsp";

						if(cur_id_classify==1){
							display_admin_menu = "block";
							display_seller_menu = "block";
						}else if(cur_id_classify==2){
							display_admin_menu = "none";
							display_seller_menu = "block";
						}
					}else{
						//로그인이 안되어 있을 경우
						display_admin_menu = "none";
						display_seller_menu = "none";
						display_cur_user = "none";
						display_login = "block";
						display_logout = "none";
						display_signup = "block";
						mypage_path = "/Book/member/login/login.jsp?URI=/Book/member/mypage/Index.jsp";
						myrental_path = "/Book/member/login/login.jsp?URI=/Book/member/mypage/membership/myrental.jsp";
						myinfo_path = "/Book/member/login/login.jsp?URI=/Book/member/mypage/membership/myInfo.jsp";

					}
				%>
				<div class="HeaderTopArea">
					<!-- 페이지 최상단 기본 메뉴 영역 -->
					<div class="AreaFlex">
						<div class="LayoutLeft">
							<!-- 관리자의 경우 물품관리/회원관리 메뉴가, 판매자의 경우 물품관리 메뉴가 생성되는 layout -->
							<ul class="LayoutLeftUl">
								<!--project_pageDB의 header_top_layoutleft-->
								<li class="HeaderAdminMenu" style="display: <%=display_admin_menu%>"><a href="#">회원관리</a></li>
								<li class="HeaderSellerMenu" style="display: <%=display_seller_menu%>"><a href="#">물품관리</a>
									<ul>
										<li style="display: <%=display_seller_menu%>"><a href="#">물품관리 홈</a></li>
										<li style="display: <%=display_seller_menu%>"><a href="/Book/item/registerNewItem.jsp">물품등록</a></li>
										<li style="display: <%=display_seller_menu%>"><a href="/Book/book/registerBook.jsp">도서등록</a></li>
										<li style="display: <%=display_seller_menu%>"><a href="#"></a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="LayoutRight">
							<!-- 기본적인 쇼핑몰의 로그인/회원가입/장바구니/주문배송/마이페이지/고객센터의 메뉴가 있는 layout -->
							<ul class="LayoutRightUl">
								<!--project_pageDB의 header_top_layoutright-->
								<li class="HeaderCurUserName" style="display: <%=display_cur_user%>"><a href="#"><%=cur_name %></a>님</li>
								<li class="HeaderLoginView" style="display: <%=display_login%>"><a href="/Book/member/login/login.jsp?URI=<%=curPath%>">로그인</a></li>
								<li class="HeaderLogout" style="display: <%=display_logout%>"><a href="/Book/logout.MemProc">로그아웃</a></li>
								<li class="HeaderMemberRegi" style="display: <%=display_signup%>"><a href="/Book/member/signUp/choiceMemberType.jsp">회원가입</a></li>
								<!-- <li><a href="#">장바구니</a></li>
								<li><a href="#">주문배송</a></li> -->
								<li class="MyPage"><a href="<%=mypage_path%>">마이페이지</a>
									<ul>
										<li><a href="<%=mypage_path%>">마이페이지 홈</a></li>
										<li><a href="<%=myrental_path%>">내 대여 현황</a></li>
										<li><a href="<%=myinfo_path%>">회원정보</a></li>
									</ul>
								</li>
								<li><a href="#">고객센터</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="HeaderContainer">
					<div class="AreaFlex">
						<div class="LogoContainer"><h1 class="Logo"><a href="/Book/index.jsp">....</a></h1></div>
						<div class="TotalSearch">
							<form method="GET" name="search_top" id="search_top">
								<div class="SearchInner">
									<input type="text" class="SearchInputBox" name="search_keywd" id="search_keywd" autocomplete="off">
									<input type="submit" class="SearchBtn" value="검색">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="HeaderBotArea">
					<div class="AreaFlex">
						<div class="LayoutLeft">
							<ul>
								<li><a href="/Book/book/rent/searchAllBook.jsp">도서 대여</a></li>
							</ul>
							<%-- <ul>
								<li><a href="#">Samsung</a></li>
								<li><a href="#">LG</a></li>
								<li><a href="#">APPLE</a></li>
								<li><a href="#">Xiaomi</a></li>
								<li><a href="#">기타</a></li>
							</ul> --%>
						</div>
						<div class="LayoutRight"></div>
					</div>
				</div>
			</div>
	</body>
</html>