/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - book_datadb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`book_datadb` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `bookdata` */

DROP TABLE IF EXISTS `bookdata`;

CREATE TABLE `bookdata` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(25) NOT NULL,
  `book_name` varchar(50) NOT NULL,
  `author` varchar(20) NOT NULL,
  `publisher` varchar(20) NOT NULL,
  `register_date` date NOT NULL,
  `img_path` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `seq` (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `bookdata` */

insert  into `bookdata`(`seq`,`book_id`,`book_name`,`author`,`publisher`,`register_date`,`img_path`) values 
(3,'123-456-789-001','jsp','홍길동','한빛','2018-05-29',NULL),
(1,'978-89-7914-992-0','sql&Server','우재남','한빛미디어','2013-03-02',NULL);

/*Table structure for table `library` */

DROP TABLE IF EXISTS `library`;

CREATE TABLE `library` (
  `book_id` varchar(10) NOT NULL,
  `book_isbn` varchar(20) NOT NULL,
  `user_id` varchar(12) NOT NULL,
  `book_state` varchar(10) NOT NULL,
  `book_rent_date` date DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  KEY `book_isbn` (`book_isbn`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `library_ibfk_1` FOREIGN KEY (`book_isbn`) REFERENCES `bookdata` (`book_id`),
  CONSTRAINT `library_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `userdata` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `library` */

insert  into `library`(`book_id`,`book_isbn`,`user_id`,`book_state`,`book_rent_date`) values 
('B00001','123-456-789-001','library','stand_by','1999-01-01'),
('B00002','123-456-789-001','library','stand_by',NULL),
('B00003','978-89-7914-992-0','library','stand_by',NULL),
('B00004','978-89-7914-992-0','user1','use','2018-05-31');

/*Table structure for table `userclassify` */

DROP TABLE IF EXISTS `userclassify`;

CREATE TABLE `userclassify` (
  `user_class` int(11) NOT NULL,
  `user_class_val` varchar(20) NOT NULL,
  PRIMARY KEY (`user_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `userclassify` */

insert  into `userclassify`(`user_class`,`user_class_val`) values 
(1,'관리자계정'),
(2,'사업자회원'),
(3,'일반회원');

/*Table structure for table `userdata` */

DROP TABLE IF EXISTS `userdata`;

CREATE TABLE `userdata` (
  `user_id` varchar(12) NOT NULL,
  `user_pw` varchar(16) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_phone` varchar(20) NOT NULL,
  `user_email` varchar(40) NOT NULL,
  `user_postcode` int(11) NOT NULL,
  `user_addr1` varchar(200) NOT NULL,
  `user_addr2` varchar(100) NOT NULL,
  `user_classify` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_classify` (`user_classify`),
  CONSTRAINT `userdata_ibfk_1` FOREIGN KEY (`user_classify`) REFERENCES `userclassify` (`user_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `userdata` */

insert  into `userdata`(`user_id`,`user_pw`,`user_name`,`user_phone`,`user_email`,`user_postcode`,`user_addr1`,`user_addr2`,`user_classify`) values 
('admin','cs1234','admin','010-3777-9605','admin@gmail.com',1234,'1234','1234',1),
('library','li1234','도서관반납ID','031-123-4567','library@yongin.ac.kr',17092,'경기 용인시 처인구 용인대학로 134 (삼가동, 용인대학교)','환경과학대학교',1),
('seller1','1111','seller1','010-3777-9605','seller1@gmail.com',6164,'서울 강남구 테헤란로 521 (삼성동, 파르나스타워)','8층',2),
('user1','1111','user1','010-6364-9999','user1@gmail.com',6164,'서울 강남구 테헤란로 521 (삼성동, 파르나스타워)','12345',3),
('user2','2222','user2','010-5498-3198','user2@nate.com',15874,'경기 군포시 부곡동 1195','1532',3),
('user3','','홍길동','010-4687-9512','kwj0118@nate.com',6164,'서울 강남구 테헤란로 521 (삼성동, 파르나스타워)','8층',3),
('user4','1234','김우준','010-3777-9605','user4@hanmail.net',16937,'경기 용인시 수지구 상현로 101 (상현동, 상현마을수지센트럴아이파크)','107동 1603호',3),
('user5','5555','user5','010-3777-9605','user5@gmail.com',16937,'경기 용인시 수지구 상현로 101 (상현동, 상현마을수지센트럴아이파크)','101',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
