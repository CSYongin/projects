/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - board
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`board` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `board` */

DROP TABLE IF EXISTS `board`;

CREATE TABLE `board` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `writer` varchar(20) DEFAULT NULL,
  `content` text,
  `regdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `cnt` int(11) DEFAULT '0',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `board` */

insert  into `board`(`seq`,`title`,`writer`,`content`,`regdate`,`cnt`) values 
(1,'가입인사','관리자','wwww','2018-06-11 13:31:59',0),
(2,'aaaa','bbbb','ccccccc','2018-06-11 16:16:19',0),
(3,'aaaa','bbbb','ccccccc','2018-06-11 16:18:38',0),
(4,'aaaa','bbbb','ccccccc','2018-06-11 16:22:23',0),
(5,'aaaa','bbbb','ccccccc','2018-06-11 16:25:47',0),
(6,'aaaa','bbbb','ccccccc','2018-06-11 16:26:17',0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` varchar(8) NOT NULL,
  `user_pw` varchar(8) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `user_role` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`user_id`,`user_pw`,`user_name`,`user_role`) values 
('test','test123','관리자','Admin'),
('user1','uset1','홍길동','User');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
