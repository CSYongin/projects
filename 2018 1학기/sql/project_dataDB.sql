/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.21-log : Database - project_datadb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_datadb` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `class_user` */

DROP TABLE IF EXISTS `class_user`;

CREATE TABLE `class_user` (
  `class_val` int(11) NOT NULL,
  `class_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`class_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `class_user` */

insert  into `class_user`(`class_val`,`class_desc`) values 
(1,'관리자'),
(11,'vvip'),
(12,'vip'),
(13,'friend'),
(21,'vvip'),
(22,'vip'),
(23,'friend');

/*Table structure for table `grant_user` */

DROP TABLE IF EXISTS `grant_user`;

CREATE TABLE `grant_user` (
  `grant_val` int(11) NOT NULL,
  `grant_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`grant_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grant_user` */

insert  into `grant_user`(`grant_val`,`grant_desc`) values 
(0,'admin'),
(10,'seller'),
(20,'buyer');

/*Table structure for table `item` */

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(45) NOT NULL,
  `item_manu` varchar(45) NOT NULL,
  `item_manu_val` int(11) DEFAULT '0',
  `item_class_val` int(11) NOT NULL,
  `item_img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_item_item_manu1_idx` (`item_manu_val`),
  KEY `fk_item_item_class1_idx` (`item_class_val`),
  CONSTRAINT `fk_item_item_class1` FOREIGN KEY (`item_class_val`) REFERENCES `item_class` (`class_val`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_item_manu1` FOREIGN KEY (`item_manu_val`) REFERENCES `item_manu` (`manu_val`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `item` */

insert  into `item`(`item_id`,`item_name`,`item_manu`,`item_manu_val`,`item_class_val`,`item_img`) values 
(1,'F8J148bt04_PINK','벨킨',0,3,'/FreeProject/img/12692b6c32.jpg'),
(2,'삼성충전케이블(5핀)','SAMSUNG',0,3,'/FreeProject/img/13f5882c122.jpg'),
(3,'UP605고속충전기','IPTime',0,5,'/FreeProject/img/10f31064d2.jpg'),
(4,'갤럭시 S8 투명케이스','???',2,1,'/FreeProject/img/38b96dc6-5f8b-4174-bd9c-4e89fb787103.jpg'),
(5,'갤럭시 S9 젤리케이스','????',2,1,'/FreeProject/img/1000024962262_i1_500.jpg');

/*Table structure for table `item_class` */

DROP TABLE IF EXISTS `item_class`;

CREATE TABLE `item_class` (
  `class_val` int(11) NOT NULL,
  `class_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`class_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `item_class` */

insert  into `item_class`(`class_val`,`class_desc`) values 
(1,'케이스'),
(2,'필름'),
(3,'케이블'),
(4,'거치대'),
(5,'충전기'),
(6,'파우치'),
(7,'기타');

/*Table structure for table `item_manu` */

DROP TABLE IF EXISTS `item_manu`;

CREATE TABLE `item_manu` (
  `manu_val` int(11) NOT NULL,
  `manu_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`manu_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `item_manu` */

insert  into `item_manu`(`manu_val`,`manu_desc`) values 
(0,'accessory'),
(1,'APPLE'),
(2,'SAMSUNG'),
(3,'LG'),
(4,'SONY'),
(5,'XIAOMI'),
(6,'ETC');

/*Table structure for table `order_cart` */

DROP TABLE IF EXISTS `order_cart`;

CREATE TABLE `order_cart` (
  `order_seq` int(11) NOT NULL AUTO_INCREMENT,
  `total_price` int(11) NOT NULL,
  `buyer_id` varchar(12) NOT NULL,
  PRIMARY KEY (`order_seq`),
  KEY `fk_order_userdata1_idx` (`buyer_id`),
  CONSTRAINT `fk_order_userdata2` FOREIGN KEY (`buyer_id`) REFERENCES `userdata` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_cart` */

/*Table structure for table `order_has_product` */

DROP TABLE IF EXISTS `order_has_product`;

CREATE TABLE `order_has_product` (
  `order_seq` int(11) NOT NULL,
  `pr_id` varchar(20) NOT NULL,
  `pr_cnt` int(11) NOT NULL,
  KEY `fk_order_has_product_order1_idx` (`order_seq`),
  KEY `fk_order_has_product_products1_idx` (`pr_id`),
  CONSTRAINT `fk_order_has_product_order1` FOREIGN KEY (`order_seq`) REFERENCES `orders` (`order_seq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_has_product_products1` FOREIGN KEY (`pr_id`) REFERENCES `products` (`pr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_has_product` */

insert  into `order_has_product`(`order_seq`,`pr_id`,`pr_cnt`) values 
(3,'PHC000001',1),
(3,'PHC0000002',2),
(3,'PC000001',3),
(4,'PHC000001',3),
(4,'PC000001',1),
(5,'PHC000001',4),
(5,'PC000001',4),
(6,'PHC000001',5),
(6,'PC000001',5),
(7,'PC000001',3),
(8,'PHC000001',3),
(8,'PC000001',2);

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `order_seq` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `total_price` int(11) NOT NULL,
  `buyer_id` varchar(12) NOT NULL,
  PRIMARY KEY (`order_seq`),
  KEY `fk_order_userdata1_idx` (`buyer_id`),
  CONSTRAINT `fk_order_userdata1` FOREIGN KEY (`buyer_id`) REFERENCES `userdata` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`order_seq`,`order_date`,`total_price`,`buyer_id`) values 
(1,'1999-01-01',0,'admin'),
(2,'1999-01-01',0,'admin'),
(3,'2018-06-19',21500,'user1'),
(4,'2018-06-19',12500,'user1'),
(5,'2018-06-19',26000,'user1'),
(6,'2018-06-19',32500,'user1'),
(7,'2018-06-19',10500,'user2'),
(8,'2018-06-20',16000,'user2');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `pr_id` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pr_name` varchar(45) NOT NULL,
  `pr_cnt` int(11) NOT NULL,
  `pr_price` int(11) NOT NULL,
  `pr_regiDate` date NOT NULL,
  `seller_id` varchar(12) NOT NULL,
  PRIMARY KEY (`pr_id`),
  KEY `fk_products_userdata1_idx` (`seller_id`),
  KEY `fk_products_item1_idx` (`item_id`),
  CONSTRAINT `fk_products_item1` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_userdata1` FOREIGN KEY (`seller_id`) REFERENCES `userdata` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`pr_id`,`item_id`,`pr_name`,`pr_cnt`,`pr_price`,`pr_regiDate`,`seller_id`) values 
('C000001',3,'IPtime 초고속 충전기',200,19000,'2018-06-20','seller2'),
('PC000001',1,'벨킨 애플 핸드폰 충전 케이블',132,3500,'2018-06-09','seller1'),
('PHC0000002',5,'삼성 갤럭시 S9 젤리케이스 쥬얼리',48,4000,'2018-06-09','seller1'),
('PHC000001',4,'갤럭시 S8 투명 젤리 케이스',44,3000,'2018-06-09','seller1');

/*Table structure for table `userdata` */

DROP TABLE IF EXISTS `userdata`;

CREATE TABLE `userdata` (
  `user_id` varchar(12) NOT NULL,
  `user_pw` text NOT NULL,
  `user_name` varchar(16) NOT NULL,
  `user_phone1` varchar(3) NOT NULL,
  `user_phone2` varchar(4) NOT NULL,
  `user_phone3` varchar(4) NOT NULL,
  `user_email1` varchar(15) NOT NULL,
  `user_email2` varchar(20) NOT NULL,
  `user_postcode` int(11) NOT NULL,
  `user_addr1` varchar(150) NOT NULL,
  `user_addr2` varchar(100) NOT NULL,
  `user_grant_val` int(11) NOT NULL,
  `user_class_val` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_userdata_grant_user_idx` (`user_grant_val`),
  KEY `fk_userdata_class_user1_idx` (`user_class_val`),
  CONSTRAINT `fk_userdata_class_user1` FOREIGN KEY (`user_class_val`) REFERENCES `class_user` (`class_val`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userdata_grant_user` FOREIGN KEY (`user_grant_val`) REFERENCES `grant_user` (`grant_val`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `userdata` */

insert  into `userdata`(`user_id`,`user_pw`,`user_name`,`user_phone1`,`user_phone2`,`user_phone3`,`user_email1`,`user_email2`,`user_postcode`,`user_addr1`,`user_addr2`,`user_grant_val`,`user_class_val`) values 
('admin','A4D0C90427831CA2F73F903A6F0C44A0','관리자','010','1234','5678','admin','gmail.com',16937,'경기도 용인시 수지구 상현로 101(상현마을, 수지센트럴아이파크)','107-1603',0,1),
('seller1','1B710539314D06E01FD3AB3F563B757F','판매자1','010','5555','8888','seller1','nate.com',6164,'서울 강남구 테헤란로 521 (삼성동, 파르나스타워)','12층',10,13),
('seller2','0A502D62B7055A0C4662158A5117A04C','판매자2','010','4568','9876','seller2','gmail.com',13536,'경기 성남시 분당구 판교역로 24 (백현동)','7층 ',10,13),
('user1','4F9AA6528187E20EA9746F61AB9E57CF','회원1','019','987','1245','usr1','hanmail.net',13536,'경기 성남시 분당구 판교역로 24 (백현동)','안랩 지하 1층',20,23),
('user2','4ABA575A7112DC71938B914AFE2B6869','회원2','010','3214','5978','user2','gmail.com',6164,'서울 강남구 테헤란로 521 (삼성동, 파르나스타워)','8층',20,23);

/*Table structure for table `v_desc_item_accessory_all` */

DROP TABLE IF EXISTS `v_desc_item_accessory_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_item_accessory_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_item_accessory_all` */;

/*!50001 CREATE TABLE  `v_desc_item_accessory_all`(
 `item_id` int(11) ,
 `item_name` varchar(45) ,
 `item_manu` varchar(45) ,
 `class_desc` varchar(45) ,
 `manu_desc` varchar(45) ,
 `item_img` varchar(200) 
)*/;

/*Table structure for table `v_desc_item_all` */

DROP TABLE IF EXISTS `v_desc_item_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_item_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_item_all` */;

/*!50001 CREATE TABLE  `v_desc_item_all`(
 `item_id` int(11) ,
 `item_name` varchar(45) ,
 `item_manu` varchar(45) ,
 `class_desc` varchar(45) ,
 `manu_desc` varchar(45) ,
 `item_img` varchar(200) 
)*/;

/*Table structure for table `v_desc_item_case_all` */

DROP TABLE IF EXISTS `v_desc_item_case_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_item_case_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_item_case_all` */;

/*!50001 CREATE TABLE  `v_desc_item_case_all`(
 `item_id` int(11) ,
 `item_name` varchar(45) ,
 `item_manu` varchar(45) ,
 `class_desc` varchar(45) ,
 `manu_desc` varchar(45) ,
 `item_img` varchar(200) 
)*/;

/*Table structure for table `v_desc_item_film_all` */

DROP TABLE IF EXISTS `v_desc_item_film_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_item_film_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_item_film_all` */;

/*!50001 CREATE TABLE  `v_desc_item_film_all`(
 `item_id` int(11) ,
 `item_name` varchar(45) ,
 `item_manu` varchar(45) ,
 `class_desc` varchar(45) ,
 `manu_desc` varchar(45) ,
 `item_img` varchar(200) 
)*/;

/*Table structure for table `v_desc_productdata_all` */

DROP TABLE IF EXISTS `v_desc_productdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_productdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_productdata_all` */;

/*!50001 CREATE TABLE  `v_desc_productdata_all`(
 `pr_id` varchar(20) ,
 `pr_name` varchar(45) ,
 `pr_cnt` int(11) ,
 `pr_price` int(11) ,
 `item_name` varchar(45) ,
 `item_manufac` varchar(45) ,
 `manu_desc` varchar(45) ,
 `class_desc` varchar(45) ,
 `item_img` varchar(200) ,
 `seller_id` varchar(12) ,
 `seller_name` varchar(16) ,
 `seller_phone` varchar(13) ,
 `seller_email` varchar(36) ,
 `seller_postcode` int(11) ,
 `seller_addr1` varchar(150) ,
 `seller_addr2` varchar(100) 
)*/;

/*Table structure for table `v_desc_userdata_all` */

DROP TABLE IF EXISTS `v_desc_userdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_desc_userdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_desc_userdata_all` */;

/*!50001 CREATE TABLE  `v_desc_userdata_all`(
 `user_id` varchar(12) ,
 `user_pw` text ,
 `user_name` varchar(16) ,
 `user_phone` varchar(13) ,
 `user_phone1` varchar(3) ,
 `user_phone2` varchar(4) ,
 `user_phone3` varchar(4) ,
 `user_email` varchar(36) ,
 `user_email1` varchar(15) ,
 `user_email2` varchar(20) ,
 `user_postcode` int(11) ,
 `user_addr1` varchar(150) ,
 `user_addr2` varchar(100) ,
 `grant_desc` varchar(45) ,
 `class_desc` varchar(45) 
)*/;

/*Table structure for table `v_itemdata_all` */

DROP TABLE IF EXISTS `v_itemdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_itemdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_itemdata_all` */;

/*!50001 CREATE TABLE  `v_itemdata_all`(
 `item_id` int(11) ,
 `item_name` varchar(45) ,
 `item_img` varchar(200) 
)*/;

/*Table structure for table `v_orderdata_all` */

DROP TABLE IF EXISTS `v_orderdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_orderdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_orderdata_all` */;

/*!50001 CREATE TABLE  `v_orderdata_all`(
 `order_seq` int(11) ,
 `order_date` date ,
 `total_price` int(11) ,
 `pr_id` varchar(20) ,
 `pr_name` varchar(45) ,
 `pr_price` int(11) ,
 `item_img` varchar(200) ,
 `buy_cnt` int(11) ,
 `seller_id` varchar(12) ,
 `seller_name` varchar(16) ,
 `seller_phone` varchar(13) ,
 `seller_email` varchar(36) ,
 `seller_postcode` int(11) ,
 `seller_addr1` varchar(150) ,
 `seller_addr2` varchar(100) ,
 `buyer_id` varchar(12) ,
 `buyer_name` varchar(16) ,
 `buyer_phone` varchar(13) ,
 `buyer_email` varchar(36) ,
 `buyer_postcode` int(11) ,
 `buyer_addr1` varchar(150) ,
 `buyer_addr2` varchar(100) 
)*/;

/*Table structure for table `v_productdata_all` */

DROP TABLE IF EXISTS `v_productdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_productdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_productdata_all` */;

/*!50001 CREATE TABLE  `v_productdata_all`(
 `pr_id` varchar(20) ,
 `pr_name` varchar(45) ,
 `pr_cnt` int(11) ,
 `pr_price` int(11) 
)*/;

/*Table structure for table `v_userdata_all` */

DROP TABLE IF EXISTS `v_userdata_all`;

/*!50001 DROP VIEW IF EXISTS `v_userdata_all` */;
/*!50001 DROP TABLE IF EXISTS `v_userdata_all` */;

/*!50001 CREATE TABLE  `v_userdata_all`(
 `user_id` varchar(12) ,
 `user_pw` text ,
 `user_name` varchar(16) ,
 `user_phone1` varchar(3) ,
 `user_phone2` varchar(4) ,
 `user_phone3` varchar(4) ,
 `user_email1` varchar(15) ,
 `user_email2` varchar(20) ,
 `user_postcode` int(11) ,
 `user_addr1` varchar(150) ,
 `user_addr2` varchar(100) 
)*/;

/*View structure for view v_desc_item_accessory_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_item_accessory_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_item_accessory_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_item_accessory_all` AS select `i`.`item_id` AS `item_id`,`i`.`item_name` AS `item_name`,`i`.`item_manu` AS `item_manu`,`ic`.`class_desc` AS `class_desc`,`im`.`manu_desc` AS `manu_desc`,`i`.`item_img` AS `item_img` from ((`item` `i` left join `item_class` `ic` on((`i`.`item_class_val` = `ic`.`class_val`))) left join `item_manu` `im` on((`i`.`item_manu_val` = `im`.`manu_val`))) where (`im`.`manu_desc` = 'accessory') */;

/*View structure for view v_desc_item_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_item_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_item_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_item_all` AS select `i`.`item_id` AS `item_id`,`i`.`item_name` AS `item_name`,`i`.`item_manu` AS `item_manu`,`ic`.`class_desc` AS `class_desc`,`im`.`manu_desc` AS `manu_desc`,`i`.`item_img` AS `item_img` from ((`item` `i` left join `item_class` `ic` on((`i`.`item_class_val` = `ic`.`class_val`))) left join `item_manu` `im` on((`i`.`item_manu_val` = `im`.`manu_val`))) */;

/*View structure for view v_desc_item_case_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_item_case_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_item_case_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_item_case_all` AS select `i`.`item_id` AS `item_id`,`i`.`item_name` AS `item_name`,`i`.`item_manu` AS `item_manu`,`ic`.`class_desc` AS `class_desc`,`im`.`manu_desc` AS `manu_desc`,`i`.`item_img` AS `item_img` from ((`item` `i` left join `item_class` `ic` on((`i`.`item_class_val` = `ic`.`class_val`))) left join `item_manu` `im` on((`i`.`item_manu_val` = `im`.`manu_val`))) where (`ic`.`class_desc` = '휴대폰 케이스') */;

/*View structure for view v_desc_item_film_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_item_film_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_item_film_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_item_film_all` AS select `i`.`item_id` AS `item_id`,`i`.`item_name` AS `item_name`,`i`.`item_manu` AS `item_manu`,`ic`.`class_desc` AS `class_desc`,`im`.`manu_desc` AS `manu_desc`,`i`.`item_img` AS `item_img` from ((`item` `i` left join `item_class` `ic` on((`i`.`item_class_val` = `ic`.`class_val`))) left join `item_manu` `im` on((`i`.`item_manu_val` = `im`.`manu_val`))) where (`ic`.`class_desc` = '필름,글라스') */;

/*View structure for view v_desc_productdata_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_productdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_productdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_productdata_all` AS select `p`.`pr_id` AS `pr_id`,`p`.`pr_name` AS `pr_name`,`p`.`pr_cnt` AS `pr_cnt`,`p`.`pr_price` AS `pr_price`,`i`.`item_name` AS `item_name`,`i`.`item_manu` AS `item_manufac`,`im`.`manu_desc` AS `manu_desc`,`ic`.`class_desc` AS `class_desc`,`i`.`item_img` AS `item_img`,`su`.`user_id` AS `seller_id`,`su`.`user_name` AS `seller_name`,concat(`su`.`user_phone1`,'-',`su`.`user_phone2`,'-',`su`.`user_phone3`) AS `seller_phone`,concat(`su`.`user_email1`,'@',`su`.`user_email2`) AS `seller_email`,`su`.`user_postcode` AS `seller_postcode`,`su`.`user_addr1` AS `seller_addr1`,`su`.`user_addr2` AS `seller_addr2` from ((((`products` `p` left join `item` `i` on((`p`.`item_id` = `i`.`item_id`))) left join `userdata` `su` on((`p`.`seller_id` = `su`.`user_id`))) left join `item_manu` `im` on((`i`.`item_manu_val` = `im`.`manu_val`))) left join `item_class` `ic` on((`i`.`item_class_val` = `ic`.`class_val`))) */;

/*View structure for view v_desc_userdata_all */

/*!50001 DROP TABLE IF EXISTS `v_desc_userdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_desc_userdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desc_userdata_all` AS select `u`.`user_id` AS `user_id`,`u`.`user_pw` AS `user_pw`,`u`.`user_name` AS `user_name`,concat(`u`.`user_phone1`,'-',`u`.`user_phone2`,'-',`u`.`user_phone3`) AS `user_phone`,`u`.`user_phone1` AS `user_phone1`,`u`.`user_phone2` AS `user_phone2`,`u`.`user_phone3` AS `user_phone3`,concat(`u`.`user_email1`,'@',`u`.`user_email2`) AS `user_email`,`u`.`user_email1` AS `user_email1`,`u`.`user_email2` AS `user_email2`,`u`.`user_postcode` AS `user_postcode`,`u`.`user_addr1` AS `user_addr1`,`u`.`user_addr2` AS `user_addr2`,`gu`.`grant_desc` AS `grant_desc`,`cu`.`class_desc` AS `class_desc` from ((`userdata` `u` left join `grant_user` `gu` on((`u`.`user_grant_val` = `gu`.`grant_val`))) left join `class_user` `cu` on((`u`.`user_class_val` = `cu`.`class_val`))) */;

/*View structure for view v_itemdata_all */

/*!50001 DROP TABLE IF EXISTS `v_itemdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_itemdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_itemdata_all` AS select `item`.`item_id` AS `item_id`,`item`.`item_name` AS `item_name`,`item`.`item_img` AS `item_img` from `item` */;

/*View structure for view v_orderdata_all */

/*!50001 DROP TABLE IF EXISTS `v_orderdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_orderdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_orderdata_all` AS select `o`.`order_seq` AS `order_seq`,`o`.`order_date` AS `order_date`,`o`.`total_price` AS `total_price`,`ohp`.`pr_id` AS `pr_id`,`pd`.`pr_name` AS `pr_name`,`pd`.`pr_price` AS `pr_price`,`pd`.`item_img` AS `item_img`,`ohp`.`pr_cnt` AS `buy_cnt`,`pd`.`seller_id` AS `seller_id`,`pd`.`seller_name` AS `seller_name`,`pd`.`seller_phone` AS `seller_phone`,`pd`.`seller_email` AS `seller_email`,`pd`.`seller_postcode` AS `seller_postcode`,`pd`.`seller_addr1` AS `seller_addr1`,`pd`.`seller_addr2` AS `seller_addr2`,`u`.`user_id` AS `buyer_id`,`u`.`user_name` AS `buyer_name`,concat(`u`.`user_phone1`,'-',`u`.`user_phone2`,'-',`u`.`user_phone3`) AS `buyer_phone`,concat(`u`.`user_email1`,'@',`u`.`user_email2`) AS `buyer_email`,`u`.`user_postcode` AS `buyer_postcode`,`u`.`user_addr1` AS `buyer_addr1`,`u`.`user_addr2` AS `buyer_addr2` from (((`orders` `o` left join `order_has_product` `ohp` on((`o`.`order_seq` = `ohp`.`order_seq`))) left join `v_desc_productdata_all` `pd` on((`ohp`.`pr_id` = `pd`.`pr_id`))) left join `userdata` `u` on((`o`.`buyer_id` = `u`.`user_id`))) order by `o`.`order_seq` desc */;

/*View structure for view v_productdata_all */

/*!50001 DROP TABLE IF EXISTS `v_productdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_productdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_productdata_all` AS select `products`.`pr_id` AS `pr_id`,`products`.`pr_name` AS `pr_name`,`products`.`pr_cnt` AS `pr_cnt`,`products`.`pr_price` AS `pr_price` from `products` */;

/*View structure for view v_userdata_all */

/*!50001 DROP TABLE IF EXISTS `v_userdata_all` */;
/*!50001 DROP VIEW IF EXISTS `v_userdata_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_userdata_all` AS select `userdata`.`user_id` AS `user_id`,`userdata`.`user_pw` AS `user_pw`,`userdata`.`user_name` AS `user_name`,`userdata`.`user_phone1` AS `user_phone1`,`userdata`.`user_phone2` AS `user_phone2`,`userdata`.`user_phone3` AS `user_phone3`,`userdata`.`user_email1` AS `user_email1`,`userdata`.`user_email2` AS `user_email2`,`userdata`.`user_postcode` AS `user_postcode`,`userdata`.`user_addr1` AS `user_addr1`,`userdata`.`user_addr2` AS `user_addr2` from `userdata` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
